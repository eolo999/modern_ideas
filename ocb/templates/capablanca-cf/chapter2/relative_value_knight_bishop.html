{% extends "capablanca-cf-base.html" %}

{% block title %}
14. The relative value of knight and bishop
{% endblock %}

{% block content %}
<h1>14. The relative value of knight and bishop</h1>


<p>
    Before turning our attention to this matter it is well to state now that
    <em>two Knights alone cannot mate</em>, but, under certain conditions of
    course, they can do so if the opponent has one or more Pawns.
</p>

<h3>Example 29.</h3>

{{ game.pgn("example29a", "chapter2", "capablanca-cf") }}

<p>
    In the above position White cannot win, although the Black King is
    cornered, but in the following position, in which Black has a Pawn, White
    wins with or without the move. Thus:
</p>

{{ game.pgn("example29b", "chapter2", "capablanca-cf") }}

<p>
    The reason for this peculiarity in chess is evident. <em>White with the two
      Knights can only stalemate the King, unless Black has a Pawn which can be
    moved</em>.
</p>

<h3>Example 30.</h3>

<p>
    Although he is a Bishop and a Pawn ahead the following position cannot be
    won by White.
</p>

{{ game.pgn("example30", "chapter2", "capablanca-cf") }}

<p>
    It is the greatest weakness of the Bishop, that when the Rook's Pawn Queens
    on a square of opposite colour and the opposing King is in front of the
    Pawn, the Bishop is absolutely worthless. All that Black has to do is to
    keep moving his King close to the corner square.
</p>

<h3>Example 31.</h3>

<p>
    In the following position White with or without the move can win. Take the
    most difficult variation.
</p>

{{ game.pgn("example31", "chapter2", "capablanca-cf") }}

<p>
    Now that we have seen these exceptional cases, we can analyse the different
    merits and the relative value of the Knight and the Bishop.
</p>

<p>
    It is generally thought by amateurs that the Knight is the more valuable
    piece of the two, the chief reason being that, unlike the Bishop, the
    Knight can command both Black and White squares. However, the fact is
    generally overlooked that the Knight, at any one time, has the choice of
    one colour only. It takes much longer to bring a Knight from one wing to
    the other. Also, as shown in the following Example, a Bishop can stalemate
    a Knight; a compliment which the Knight is unable to return.
</p>

<h3>Example 32.</h3>

{{ game.pgn("example32", "chapter2", "capablanca-cf") }}

<p>
    The weaker the player the more terrible the Knight is to him, but as a
    player increases in strength the value of the Bishop becomes more evident
    to him, and of course there is, or should be, a corresponding decrease in
    his estimation of the value of the Knight as compared to the Bishop. In
    this respect, as in many others, the masters of to-day are far ahead of the
    masters of former generations. While not so long ago some of the very best
    amongst them, like Pillsbury and Tchigorin, preferred Knights to Bishops,
    there is hardly a master of to-day who would not completely agree with the
    statements made above.  </p>

<h3>Example 33.</h3>

<p>
    This is about the only case when the Knight is more valuable than the
    Bishop.
</p>

{{ game.pgn("example33", "chapter2", "capablanca-cf") }}

<p>
    It is what is called a "<em>block position</em>", and all the Pawns are on
    one side of the board. (If there were Pawns on both sides of the board
    there would be no advantage in having a Knight.) In such a position Black
    has excellent chances of winning. Of course, there is an extra source of
    weakness for White in having his Pawns on the same colour-squares as his
    Bishop. This is a mistake often made by players. The proper way, generally,
    in an ending, is to have your Pawns on squares of opposite colour to that
    of your own Bishop. When you have your Pawns on squares of the same colour
    the action of your own Bishop is limited by them, and consequently the
    value of the Bishop is diminished, since the value of a piece can often be
    measured by the number of squares it commands. While on this subject, I
    shall also call attention to the fact that it is generally preferable to
    keep your Pawns on squares of the same colour as that of the opposing
    Bishop, particularly if they are passed Pawns supported by the King. The
    principles might be stated thus:
</p>

<p>
<em>When the opponent has a Bishop, keep your Pawns on squares of the same
  colour as your opponent's Bishop</em>.
</p>

<p>
    <em>Whenever you have a Bishop, whether the opponent has also one or not,
      keep your Pawns on squares of the opposite colour to that of your own
    Bishop.</em>
</p>

<p>
    Naturally, these principles have sometimes to be modified to suit the
    exigencies of the position.
</p>

<h3>Example 34.</h3>

<p>
    In the following position the Pawns are on one side of the board, and there
    is no advantage in having either a Knight or a Bishop. The game should
    surely end in a draw.
</p>

{{ game.pgn("example34", "chapter2", "capablanca-cf") }}

<h3>Example 35.</h3>

<p>
    Now let us add three Pawns on each side to the above position, so that
    there are Pawns on both sides of the board.
</p>

{{ game.pgn("example35", "chapter2", "capablanca-cf") }}

<p>
    It is now preferable to have the Bishop, though the position, if properly
    played out, should end in a draw. The advantage of having the Bishop lies
    as much in its ability to command, at long range, both sides of the board
    from a central position as in its ability to move quickly from one side of
    the board to the other.
</p>

<h3>Example 36.</h3>

<p>
    In the following position it is unquestionably an advantage to have the
    Bishop, because, although each player has the same number of Pawns, they
    are not balanced on each side of the board. Thus, on the King's side, White
    has three to two, while on the Queen's side it is Black that has three to
    two. Still, with proper play, the game should end in a draw, though White
    has somewhat better chances.
</p>

{{ game.pgn("example36", "chapter2", "capablanca-cf") }}

<h3>Example 37.</h3>

<p>
    Here is a position in which to have the Bishop is a decided advantage,
    since not only are there Pawns on both sides of the board, but there is a
    passed Pawn ("h" for White, "a" for Black). Black should have extreme
    difficulty in drawing this position, if he can do it at all.
</p>

{{ game.pgn("example37", "chapter2", "capablanca-cf") }}

<h3>Example 38.</h3>

<p>
    Again Black would have great difficulty in drawing this position.
</p>

{{ game.pgn("example38", "chapter2", "capablanca-cf") }}

<p>
    The student should carefully consider these positions. I hope that the many
    examples will help him to understand, in their true value, the relative
    merits of the Knight and Bishop. As to the general method of procedure, a
    teacher, or practical experience, will be best. I might say generally,
    however, that the proper course in these endings, as in all similar
    endings, is: Advance of the King to the centre of the board or towards the
    passed Pawns, or Pawns that are susceptible of being attacked, and rapid
    advance of the passed Pawn or Pawns as far as is consistent with their
    safety.
</p>

<p>
    To give a fixed line of play would be folly. Each ending is different, and
    requires different handling, according to what the adversary proposes to
    do. Calculation by visualising the future positions is what will count. 
</p>

{% endblock %}
