/* Copyright 2007-2010 Richard Jones
   This work is licensed under the Creative Commons Attribution-Noncommercial-No
   Derivative Works License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-nc-nd/2.5/au/
   */
Gettext=function(_1){
    this.domain="messages";
    this.locale_data=undefined;
    var _2=["domain","locale_data"];
    if(this.isValidObject(_1)){
        for(var i in _1){
            for(var j=0;j<_2.length;j++){
                if(i==_2[j]){
                    if(this.isValidObject(_1[i])){
                        this[i]=_1[i];
                    }
                }
            }
        }
    }
    this.try_load_lang();
    return this;
};
Gettext.context_glue="\x04";
Gettext._locale_data={};
Gettext.prototype.try_load_lang=function(){
    if(typeof (this.locale_data)!="undefined"){
        var _5=this.locale_data;
        this.locale_data=undefined;
        this.parse_locale_data(_5);
        if(typeof (Gettext._locale_data[this.domain])=="undefined"){
            throw new Error("Error: Gettext 'locale_data' does not contain the domain '"+this.domain+"'");
        }
    }
    var _6=this.get_lang_refs();
    if(typeof (_6)=="object"&&_6.length>0){
        for(var i=0;i<_6.length;i++){
            var _8=_6[i];
            if(_8.type=="application/json"){
                if(!this.try_load_lang_json(_8.href)){
                    throw new Error("Error: Gettext 'try_load_lang_json' failed. Unable to exec xmlhttprequest for link ["+_8.href+"]");
                }
            }else{
                if(_8.type=="application/x-po"){
                    if(!this.try_load_lang_po(_8.href)){
                        throw new Error("Error: Gettext 'try_load_lang_po' failed. Unable to exec xmlhttprequest for link ["+_8.href+"]");
                    }
                }else{
                    throw new Error("TODO: link type ["+_8.type+"] found, and support is planned, but not implemented at this time.");
                }
            }
        }
    }
};
Gettext.prototype.parse_locale_data=function(_9){
    if(typeof (Gettext._locale_data)=="undefined"){
        Gettext._locale_data={};
    }
    for(var _a in _9){
        if((!_9.hasOwnProperty(_a))||(!this.isValidObject(_9[_a]))){
            continue;
        }
        var _b=false;
        for(var _c in _9[_a]){
            _b=true;
            break;
        }
        if(!_b){
            continue;
        }
        var _d=_9[_a];
        if(_a==""){
            _a="messages";
        }
        if(!this.isValidObject(Gettext._locale_data[_a])){
            Gettext._locale_data[_a]={};
        }
        if(!this.isValidObject(Gettext._locale_data[_a].head)){
            Gettext._locale_data[_a].head={};
        }
        if(!this.isValidObject(Gettext._locale_data[_a].msgs)){
            Gettext._locale_data[_a].msgs={};
        }
        for(var _e in _d){
            if(_e==""){
                var _f=_d[_e];
                for(var _10 in _f){
                    var h=_10.toLowerCase();
                    Gettext._locale_data[_a].head[h]=_f[_10];
                }
            }else{
                Gettext._locale_data[_a].msgs[_e]=_d[_e];
            }
        }
    }
    for(var _a in Gettext._locale_data){
        if(this.isValidObject(Gettext._locale_data[_a].head["plural-forms"])&&typeof (Gettext._locale_data[_a].head.plural_func)=="undefined"){
            var _12=Gettext._locale_data[_a].head["plural-forms"];
            var _13=new RegExp("^(\\s*nplurals\\s*=\\s*[0-9]+\\s*;\\s*plural\\s*=\\s*(?:\\s|[-\\?\\|&=!<>+*/%:;a-zA-Z0-9_()])+)","m");
            if(_13.test(_12)){
                var pf=Gettext._locale_data[_a].head["plural-forms"];
                if(!/;\s*$/.test(pf)){
                    pf=pf.concat(";");
                }
                var _15="var plural; var nplurals; "+pf+" return { \"nplural\" : nplurals, \"plural\" : (plural === true ? 1 : plural ? plural : 0) };";
                Gettext._locale_data[_a].head.plural_func=new Function("n",_15);
            }else{
                throw new Error("Syntax error in language file. Plural-Forms header is invalid ["+_12+"]");
            }
        }else{
            if(typeof (Gettext._locale_data[_a].head.plural_func)=="undefined"){
                Gettext._locale_data[_a].head.plural_func=function(n){
                    var p=(n!=1)?1:0;
                    return {"nplural":2,"plural":p};
                };
            }
        }
    }
    return;
};
Gettext.prototype.try_load_lang_po=function(uri){
    var _19=this.sjax(uri);
    if(!_19){
        return;
    }
    var _1a=this.uri_basename(uri);
    var _1b=this.parse_po(_19);
    var rv={};
    if(_1b){
        if(!_1b[""]){
            _1b[""]={};
        }
        if(!_1b[""]["domain"]){
            _1b[""]["domain"]=_1a;
        }
        _1a=_1b[""]["domain"];
        rv[_1a]=_1b;
        this.parse_locale_data(rv);
    }
    return 1;
};
Gettext.prototype.uri_basename=function(uri){
    var rv;
    if(rv=uri.match(/^(.*\/)?(.*)/)){
        var _1f;
        if(_1f=rv[2].match(/^(.*)\..+$/)){
            return _1f[1];
        }else{
            return rv[2];
        }
    }else{
        return "";
    }
};
Gettext.prototype.parse_po=function(_20){
    var rv={};
    var _22={};
    var _23="";
    var _24=[];
    var _25=_20.split("\n");
    for(var i=0;i<_25.length;i++){
        _25[i]=_25[i].replace(/(\n|\r)+$/,"");
        var _27;
        if(/^$/.test(_25[i])){
            if(typeof (_22["msgid"])!="undefined"){
                var _28=(typeof (_22["msgctxt"])!="undefined"&&_22["msgctxt"].length)?_22["msgctxt"]+Gettext.context_glue+_22["msgid"]:_22["msgid"];
                var _29=(typeof (_22["msgid_plural"])!="undefined"&&_22["msgid_plural"].length)?_22["msgid_plural"]:null;
                var _2a=[];
                for(var str in _22){
                    var _27;
                    if(_27=str.match(/^msgstr_(\d+)/)){
                        _2a[parseInt(_27[1])]=_22[str];
                    }
                }
                _2a.unshift(_29);
                if(_2a.length>1){
                    rv[_28]=_2a;
                }
                _22={};
                _23="";
            }
        }else{
            if(/^#/.test(_25[i])){
                continue;
            }else{
                if(_27=_25[i].match(/^msgctxt\s+(.*)/)){
                    _23="msgctxt";
                    _22[_23]=this.parse_po_dequote(_27[1]);
                }else{
                    if(_27=_25[i].match(/^msgid\s+(.*)/)){
                        _23="msgid";
                        _22[_23]=this.parse_po_dequote(_27[1]);
                    }else{
                        if(_27=_25[i].match(/^msgid_plural\s+(.*)/)){
                            _23="msgid_plural";
                            _22[_23]=this.parse_po_dequote(_27[1]);
                        }else{
                            if(_27=_25[i].match(/^msgstr\s+(.*)/)){
                                _23="msgstr_0";
                                _22[_23]=this.parse_po_dequote(_27[1]);
                            }else{
                                if(_27=_25[i].match(/^msgstr\[0\]\s+(.*)/)){
                                    _23="msgstr_0";
                                    _22[_23]=this.parse_po_dequote(_27[1]);
                                }else{
                                    if(_27=_25[i].match(/^msgstr\[(\d+)\]\s+(.*)/)){
                                        _23="msgstr_"+_27[1];
                                        _22[_23]=this.parse_po_dequote(_27[2]);
                                    }else{
                                        if(/^"/.test(_25[i])){
                                            _22[_23]+=this.parse_po_dequote(_25[i]);
                                    }else{
                                        _24.push("Strange line ["+i+"] : "+_25[i]);
                                    }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if(typeof (_22["msgid"])!="undefined"){
        var _28=(typeof (_22["msgctxt"])!="undefined"&&_22["msgctxt"].length)?_22["msgctxt"]+Gettext.context_glue+_22["msgid"]:_22["msgid"];
        var _29=(typeof (_22["msgid_plural"])!="undefined"&&_22["msgid_plural"].length)?_22["msgid_plural"]:null;
        var _2a=[];
        for(var str in _22){
            var _27;
            if(_27=str.match(/^msgstr_(\d+)/)){
                _2a[parseInt(_27[1])]=_22[str];
            }
        }
        _2a.unshift(_29);
        if(_2a.length>1){
            rv[_28]=_2a;
        }
        _22={};
        _23="";
    }
    if(rv[""]&&rv[""][1]){
        var cur={};
        var _2d=rv[""][1].split(/\\n/);
        for(var i=0;i<_2d.length;i++){
            if(!_2d.length){
                continue;
            }
            var pos=_2d[i].indexOf(":",0);
            if(pos!=-1){
                var key=_2d[i].substring(0,pos);
                var val=_2d[i].substring(pos+1);
                var _31=key.toLowerCase();
                if(cur[_31]&&cur[_31].length){
                    _24.push("SKIPPING DUPLICATE HEADER LINE: "+_2d[i]);
                }else{
                    if(/#-#-#-#-#/.test(_31)){
                        _24.push("SKIPPING ERROR MARKER IN HEADER: "+_2d[i]);
                    }else{
                        val=val.replace(/^\s+/,"");
                        cur[_31]=val;
                    }
                }
            }else{
                _24.push("PROBLEM LINE IN HEADER: "+_2d[i]);
                cur[_2d[i]]="";
            }
        }
        rv[""]=cur;
    }else{
        rv[""]={};
    }
    return rv;
};
Gettext.prototype.parse_po_dequote=function(str){
    var _33;
    if(_33=str.match(/^"(.*)"/)){
        str=_33[1];
    }
    str=str.replace(/\\"/,"");
    return str;
};
Gettext.prototype.try_load_lang_json=function(uri){
    var _35=this.sjax(uri);
    if(!_35){
        return;
    }
    var rv=this.JSON(_35);
    this.parse_locale_data(rv);
    return 1;
};
Gettext.prototype.get_lang_refs=function(){
    var _37=new Array();
    var _38=document.getElementsByTagName("link");
    for(var i=0;i<_38.length;i++){
        if(_38[i].rel=="gettext"&&_38[i].href){
            if(typeof (_38[i].type)=="undefined"||_38[i].type==""){
                if(/\.json$/i.test(_38[i].href)){
                    _38[i].type="application/json";
                }else{
                    if(/\.js$/i.test(_38[i].href)){
                        _38[i].type="application/json";
                    }else{
                        if(/\.po$/i.test(_38[i].href)){
                            _38[i].type="application/x-po";
                        }else{
                            if(/\.mo$/i.test(_38[i].href)){
                                _38[i].type="application/x-mo";
                            }else{
                                throw new Error("LINK tag with rel=gettext found, but the type and extension are unrecognized.");
                            }
                        }
                    }
                }
            }
            _38[i].type=_38[i].type.toLowerCase();
            if(_38[i].type=="application/json"){
                _38[i].type="application/json";
            }else{
                if(_38[i].type=="text/javascript"){
                    _38[i].type="application/json";
                }else{
                    if(_38[i].type=="application/x-po"){
                        _38[i].type="application/x-po";
                    }else{
                        if(_38[i].type=="application/x-mo"){
                            _38[i].type="application/x-mo";
                        }else{
                            throw new Error("LINK tag with rel=gettext found, but the type attribute ["+_38[i].type+"] is unrecognized.");
                        }
                    }
                }
            }
            _37.push(_38[i]);
        }
    }
    return _37;
};
Gettext.prototype.textdomain=function(_3a){
    if(_3a&&_3a.length){
        this.domain=_3a;
    }
    return this.domain;
};
Gettext.prototype.gettext=function(_3b){
    var _3c;
    var _3d;
    var n;
    var _3f;
    return this.dcnpgettext(null,_3c,_3b,_3d,n,_3f);
};
Gettext.prototype.dgettext=function(_40,_41){
    var _42;
    var _43;
    var n;
    var _45;
    return this.dcnpgettext(_40,_42,_41,_43,n,_45);
};
Gettext.prototype.dcgettext=function(_46,_47,_48){
    var _49;
    var _4a;
    var n;
    return this.dcnpgettext(_46,_49,_47,_4a,n,_48);
};
Gettext.prototype.ngettext=function(_4c,_4d,n){
    var _4f;
    var _50;
    return this.dcnpgettext(null,_4f,_4c,_4d,n,_50);
};
Gettext.prototype.dngettext=function(_51,_52,_53,n){
    var _55;
    var _56;
    return this.dcnpgettext(_51,_55,_52,_53,n,_56);
};
Gettext.prototype.dcngettext=function(_57,_58,_59,n,_5b){
    var _5c;
    return this.dcnpgettext(_57,_5c,_58,_59,n,_5b,_5b);
};
Gettext.prototype.pgettext=function(_5d,_5e){
    var _5f;
    var n;
    var _61;
    return this.dcnpgettext(null,_5d,_5e,_5f,n,_61);
};
Gettext.prototype.dpgettext=function(_62,_63,_64){
    var _65;
    var n;
    var _67;
    return this.dcnpgettext(_62,_63,_64,_65,n,_67);
};
Gettext.prototype.dcpgettext=function(_68,_69,_6a,_6b){
    var _6c;
    var n;
    return this.dcnpgettext(_68,_69,_6a,_6c,n,_6b);
};
Gettext.prototype.npgettext=function(_6e,_6f,_70,n){
    var _72;
    return this.dcnpgettext(null,_6e,_6f,_70,n,_72);
};
Gettext.prototype.dnpgettext=function(_73,_74,_75,_76,n){
    var _78;
    return this.dcnpgettext(_73,_74,_75,_76,n,_78);
};
Gettext.prototype.dcnpgettext=function(_79,_7a,_7b,_7c,n,_7e){
    if(!this.isValidObject(_7b)){
        return "";
    }
    var _7f=this.isValidObject(_7c);
    var _80=this.isValidObject(_7a)?_7a+Gettext.context_glue+_7b:_7b;
    var _81=this.isValidObject(_79)?_79:this.isValidObject(this.domain)?this.domain:"messages";
    var _82="LC_MESSAGES";
    var _7e=5;
    var _83=new Array();
    if(typeof (Gettext._locale_data)!="undefined"&&this.isValidObject(Gettext._locale_data[_81])){
        _83.push(Gettext._locale_data[_81]);
    }else{
        if(typeof (Gettext._locale_data)!="undefined"){
            for(var dom in Gettext._locale_data){
                _83.push(Gettext._locale_data[dom]);
            }
        }
    }
    var _85=[];
    var _86=false;
    var _87;
    if(_83.length){
        for(var i=0;i<_83.length;i++){
            var _89=_83[i];
            if(this.isValidObject(_89.msgs[_80])){
                for(var j=0;j<_89.msgs[_80].length;j++){
                    _85[j]=_89.msgs[_80][j];
                }
                _85.shift();
                _87=_89;
                _86=true;
                if(_85.length>0&&_85[0].length!=0){
                    break;
                }
            }
        }
    }
    if(_85.length==0||_85[0].length==0){
        _85=[_7b,_7c];
    }
    var _8b=_85[0];
    if(_7f){
        var p;
        if(_86&&this.isValidObject(_87.head.plural_func)){
            var rv=_87.head.plural_func(n);
            if(!rv.plural){
                rv.plural=0;
            }
            if(!rv.nplural){
                rv.nplural=0;
            }
            if(rv.nplural<=rv.plural){
                rv.plural=0;
            }
            p=rv.plural;
        }else{
            p=(n!=1)?1:0;
        }
        if(this.isValidObject(_85[p])){
            _8b=_85[p];
        }
    }
    return _8b;
};
Gettext.strargs=function(str,_8f){
    if(null==_8f||"undefined"==typeof (_8f)){
        _8f=[];
    }else{
        if(_8f.constructor!=Array){
            _8f=[_8f];
        }
    }
    var _90="";
    while(true){
        var i=str.indexOf("%");
        var _92;
        if(i==-1){
            _90+=str;
            break;
        }
        _90+=str.substr(0,i);
        if(str.substr(i,2)=="%%"){
            _90+="%";
            str=str.substr((i+2));
        }else{
            if(_92=str.substr(i).match(/^%(\d+)/)){
                var _93=parseInt(_92[1]);
                var _94=_92[1].length;
                if(_93>0&&_8f[_93-1]!=null&&typeof (_8f[_93-1])!="undefined"){
                    _90+=_8f[_93-1];
                }
                str=str.substr((i+1+_94));
            }else{
                _90+="%";
                str=str.substr((i+1));
            }
        }
    }
    return _90;
};
Gettext.prototype.strargs=function(str,_96){
    return Gettext.strargs(str,_96);
};
Gettext.prototype.isArray=function(_97){
    return this.isValidObject(_97)&&_97.constructor==Array;
};
Gettext.prototype.isValidObject=function(_98){
    if(null==_98){
        return false;
    }else{
        if("undefined"==typeof (_98)){
            return false;
        }else{
            return true;
        }
    }
};
Gettext.prototype.sjax=function(uri){
    var _9a;
    if(window.XMLHttpRequest){
        _9a=new XMLHttpRequest();
    }else{
        if(navigator.userAgent.toLowerCase().indexOf("msie 5")!=-1){
            _9a=new ActiveXObject("Microsoft.XMLHTTP");
        }else{
            _9a=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    if(!_9a){
        throw new Error("Your browser doesn't do Ajax. Unable to support external language files.");
    }
    _9a.open("GET",uri,false);
    try{
        _9a.send(null);
    }
    catch(e){
        return;
    }
    var _9b=_9a.status;
    if(_9b==200||_9b==0){
        return _9a.responseText;
    }else{
        var _9c=_9a.statusText+" (Error "+_9a.status+")";
        if(_9a.responseText.length){
            _9c+="\n"+_9a.responseText;
        }
        alert(_9c);
        return;
    }
};
Gettext.prototype.JSON=function(_9d){
    return eval("("+_9d+")");
};

CTSound=function(_1){
    this.sounds=[];
    this.soundPath=_1.soundPath;
    var _2=!!((myAudioTag=document.createElement("audio")).canPlayType);
    var _3=null;
    if(typeof Audio!="undefined"){
        _3=new Audio("");
    }
    this.haveAudio=_3&&!!(_3.canPlayType);
    if(this.haveAudio){
        this.canPlayOgg=(("no"!=_3.canPlayType("audio/ogg"))&&(""!=_3.canPlayType("audio/ogg")));
        this.canPlayMp3=(("no"!=_3.canPlayType("audio/mpeg"))&&(""!=_3.canPlayType("audio/mpeg")));
        this.canPlayWav=(("no"!=_3.canPlayType("audio/wav"))&&(""!=_3.canPlayType("audio/wav")));
    }
};
CTSound.prototype.createSound=function(_4,_5){
    if(!this.haveAudio){
        return;
    }
    var _6=null;
    var _7="";
    if(this.canPlayMp3){
        _7=this.soundPath+"/"+_4+".mp3";
    }else{
        if(this.canPlayOgg){
            _7=this.soundPath+"/"+_4+".ogg";
        }else{
            if(this.canPlayWav){
                _7=this.soundPath+"/"+_4+".wav";
            }
        }
    }
    if(_7){
        _6=new Audio(_7);
    }
    if(_6){
        _6.id=_5+"-"+_4;
        this.sounds[_4]=_6;
        if(_5){
            this.sounds[_5]=_6;
        }
    }
};
CTSound.prototype.playSound=function(_8){
    var _9=this.sounds[_8];
    if(_9){
        _9.play();
    }
};

function getLocale(){
    if(navigator){
        if(navigator.language){
            return navigator.language;
        }else{
            if(navigator.browserLanguage){
                return navigator.browserLanguage;
            }else{
                if(navigator.systemLanguage){
                    return navigator.systemLanguage;
                }else{
                    if(navigator.userLanguage){
                        return navigator.userLanguage;
                    }
                }
            }
        }
    }
}
var gt=null;
function init_gettext(){
    if(typeof json_locale_data!=="undefined"){
        var _1={"domain":"js-messages","locale_data":json_locale_data};
        gt=new Gettext(_1);
    }
}
init_gettext();
function _js(_2){
    if(gt){
        return gt.gettext(_2);
    }else{
        return _2;
    }
}
function __js(_3,a){
    var _3=_js(_3);
    for(var i=0;i<a.length;i++){
        var re=new RegExp("{"+a[i][0]+"}","g");
        _3=_3.replace(re,a[i][1]);
    }
    return _3;
}
function _jn(_7,_8,_9){
    var _a;
    if(gt){
        _a=gt.ngettext(_7,_8,_9);
    }else{
        if(_9==0||_9>1){
            _a=_8;
        }else{
            _a=_7;
        }
    }
    return _a;
}
function __jn(_b,_c,_d,a){
    var _f=_jn(_b,_c,_d);
    return __gt_expand(_f,a);
    return _f;
}
function __gt_expand(msg,a){
    for(var i=0;i<a.length;i++){
        var re=new RegExp("{"+a[i][0]+"}","g");
        msg=msg.replace(re,a[i][1]);
    }
    return msg;
}

PgnViewer=function(_1,_2){
    var _3=new BoardConfig();
    if(_1){
        _3.applyConfig(_1);
    }
    if(!window._pvObject){
        window._pvObject=new Array();
    }
    window._pvObject[_3.boardName]=this;
    _1=_3;
    _1.pgnMode=true;
    _1.scrollVariations=true;
    this.chessapp=new ChessApp(_1);
    this.finishedCallback=_2;
    if(_1.loadImmediately){
        this.chessapp.init(null,null,null,this,true);
        this.board=this.chessapp.board;
    }else{
        YAHOO.util.Event.onDOMReady(this.setup,this,true);
    }
};
PgnViewer.prototype.setup=function(){
    this.chessapp.init(null,null,null,this,true);
    this.board=this.chessapp.board;
};
PgnViewer.prototype.updatePieceCallback=function(_4,_5,_6,_7,_8,_9,_a,_b,_c,_d,_e,_f){
    var _10=new Object();
    var _11=_e;
    var _12=false;
    var _13=Board.getVarMove(_11,_7,_6,_5,_4);
    if(_11.fromColumn==_5.column&&_11.fromRow==_5.row&&_11.toRow==_7&&_11.toColumn==_6&&(_4==""||(_4==_11.promotion))){
        _12=true;
    }else{
        if(_13){
            _11=_13;
            _12=true;
        }
    }
    _10.move=_11;
    _10.allowMove=_12;
    _10.dontMakeOpponentMove=false;
    return _10;
};
PgnViewer.prototype.setupFromPgn=function(pgn,_15){
    this.chessapp.pgn.setupFromPGN(pgn,_15);
};
PgnViewer.prototype.setupFromFen=function(fen,_17,_18,_19){
    this.chessapp.pgn.board.setupFromFen(fen,_17,_18,_19);
};
PGNGame=function(_1a,_1b,_1c,_1d,_1e,_1f,_20,_21,_22,_23,_24,_25){
    this.movesseq=_1a;
    this.startFen=_1b;
    this.blackPlayer=_1c;
    this.whitePlayer=_1d;
    this.pgn_result=_1e;
    this.event=_1f;
    this.site=_20;
    this.date=_21;
    this.round=_22;
    this.start_movenum=_23;
    this.whitePlayerElo=_24;
    this.blackPlayerElo=_25;
};
PGN=function(_26){
    this.board=_26;
    this.pgnGames=new Array();
    this.lastShownGame=0;
};
PGN.prototype.pollPGNFromURL=function(url,_28,_29){
    var _2a=this;
    this.getPGNFromURL(url,_28);
    if(this.foundResult){
        _29=this.board.pollPGNMillisecondsPostResult;
        this.foundResultPolls++;
    }
    if(this.foundResultPolls>=this.board.numberPollsAfterResult){
        this.finishedPolling=true;
        return;
    }
    this.pollTime=_29;
    this.lastPoll=new Date().getTime();
    setTimeout(function(){
        _2a.pollPGNFromURL(url,_28,_29);
    },_29);
};
PGN.prototype.getPGNFromURL=function(url,_2c){
    var _2d=(new Date()).getTime()+"-"+parseInt(Math.random()*99999);
    YAHOO.util.Connect.asyncRequest("GET",url+"?rs="+_2d,{success:function(o){
        var _2f="";
        var _30="";
        var _31="";
        var re=eval("/\\n[^[]/");
        if(o.responseText.indexOf("\r")>=0){
            eval("/\\r[^[]/");
        }
        var ind=o.responseText.search(re);
        if(ind>=0){
            _31=o.responseText.substring(ind);
        }
        re=eval("/\\[Result /");
        ind=o.responseText.search(re);
        if(ind>=0){
            var _34=o.responseText.indexOf("\n",ind);
            if(_34<0){
                _34=o.responseText.indexOf("\r",ind);
            }
            if(_34>=0){
                _2f=o.responseText.substring(ind,_34);
            }
        }
        re=eval("/\\[Site /");
        ind=o.responseText.search(re);
        if(ind>=0){
            var _34=o.responseText.indexOf("]",ind);
            if(_34>=0){
                _30=o.responseText.substring(ind+6,_34-1);
            }
        }
        if(_30){
            if(this.board.fideClock){
                var _35=YAHOO.util.Dom.get(this.board.boardName+"-whitePlayerClock");
                var _36=YAHOO.util.Dom.get(this.board.boardName+"-blackPlayerClock");
                var ss=_30.split("-");
                var _38=ss[0];
                var _39="0";
                if(_38.charAt(0)=="\""){
                    _38=_38.substr(1);
                }
                if(ss.length>1){
                    _39=ss[1];
                }
                if(_35){
                    _35.innerHTML=_38;
                }
                if(_36){
                    _36.innerHTML=_39;
                }
            }else{
                var _3a=YAHOO.util.Dom.get(this.board.boardName+"-site");
                if(_3a){
                    _3a.innerHTML=_30;
                }
            }
        }
        if(this.currentMoveText==_31&&this.currentResultTag==_2f){
            return;
        }
        this.currentMoveText=_31;
        this.currentResultTag=_2f;
        this.setupFromPGN(o.responseText,_2c);
    },failure:function(o){
        if(!this.board.hidePGNErrors){
            alert("pgn load failed:"+o.statusText+" for file:"+url);
        }
    },scope:this},"rs2="+_2d);
};
PGN.prototype.getMoveFromPGNMove=function(_3c,_3d,_3e){
    var _3f=false;
    var _40=false;
    var _41=false;
    var _42;
    var _43=null;
    var _44=false;
    var _45=null;
    if(_3c.charAt(_3c.length-1)=="#"){
        _40=true;
        _3f=true;
        _3c=_3c.substr(0,_3c.length-1);
    }else{
        if(_3c.charAt(_3c.length-1)=="+"){
            _40=true;
            if(_3c.length>1&&_3c.charAt(_3c.length-2)=="+"){
                _3f=true;
                _3c=_3c.substr(0,_3c.length-2);
            }else{
                _3c=_3c.substr(0,_3c.length-1);
            }
        }
    }
    if(_3c=="O-O-O"){
        if(_3d=="w"){
            return this.board.createMoveFromString("e1c1");
        }else{
            return this.board.createMoveFromString("e8c8");
        }
    }else{
        if(_3c=="O-O"){
            if(_3d=="w"){
                return this.board.createMoveFromString("e1g1");
            }else{
                return this.board.createMoveFromString("e8g8");
            }
        }
    }
    var _46=_3c.indexOf("=");
    if(_46>=0){
        var _47;
        _43=_3c.substr(_46+1,1);
        _47=_43.charAt(0);
        _42=this.board.pieceCharToPieceNum(_47);
        _41=true;
        _3c=_3c.substr(0,_46);
    }
    var _48=_3c.charAt(_3c.length-1);
    if(_48=="Q"||_48=="R"||_48=="N"||_48=="B"){
        _43=_48+"";
        _42=this.board.pieceCharToPieceNum(_43);
        _41=true;
        _3c=_3c.substr(0,_3c.length-1);
    }
    var _49=_3c.substr(_3c.length-2,2);
    var _4a=_49.charCodeAt(0)-"a".charCodeAt(0);
    var _4b=_49.charCodeAt(1)-"1".charCodeAt(0);
    if(_4a>7||_4a<0||_4b>7||_4b<0){
        this.lastMoveFromError=__js("Error processing to Square:{TO_SQUARE} on move:{MOVE}",[["TO_SQUARE",_49],["MOVE",_3c]]);
        return null;
    }
    if(_3c.length>2){
        if(_3c.charAt(_3c.length-3)=="x"){
            _44=true;
            _45=_3c.substr(0,_3c.length-3);
        }else{
            _45=_3c.substr(0,_3c.length-2);
        }
    }
    var _4c=new Array();
    var _4d=0;
    var _4e=null;
    var _4f=(_3d=="w")?ChessPiece.WHITE:ChessPiece.BLACK;
    switch(_3c.charAt(0)){
        case "K":
        case "k":
            _4e=ChessPiece.KING;
            break;
        case "Q":
        case "q":
            _4e=ChessPiece.QUEEN;
            break;
        case "R":
        case "r":
            _4e=ChessPiece.ROOK;
            break;
        case "B":
            _4e=ChessPiece.BISHOP;
            break;
        case "N":
        case "n":
            _4e=ChessPiece.KNIGHT;
            break;
        case "P":
        case "p":
            _4e=ChessPiece.PAWN;
            break;
        default:
            _4e=ChessPiece.PAWN;
    }
    var _50=null;
    var _51=null;
    if(_45){
        var _52=_45.toLowerCase().charAt(0);
        if(_52==_45.charAt(0)&&_52>="a"&&_52<="h"){
            _51=_52;
            if(_45.length==2){
                _50=_45.charAt(1);
            }
        }else{
            if(_45.length>1){
                if(_45.length==2){
                    var c=_45.charAt(1);
                    if(c>="1"&&c<="8"){
                        _50=c;
                    }else{
                        _51=c;
                    }
                }else{
                    if(_45.length==3){
                        _51=_45.charAt(1);
                        _50=_45.charAt(2);
                        if(_51>="1"&&_51<="9"){
                            var tmp=_51;
                            _51=_50;
                            _50=tmp;
                        }
                    }else{
                        this.lastMoveFromError=__js("Error: unhandled fromChars:{FROM_CHARS}",[["FROM_CHARS",_45]]);
                        return null;
                    }
                }
            }
        }
    }
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var bp=this.board.boardPieces[i][j];
            if(bp!=null&&bp.colour==_4f&&bp.piece==_4e){
                if(this.board.canMove(bp,_4a,_4b,_3e,true)){
                    var _58=String.fromCharCode("a".charCodeAt(0)+i).charAt(0);
                    var _59=String.fromCharCode("1".charCodeAt(0)+j).charAt(0);
                    if((_51==null||_51==_58)&&(_50==null||_50==_59)){
                        _4c[_4d++]=bp;
                    }else{
                    }
                }
            }
        }
    }
    if(_4d==0){
        this.lastMoveFromError=__js("no candidate pieces for:{MOVE}",[["MOVE",_3c]]);
        return null;
    }
    if(_4d>1){
        this.lastMoveFromError=__js("Ambiguous:{MOVE} with fromChars:{FROM_CHARS} disambigRow:{DISAMBIG_ROW} disambigCol:{DISAMBIG_COL}",[["MOVE",_3c],["FROM_CHARS",_45],["DISAMBIG_ROW",_50],["DISAMBIG_COL",_51]]);
        return null;
    }
    var _5a=_4c[0];
    var _5b="";
    _5b+=String.fromCharCode("a".charCodeAt(0)+_5a.column);
    _5b+=String.fromCharCode("1".charCodeAt(0)+_5a.row);
    if(_44){
        _5b+="x";
    }
    _5b+=_49;
    if(_43){
        _5b+=_43;
    }
    var _5c=this.board.createMoveFromString(_5b);
    return _5c;
};
PGN.prototype.parseTag=function(_5d,pgn,_5f){
    if(pgn.substr(_5f,_5d.length+3)=="["+_5d+" \""){
        var _60=pgn.indexOf("\"",_5f+_5d.length+3);
        if(_60>=0){
            return pgn.substring(_5f+_5d.length+3,_60);
        }
    }
    return null;
};
PGN.prototype.parsePGN=function(pgn,_62,_63){
    if(ctime){
        console.time("parsePGN");
    }
    pgn=pgn.replace(/&nbsp;/g," ");
    pgn=pgn.replace(/^\s+|\s+$/g,"");
    var _64=0;
    this.pgn=pgn;
    var _65=new Array();
    var _66=1;
    var _67=0;
    this.pgnGames=new Array();
    this.finishedParseCallback=_62;
    this.startParseTime=new Date().getTime();
    var ret=this.parsePGN_cont(_65,_66,_67,_64,_63);
    var _69=new Object();
    if(!ret){
        _69.parsedOk=true;
        _69.pgnGames=this.pgnGames;
    }else{
        _69.parsedOk=false;
        _69.errorString=ret;
        _69.pgnGames=null;
    }
    if(ctime){
        console.timeEnd("parsePGN");
    }
    return _69;
};
PGN.prototype.parsePGN_cont=function(_6a,_6b,_6c,_6d,_6e){
    var pgn=this.pgn;
    var _70=this.board.boardName+"-progress";
    var _71=YAHOO.util.Dom.get(_70);
    while(_6d<pgn.length){
        var _72="";
        var _73="";
        var _74="";
        var _75="";
        var _76="";
        var _77="";
        var _78="";
        var _79="?";
        var _7a="?";
        var _7b="w";
        var _7c=0;
        var _7d=0;
        var _7e=new Array();
        var _7f=0;
        var _80="";
        var _81=null;
        var _82=null;
        var _83=new Array();
        var _84=new Array();
        var _85=new Array();
        var _86=new Array();
        var _87=new Array();
        this.board.pieceMoveDisabled=true;
        if(this.board.initialFen){
            this.board.startFen=this.board.initialFen;
        }else{
            this.board.startFen="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        }
        var i=0;
        for(i=_6d;i<pgn.length;i++){
            var tag=this.parseTag("FEN",pgn,i);
            if(tag&&tag!="?"){
                this.board.startFen=tag;
            }else{
                tag=this.parseTag("White",pgn,i);
                if(tag&&tag!="?"){
                    _78=tag;
                }else{
                    tag=this.parseTag("Black",pgn,i);
                    if(tag&&tag!="?"){
                        _73=tag;
                    }else{
                        tag=this.parseTag("Result",pgn,i);
                        if(tag&&tag!="?"){
                            _72=tag;
                        }else{
                            tag=this.parseTag("Event",pgn,i);
                            if(tag&&tag!="?"){
                                _74=tag;
                            }else{
                                tag=this.parseTag("Site",pgn,i);
                                if(tag&&tag!="?"){
                                    _75=tag;
                                }else{
                                    tag=this.parseTag("Date",pgn,i);
                                    if(tag&&tag!="?"){
                                        _76=tag;
                                    }else{
                                        tag=this.parseTag("Round",pgn,i);
                                        if(tag&&tag!="?"){
                                            _77=tag;
                                        }else{
                                            tag=this.parseTag("WhiteElo",pgn,i);
                                            if(tag&&tag!="?"){
                                                _79=tag;
                                            }else{
                                                tag=this.parseTag("BlackElo",pgn,i);
                                                if(tag&&tag!="?"){
                                                    _7a=tag;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(pgn.charAt(i)=="["){
                var j=pgn.indexOf;
                for(j=i+1;j<pgn.length&&pgn.charAt(j)!="]";j++){
                }
                if(j==pgn.length){
                    var err=_js("PgnViewer: Error parsing PGN. Found unclosed [");
                    if(this.finishedParseCallback){
                        this.finishedParseCallback(_6e,err);
                    }
                    return err;
                }
                i=j-1;
                continue;
            }
            if(pgn.charAt(i)=="{"){
                var _8c=pgn.indexOf("}",i+1);
                if(_8c>=0){
                    var _8d=pgn.substring(i+1,_8c);
                    i=_8c;
                    _80+="{ "+_8d+" } ";
                }else{
                    var err=_js("PgnViewer: Error parsing PGN. Found unclosed {");
                    if(this.finishedParseCallback){
                        this.finishedParseCallback(_6e,err);
                    }
                    return err;
                }
                continue;
            }
            if(pgn.substr(i,1)=="."){
                var j=i-1;
                while(j>=0&&pgn.charAt(j)>="0"&&pgn.charAt(j)<="9"){
                    j--;
                }
                j++;
                if(pgn.charAt(j)>="0"&&pgn.charAt(j)<="9"){
                    _6b=parseInt(pgn.substring(j,i));
                }
                break;
            }
        }
        if(pgn.substr(i,1)!="."){
        }
        this.board.prev_move=null;
        this.board.setupFromFen(this.board.startFen,false,false,true,true);
        _81=this.board.prev_move;
        var _8e=i;
        var _8f=null;
        for(i=i;i<pgn.length;i++){
            var _90=-1;
            if(pgn.substr(i,3)=="1-0"||pgn.substr(i,3)=="0-1"){
                _90=3;
            }else{
                if(pgn.substr(i,7)=="1/2-1/2"){
                    _90=7;
                }else{
                    if(pgn.substr(i,1)=="*"){
                        _90=1;
                    }
                }
            }
            if(_90>0){
                _8f=pgn.substr(i,_90);
                _6d=i+_90;
                break;
            }
            if(pgn.charAt(i)=="["){
                _6d=i;
                break;
            }
            if(pgn.charAt(i)==" "||pgn.charAt(i)=="\t"||pgn.charAt(i)=="\n"||pgn.charAt(i)=="\r"){
                _8e=i+1;
                continue;
            }
            if(pgn.charAt(i)>="0"&&pgn.charAt(i)<="9"){
                continue;
            }
            if(pgn.charAt(i)=="."){
                var _91=pgn.substring(_8e,i).replace(/^\s+|\s+$/g,"");
                _8e=i;
                while(i+1<pgn.length&&pgn.charAt(i+1)=="."){
                    i++;
                }
                if(_8e!=i){
                    _7b="b";
                }else{
                    _7b="w";
                }
                _8e=i+1;
            }else{
                if(pgn.charAt(i)=="{"){
                    var _8c=pgn.indexOf("}",i+1);
                    if(_8c>=0){
                        var _8d=pgn.substring(i+1,_8c);
                        i=_8c;
                        _80+="{ "+_8d+" } ";
                    }
                    _8e=i+1;
                }else{
                    if(pgn.charAt(i)=="("){
                        _83[_7c]=this.board.boardPieces;
                        _84[_7c]=_7b;
                        _86[_7c]=_81;
                        _87[_7c]=_82;
                        this.board.boardPieces=_85[_7c];
                        this.board.boardPieces=this.board.copyBoardPieces(false);
                        _81=_82;
                        _7c++;
                        _8e=i+1;
                        _80+="( ";
                    }else{
                        if(pgn.charAt(i)==")"){
                            boardPool.putObject(_83[_7c]);
                            _7c--;
                            this.board.boardPieces=_83[_7c];
                            _7b=_84[_7c];
                            _81=_86[_7c];
                            _82=_87[_7c];
                            _8e=i+1;
                            _80+=") ";
                        }else{
                            if(pgn.charAt(i)=="$"){
                                var j;
                                for(j=i+1;j<pgn.length&&pgn.charAt(j)>="0"&&pgn.charAt(j)<="9";j++){
                                }
                                j--;
                                if(j>i){
                                    var _92=parseInt(pgn.substr(i+1,j+1));
                                    if(_92<=9){
                                        switch(_92){
                                            case 1:
                                                _80=_80.substr(0,_80.length-1)+"! ";
                                                break;
                                            case 2:
                                                _80=_80.substr(0,_80.length-1)+"? ";
                                                break;
                                            case 3:
                                                _80=_80.substr(0,_80.length-1)+"!! ";
                                                break;
                                            case 4:
                                                _80=_80.substr(0,_80.length-1)+"?? ";
                                                break;
                                            case 5:
                                                _80=_80.substr(0,_80.length-1)+"!? ";
                                                break;
                                            case 6:
                                                _80=_80.substr(0,_80.length-1)+"?! ";
                                                break;
                                            case 7:
                                            case 8:
                                            case 9:
                                            case 0:
                                            default:
                                        }
                                    }else{
                                        _80+=pgn.substring(i,j+1)+" ";
                                    }
                                    i=j;
                                }
                                continue;
                            }else{
                                var _93=-1;
                                for(var j=i+1;j<pgn.length;j++){
                                    if(pgn.charAt(j)==")"||pgn.charAt(j)=="("||pgn.charAt(j)=="{"||pgn.charAt(j)=="}"||pgn.charAt(j)==" "||pgn.charAt(j)=="\t"||pgn.charAt(j)=="\n"||pgn.charAt(j)=="\r"){
                                        _93=j;
                                        break;
                                    }
                                }
                                if(_93==-1){
                                    _93=pgn.length;
                                }
                                var _94=_8e;
                                var _95=pgn.substring(_8e,_93).replace(/^\s+|\s+$/g,"");
                                _8e=_93;
                                i=_8e-1;
                                if(_95.length>=4&&_95.substring(0,4)=="e.p."){
                                    continue;
                                }
                                if(_95.length==0){
                                    var err=__js("PgnViewer: Error: got empty move endMoveInd:{ENDMOVE_INDEX} upto:{UPTO} from:{FROM}",[["ENDMOVE_INDEX",_93],["UPTO",_94],["FROM",pgn.substr(_94)]]);
                                    if(this.finishedParseCallback){
                                        this.finishedParseCallback(_6e,err);
                                    }
                                    return err;
                                }
                                var _96=_95.length-1;
                                while(_96>=0){
                                    if(_95.charAt(_96)=="?"){
                                        _96--;
                                    }else{
                                        if(_95.charAt(_96)=="!"){
                                            _96--;
                                        }else{
                                            break;
                                        }
                                    }
                                }
                                var _97=_95.substring(0,_96+1);
                                var _98=this.getMoveFromPGNMove(_97,_7b,_81);
                                if(_98==null){
                                    _80+="unknown ";
                                    var err=__js("PgnViewer: Error parsing:{MOVE}, {ERROR_REASON}",[["MOVE",_95],["ERROR_REASON",this.lastMoveFromError]]);
                                    if(this.finishedParseCallback){
                                        this.finishedParseCallback(_6e,err);
                                    }
                                    return err;
                                }
                                _82=_81;
                                _81=_98;
                                var _99=this.board.boardPieces[_98.fromColumn][_98.fromRow];
                                boardPool.putObject(_85[_7c]);
                                _85[_7c]=this.board.copyBoardPieces(false);
                                if(_99){
                                    this.board.makeMove(_98,_99,false,0.5,false,false);
                                }
                                _7d=_7c;
                                _7f++;
                                _7b=this.board.flipToMove(_7b);
                                _80+=_98.moveString+"|"+_95+" ";
                            }
                        }
                    }
                }
            }
        }
        if(_6d<i){
            _6d=i;
        }
        var _9a=pgn.indexOf("{",_6d);
        var _9b=pgn.indexOf("[",_6d);
        if(_9a>=0){
            if(_9b==-1||_9a<_9b){
                var _9c=pgn.indexOf("}",_9a+1);
                if(_9c>=0){
                    var _8d=pgn.substring(_9a+1,_9c);
                    _6d=_9c+1;
                    _80+="{ "+_8d+" } ";
                }else{
                    var err=_js("PgnViewer: Error: Unclosed {");
                    if(this.finishedParseCallback){
                        this.finishedParseCallback(_6e,err);
                    }
                    return err;
                }
            }
        }
        _80=_80.replace(/^\s+|\s+$/g,"");
        this.board.pieceMoveDisabled=false;
        if(_8f!=null){
            if(_72.length==0||_72=="?"){
                _72=_8f;
            }
        }
        if(this.board.ignoreMultipleGames){
            if(_8f&&_72&&_72=="*"&&_8f!="*"&&_8f!="?"&&_8f!=""){
                _72=_8f;
            }
        }
        this.pgnGames[_6c++]=new PGNGame(_80,this.board.startFen,_73,_78,_72,_74,_75,_76,_77,_6b,_79,_7a);
        if(_71){
            _71.innerHTML="Loaded "+_6c+" games";
        }
        if(this.board.ignoreMultipleGames){
            break;
        }
        if(this.finishedParseCallback&&new Date().getTime()-this.startParseTime>500){
            this.startParseTime=new Date().getTime();
            setTimeout("window._pvObject[\""+this.board.boardName+"\"].chessapp.pgn.parsePGN_cont(\""+_6a+"\",\""+_6b+"\",\""+_6c+"\",\""+_6d+"\","+_6e+");",0);
            return;
        }
    }
    if(this.finishedParseCallback){
        this.finishedParseCallback(_6e);
    }
    return false;
};
PGN.prototype.setupFromPGN=function(pgn,_9e){
    this.parsePGN(pgn,this.setupFromPGNCallback,_9e);
};
PGN.prototype.setupFromPGNCallback=function(_9f,err){
    var _a1=this.board.boardName+"-progress";
    var _a2=YAHOO.util.Dom.get(_a1);
    if(err){
        if(!this.board.hidePGNErrors){
            var _a3=YAHOO.util.Dom.get(this.board.boardName+"-pgnError");
            if(_a3){
                _a3.innerHTML=err;
            }else{
                alert(err);
            }
        }
        return false;
    }
    if(this.pgnGames.length==0){
        if(!this.board.hidePGNErrors){
            alert("PgnViewer: Error: Unable to find any pgn games in:"+pgn);
        }
        return false;
    }
    if(this.pgnGames.length==1||this.board.ignoreMultipleGames){
        var _a4=0;
        if(_9f){
            _a4=-1;
        }
        this.showGame(0,_a4);
    }else{
        var _a5=this.board.boardName+"-container";
        var _a6=YAHOO.util.Dom.get(_a5);
        var _a7=YAHOO.util.Dom.get(this.board.boardName+"-problemSelector");
        var _a8=document.createElement("div");
        var _a9="<form id=\""+this.board.boardName+"-problemSelectorForm\" action=\"\" method=\"\">";
        var _aa="<select id=\""+this.board.boardName+"-problemSelector\" name=\""+this.board.boardName+"-problemSelector\" style=\"width: "+this.board.pieceSize*8+"px;\">";
        var _ab="";
        for(i=0;i<this.pgnGames.length;i++){
            var _ac=this.pgnGames[i];
            var _ad=this.board.boardName+"-game-"+i;
            var _ae=(i+1)+". "+_ac.whitePlayer+" vs "+_ac.blackPlayer;
            if(_ac.pgn_result.length>0&&_ac.pgn_result!="?"&&this.board.showResult==1){
                _ae+=" "+_ac.pgn_result;
            }
            if(_ac.event.length>0&&_ac.event!="?"&&this.board.showEvent==1){
                _ae+=" "+_ac.event;
            }
            if(_ac.round.length>0&&_ac.round!="?"&&this.board.showRound==1){
                _ae+=" Rnd:"+_ac.round;
            }
            if(_ac.site.length>0&&_ac.site!="?"&&this.board.showSite==1){
                _ae+=" "+_ac.site;
            }
            if(_ac.date.length>0&&_ac.date!="?"&&this.board.showDate==1){
                _ae+=" "+_ac.date;
            }
            var sel="";
            if(i==this.lastShownGame){
                sel="selected=\"\"";
            }
            _ab+="<option "+sel+" id=\""+_ad+"\" value=\""+i+"\">"+_ae+"</option>";
        }
        if(_a7){
            if(this.board.selectorBody!=_ab){
                _a7.innerHTML=_ab;
                this.board.selectorBody=_ab;
            }
        }else{
            _a9+=_aa+_ab+"</select></form>";
            _a8.innerHTML=_a9;
            _a6.insertBefore(_a8,_a6.firstChild);
            this.board.selectorBody=_ab;
        }
        var _a7=YAHOO.util.Dom.get(this.board.boardName+"-problemSelector");
        YAHOO.util.Event.addListener(_a7,"change",this.selectGame,this,true);
        var _a4=0;
        var _b0=0;
        if(_9f){
            _a4=-1;
            _b0=this.lastShownGame;
        }
        this.showGame(_b0,_a4);
    }
    if(_a2){
        YAHOO.util.Dom.setStyle(_a2,"visibility","hidden");
    }
    if(window._pvObject[this.board.boardName].finishedCallback){
        window._pvObject[this.board.boardName].finishedCallback();
    }
    return;
};
PGN.prototype.selectGame=function(e){
    var _b2=YAHOO.util.Event.getTarget(e).selectedIndex;
    var _b3=0;
    if(this.board.gotoEndOnRefresh){
        _b3=-1;
    }
    this.showGame(_b2,_b3);
    var _b4=this.board.boardName+"-piecestaken";
    var _b5=YAHOO.util.Dom.get(_b4);
    if(_b5){
        _b5.innerHTML="";
    }
    this.board.resetMoveListScrollPosition();
};
PGN.prototype.showGame=function(_b6,_b7){
    _b7=(typeof _b7=="undefined")?0:_b7;
    var _b8=this.lastShownGame;
    this.lastShownGame=_b6;
    var _b9=this.board.moveArray;
    var _ba=this.board.currentMove;
    var _bb=false;
    if(_ba&&_ba.atEnd){
        _bb=true;
    }
    var _bc=this.pgnGames[_b6];
    var _bd=_bc.pgn_result;
    if(_bd&&(_bd=="1/2-1/2"||_bd=="0-1"||_bd=="1-0")){
        this.foundResult=true;
    }else{
        this.foundResult=false;
        this.foundResultPolls=0;
    }
    this.board.startFen=_bc.startFen;
    this.board.setupFromFen(_bc.startFen,false,false,false);
    this.board.setMoveSequence(_bc.movesseq,"NA",_bc.start_movenum,_bc.pgn_result);
    var _be=true;
    var _bf=-1;
    if(_b6==_b8&&_bb){
        _bf=this.board.moveArray.length-1;
    }
    if(!Move.moveArraysEqual(_b9,this.board.moveArray)){
        _be=false;
    }else{
        var _c0=Move.findMoveInNewArray(_b9,this.board.moveArray,_ba);
        if(_c0&&_c0.prev){
            _bf=_c0.prev.index;
        }
    }
    this.board.displayPendingMoveList();
    if(this.board.moveArray.length>0){
        this.board.setCurrentMove(this.board.moveArray[0]);
    }
    if(_be){
        if(_bf>0&&_bf<this.board.moveArray.length){
            if(clog){
                console.log("going to currMoveIndex:"+_bf);
            }
            this.board.gotoMoveIndex(_bf,false,true);
        }else{
        }
    }else{
        if(_b7==-1){
            var _c1=this.board.moveArray.length-1;
            if(_c1>=0){
                this.board.gotoMoveIndex(_c1,false,true);
            }
        }else{
            if(_b7!=0){
                this.board.gotoMoveIndex(_b7);
            }
        }
        if(_b7!=-1&&this.board.autoplayFirst){
            this.board.forwardMove();
        }
    }
    this.board.displayMode=true;
    var _c2=this.board.boardName;
    var _c3=YAHOO.util.Dom.get(_c2+"-whitePlayer");
    if(_c3){
        _c3.innerHTML=_bc.whitePlayer;
    }
    var _c4=YAHOO.util.Dom.get(_c2+"-blackPlayer");
    if(_c4){
        _c4.innerHTML=_bc.blackPlayer;
    }
    var _c5=YAHOO.util.Dom.get(_c2+"-event");
    if(_c5){
        _c5.innerHTML=_bc.event;
    }
    var _c6=YAHOO.util.Dom.get(_c2+"-site");
    if(_c6){
        _c6.innerHTML=_bc.site;
    }
    var _c7=YAHOO.util.Dom.get(_c2+"-date");
    if(_c7){
        _c7.innerHTML=_bc.date;
    }
    var _c8=YAHOO.util.Dom.get(_c2+"-round");
    if(_c8){
        _c8.innerHTML=_bc.round;
    }
    var _c9=YAHOO.util.Dom.get(_c2+"-whiteElo");
    if(_c9){
        _c9.innerHTML=_bc.whitePlayerElo;
    }
    var _ca=YAHOO.util.Dom.get(_c2+"-blackElo");
    if(_ca){
        _ca.innerHTML=_bc.blackPlayerElo;
    }
    var _cb=YAHOO.util.Dom.get(_c2+"-result");
    if(_cb){
        _cb.innerHTML=_bc.pgn_result;
    }
    if(clog){
        if(this.board.currentMove){
            console.log("after show game currentMove:"+this.board.currentMove.output());
        }else{
            console.log("after show game currentMove is null");
        }
    }
};

var SITE_VERSION=1;
var clog=false;
var ctime=false;
var cprof=false;
var move_obj_id_counter=0;
var activeBoard=null;
var boardSounds=new CTSound({soundPath:"/static/sounds"});
YAHOO.util.Event.onDOMReady(function(){
    boardSounds.createSound("takesounds/78263__SuGu14__Metall01","takePiece1");
    boardSounds.createSound("movesounds/77971__SuGu14__Fusta_0_05","movePiece3");
    boardSounds.createSound("movesounds/10537__batchku__Hit_knuckle_15_004","movePiece7");
    boardSounds.createSound("analysis/76426__spazzo_1493__Finished","finished");
});
function isMouseOver(_1,e){
    var el=YAHOO.util.Dom.get(_1);
    if(!el){
        return false;
    }
    var _4=YAHOO.util.Dom.getRegion(el);
    if(!_4){
        return false;
    }
    var _5=_4.top;
    var _6=_4.left;
    var _7=_4.bottom;
    var _8=_4.right;
    var _9=YAHOO.util.Event.getXY(e);
    var mX=_9[0];
    var mY=_9[1];
    var _c=(mX>_6&&mX<_8&&mY>_5&&mY<_7);
}
function trimStr(_d){
    if(!_d){
        return "";
    }
    var _d=_d.replace(/^\s\s*/,"");
    var ws=/\s/;
    var i=_d.length;
    while(ws.test(_d.charAt(--i))){
    }
    return _d.slice(0,i+1);
}
BoardConfig=function(){
    this.boardName="board";
    this.puzzle=false;
    this.showToMoveIndicators=false;
    this.scrollVariations=false;
    this.pgnString=null;
    this.pgnDiv=null;
    this.pgnFile=null;
    this.scrollOffsetCorrection=0;
    this.handleCommentClicks=false;
    this.pollPGNMilliseconds=0;
    this.pollPGNMillisecondsPostResult=30000;
    this.numberPollsAfterResult=5;
    this.gotoEndOnRefresh=false;
    this.allowPreMoveSelection=false;
    this.pieceSet="merida";
    this.pieceSize=46;
    this.isEndgame=false;
    this.tr=false;
    this.ie6FixCoordsOffsetSize=4;
    this.allIeFixCoordsOffsetSize=0;
    this.addVersion=true;
    this.ignoreMultipleGames=false;
    this.ml=9999;
    this.r=false;
    this.g=false;
    this.g2=false;
    this.canPasteFen=false;
    this.makeActive=false;
    this.showSolutionButton=false;
    this.avoidMouseoverActive=false;
    this.autoScrollMoves=true;
    this.moveAnimationLength=0.5;
    this.showBracketsOnVariation=true;
    this.hideBracketsOnTopLevelVariation=false;
    this.variationStartString=" ( ";
    this.variationEndString=" ) ";
    this.ignoreCommentRegex=null;
    this.newlineForEachMainMove=true;
    this.useDivClearForNewline=false;
    this.showNPS=false;
    this.squareColorClass="";
    this.analysisWindowName="analysis_window";
    this.pieceTakenSize=this.pieceSize;
    this.pauseBetweenMoves=800;
    this.pgnMode=false;
    this.hidePGNErrors=false;
    this.previewMode=false;
    this.movesFormat="default";
    this.boardImagePath="";
    this.showCoordinates=false;
    this.highlightFromTo=false;
    this.highlightValidSquares=false;
    this.fideClock=false;
    this.disableFlipper=false;
    this.showResult=1;
    this.showEvent=1;
    this.showRound=1;
    this.showSite=1;
    this.showDate=1;
    this.ignoreFlipping=false;
    this.reverseFlip=false;
    this.autoplayFirst=false;
    this.dontOutputNavButtons=false;
    this.dontCheckLeavingPage=false;
    this.clickAndClick=false;
    this.clickAndClickDisabled=false;
    this.whiteMoveSoundName="movePiece3";
    this.blackMoveSoundName="movePiece7";
    this.whiteTakeSoundName="takePiece1";
    this.blackTakeSoundName="takePiece1";
    this.finishedSoundName="finished";
    this.soundEnabled=false;
    this.gamedb=false;
};
BoardConfig.prototype.applyConfig=function(_10){
    for(var _11 in _10){
        this[_11]=_10[_11];
    }
};
ChessApp=function(_12){
    this.displayMode=false;
    this.config=_12;
    this.board=null;
};
ChessApp.prototype.setDisplayMode=function(_13){
    this.displayMode=_13;
};
ChessApp.prototype.setProblemNumber=function(_14,_15){
    this.problemNumber=_14;
    this.attId=_15;
};
ChessApp.prototype.init=function(e,_17,_18,us,_1a){
    ChessPiece.init();
    this.board=new Board(this.config.boardName);
    if(_1a){
        this.board.addUpdatePieceListener(us);
    }
    this.board.moveArray=new Array();
    if(!this.hideOnInit){
        YAHOO.util.Dom.setStyle(this.config.boardName+"-container","display","block");
        YAHOO.util.Dom.setStyle("toPlaySpan","display","inline");
    }
    this.tactics=(this.displayMode||this.config.pgnMode||this.config.previewMode||this.config.fenBoard)?null:new TacticsUI(this.board);
    this.problem=(this.config.pgnMode||this.config.previewMode||this.config.fenBoard)?null:new ProblemUI(this.board,this.tactics);
    this.board.tactics=this.tactics;
    this.board.problem=this.problem;
    this.board.puzzle=this.config.puzzle;
    if(this.problem){
        this.problem.autoPlayOpponent=1;
    }
    this.pgn=(this.config.pgnMode)?new PGN(this.board):null;
    var _1b=MovesDisplay.DEFAULT_DISPLAY_TYPE;
    if(this.config.movesFormat=="main_on_own_line"){
        _1b=MovesDisplay.MAIN_ON_OWN_LINE;
    }
    this.movesDisplay=new MovesDisplay(this.board,_1b);
    this.movesDisplay.variationOnOwnLine=this.config.variationOnOwnLine;
    this.board.movesDisplay=this.movesDisplay;
    this.board.boardImagePath=this.config.boardImagePath;
    this.board.showNPS=this.config.showNPS;
    this.board.showSolutionButton=this.config.showSolutionButton;
    this.board.analysisWindowName=this.config.analysisWindowName;
    this.board.squareColorClass=this.config.squareColorClass;
    this.board.tr=this.config.tr;
    this.board.scrollToBoardTop=this.config.scrollToBoardTop;
    this.board.ml=this.config.ml;
    this.board.r=this.config.r;
    this.board.g=this.config.g;
    this.board.g2=this.config.g2;
    this.board.canPasteFen=this.config.canPasteFen;
    this.board.addVersion=this.config.addVersion;
    this.board.ignoreMultipleGames=this.config.ignoreMultipleGames;
    this.board.ie6FixCoordsOffsetSize=this.config.ie6FixCoordsOffsetSize;
    this.board.allIeFixCoordsOffsetSize=this.config.allIeFixCoordsOffsetSize;
    this.board.allowingFreeMovement=this.config.allowingFreeMovement;
    this.board.autoScrollMoves=this.config.autoScrollMoves;
    this.board.moveAnimationLength=this.config.moveAnimationLength;
    this.board.showBracketsOnVariation=this.config.showBracketsOnVariation;
    this.board.hideBracketsOnTopLevelVariation=this.config.hideBracketsOnTopLevelVariation;
    this.board.variationStartString=this.config.variationStartString;
    this.board.variationEndString=this.config.variationEndString;
    this.board.ignoreCommentRegex=this.config.ignoreCommentRegex;
    this.board.newlineForEachMainMove=this.config.newlineForEachMainMove;
    this.board.useDivClearForNewline=this.config.useDivClearForNewline;
    this.board.pieceSize=this.config.pieceSize;
    this.board.showToMoveIndicators=this.config.showToMoveIndicators;
    this.board.handleCommentClicks=this.config.handleCommentClicks;
    this.board.scrollOffsetCorrection=this.config.scrollOffsetCorrection;
    this.board.pollPGNMilliseconds=this.config.pollPGNMilliseconds;
    this.board.pollPGNMillisecondsPostResult=this.config.pollPGNMillisecondsPostResult;
    this.board.numberPollsAfterResult=this.config.numberPollsAfterResult;
    this.board.gotoEndOnRefresh=this.config.gotoEndOnRefresh;
    this.board.allowPreMoveSelection=this.config.allowPreMoveSelection;
    this.board.pieceTakenSize=this.config.pieceTakenSize;
    this.board.pieceSet=this.config.pieceSet;
    this.board.pauseBetweenMoves=this.config.pauseBetweenMoves;
    this.board.showCoordinates=this.config.showCoordinates;
    this.board.highlightFromTo=this.config.highlightFromTo;
    this.board.highlightValidSquares=this.config.highlightValidSquares;
    this.board.fideClock=this.config.fideClock;
    this.board.disableFlipper=this.config.disableFlipper;
    this.board.showDate=this.config.showDate;
    this.board.showEvent=this.config.showEvent;
    this.board.showGame=this.config.showGame;
    this.board.showResult=this.config.showResult;
    this.board.showRound=this.config.showRound;
    this.board.showSite=this.config.showSite;
    this.board.ignoreFlipping=this.config.ignoreFlipping;
    this.board.reverseFlip=this.config.reverseFlip;
    this.board.autoplayFirst=this.config.autoplayFirst;
    this.board.scrollVariations=this.config.scrollVariations;
    this.board.dontOutputNavButtons=this.config.dontOutputNavButtons;
    this.board.clickAndClick=this.config.clickAndClick;
    this.board.clickAndClickDisabled=this.config.clickAndClickDisabled;
    this.board.avoidMouseoverActive=this.config.avoidMouseoverActive;
    this.board.dontCheckLeavingPage=this.config.dontCheckLeavingPage;
    this.board.whiteMoveSoundName=this.config.whiteMoveSoundName;
    this.board.whiteTakeSoundName=this.config.whiteTakeSoundName;
    this.board.blackMoveSoundName=this.config.blackMoveSoundName;
    this.board.blackTakeSoundName=this.config.blackTakeSoundName;
    this.board.soundEnabled=this.config.soundEnabled;
    this.board.hidePGNErrors=this.config.hidePGNErrors;
    this.board.gamedb=this.config.gamedb;
    if(this.config.makeActive){
        activeBoard=this.board;
    }
    if(this.problem){
        this.problem.isEndgame=this.config.isEndgame;
    }
    if(!this.board.puzzle&&typeof loginManager!="undefined"){
        if(this.tactics){
            loginManager.setLoginCallback(this.tactics.loginCallback,this.tactics);
            loginManager.setLogoutCallback(this.tactics.logoutCallback,this.tactics);
        }
        if(this.problem){
            loginManager.setSessionCallback(this.problem.sessionCallback,this.problem);
        }
    }
    YAHOO.util.DragDropMgr.clickTimeThresh=50;
    YAHOO.util.DragDropMgr.clickPixelThresh=1;
    this.board.createBoardUI();
    if(!this.board.puzzle){
        if(this.problem){
            this.problem.createProblemUI();
        }
        if(this.tactics){
            this.tactics.initProblemCompleteOverlay();
        }
        if(this.problem){
            this.problem.initLoadingOverlay();
        }
        if(this.config.pgnMode){
            if(this.config.pgnFile){
                if(this.config.pollPGNMilliseconds){
                    this.pgn.foundResult=false;
                    this.pgn.foundResultPolls=0;
                    var _1c=this;
                    function timeToNextPollDisplay(){
                        var _1d=YAHOO.util.Dom.get(_1c.config.boardName+"-nextUpdate");
                        if(_1d){
                            if(_1c.pgn.finishedPolling||_1c.pgn.foundResult){
                                var _1e="00";
                                var _1f="00";
                                _1d.innerHTML="<span id=\"minutes\">"+_1e+"</span>:<span id=\"seconds\">"+_1f+"</span>";
                            }else{
                                var _20=new Date().getTime();
                                var _21=(_1c.pgn.lastPoll+_1c.pgn.pollTime-_20)/1000;
                                if(_21<0){
                                    _21=0;
                                }
                                var _22=_21;
                                var _23=parseInt(_22/60);
                                var _24=parseInt(_22%60);
                                if(_23<10){
                                    _1e="0"+_23;
                                }else{
                                    _1e=_23;
                                }
                                if(_24<10){
                                    _1f="0"+_24;
                                }else{
                                    _1f=_24;
                                }
                                _1d.innerHTML="<span id=\"minutes\">"+_1e+"</span>:<span id=\"seconds\">"+_1f+"</span>";
                                setTimeout(timeToNextPollDisplay,1000);
                            }
                        }
                    }
                    this.pgn.pollTime=this.config.pollPGNMilliseconds;
                    this.pgn.pollPGNFromURL(this.config.pgnFile,this.config.gotoEndOnRefresh,this.config.pollPGNMilliseconds);
                    setTimeout(timeToNextPollDisplay,1000);
                }else{
                    this.pgn.getPGNFromURL(this.config.pgnFile,this.config.gotoEndOnRefresh);
                }
            }else{
                if(this.config.pgnString){
                    this.pgn.setupFromPGN(this.config.pgnString);
                }else{
                    if(this.config.pgnDiv){
                        var _25=YAHOO.util.Dom.get(this.config.pgnDiv);
                        if(_25){
                            this.pgn.setupFromPGN(_25.innerHTML);
                        }
                    }
                }
            }
        }else{
            if(!this.board.dontCheckLeavingPage&&this.tactics){
                YAHOO.util.Event.addListener(window,"beforeunload",this.tactics.checkLeavingPage,this.tactics,true);
                YAHOO.util.Event.addListener(window,"unload",this.tactics.leavingPage,this.tactics,true);
                this.tactics.updateSessionDisplay(0,0);
                if(typeof showingStart!="undefined"&&showingStart){
                    var _1c=this;
                    var _26="";
                    if(loggedIn){
                        if(this.config.isEndgame){
                            _26=_js("Endgame Problem Set")+": <span id=\"startProblemSetStr\">"+_js(startEndgameSetName)+"</span>";
                        }else{
                            _26=_js("Tactics Problem Set")+": <span id=\"startProblemSetStr\">"+_js(startTacticsSetName)+"</span>";
                        }
                    }
                    this.board.preloadPieces();
                    var _27=new YAHOO.widget.SimpleDialog("starttacticdialog1",{width:"300px",fixedcenter:true,modal:false,visible:true,draggable:true,close:false,text:"<div style=\"color:black\">"+_26+"</div><br/>"+"<div style=\"color:black\">"+_js("Click start to begin solving problems")+"</div>",icon:YAHOO.widget.SimpleDialog.ICON_INFO,constraintoviewport:true,buttons:[{text:_js("Start"),handler:function(){
                        if(_1c.board.imagesLoaded){
                            this.hide();
                            _1c.problem.getProblem();
                        }else{
                            var _28=_js("Still trying to load piece images.\n If you keep receiving this message you may need to reload the page.\n If you continue to get this message, you can disable it by going into your preferences and turning 'show problem start dialog' (available under the other tab) off.");
                            alert(_28);
                        }
                    },isDefault:true}]});
                    var _29=YAHOO.util.Dom.get("ctb-"+this.board.boardName);
                    _27.render(document.body);
                }else{
                    this.problem.getProblem();
                }
            }else{
                if(this.problem){
                    if(this.problemNumber!=""){
                        YAHOO.util.Dom.setStyle("boardandmoves","display","block");
                        this.problem.getProblem(this.problemNumber,this.attId);
                    }
                }
            }
        }
    }
    this.board.setupEventHandlers();
    if(this.problem){
        this.problem.setupEventHandlers();
    }
    if(this.tactics){
        this.tactics.setupEventHandlers();
    }
    if(this.board.scrollToBoardTop){
        var xy=YAHOO.util.Dom.getXY(this.board.boardName+"-boardBorder");
        window.scrollTo(xy[0],xy[1]);
    }
    if(this.config.flipListener){
        this.board.addFlipListener(this.config.flipListener);
    }
};
function clearClone(o){
    if(o==null){
        return;
    }
    for(prop in o){
        if(typeof (o[prop])=="object"&&o[prop]!=null&&o[prop].alreadyCloned){
            o[prop].alreadyCloned=false;
            clearClone(o[prop]);
        }
    }
}
function cloneWork(o){
    if(o==null){
        return null;
    }
    var _2d=new Object();
    for(prop in o){
        if(typeof (o[prop])=="object"){
            _2d[prop]=o[prop];
        }else{
            _2d[prop]=o[prop];
        }
    }
    return _2d;
}
function clone(o){
    return cloneWork(o);
}
get_image_str=function(_2f,_30,_31,_32,_33){
    var _34=".vers"+SITE_VERSION;
    if(!_33){
        _34="";
    }
    if(check_bad_msie()){
        return _30+"/static/images/"+_31+"/"+_2f+_32+_34+".png";
    }else{
        return _30+"/static/images/"+_31+"/"+_2f+_32+_34+".png";
    }
};
check_bad_msie=function(){
    var _35=(window.ActiveXObject&&(typeof document.body.style.maxHeight=="undefined"));
    return _35;
};
fix_ie_png=function(img){
    if(!check_bad_msie()){
        return;
    }
    var _37=(img.id)?"id='"+img.id+"' ":"";
    var _38=(img.className)?"class='"+img.className+"' ":"";
    var _39=(img.title)?"title='"+img.title+"' ":"title='"+img.alt+"' ";
    var _3a="display:inline-block;"+img.style.cssText;
    if(img.align=="left"){
        _3a="float:left;"+_3a;
    }
    if(img.align=="right"){
        _3a="float:right;"+_3a;
    }
    if(img.parentElement.href){
        _3a="cursor:hand;"+_3a;
    }
    var _3b="<span "+_37+_38+_39+" style=\""+"width:"+img.width+"px; height:"+img.height+"px;"+_3a+";"+"filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"+"(src='"+img.src+"', sizingMethod='image');\"></span>";
    img.outerHTML=_3b;
};
Move=function(_3c,_3d,_3e,_3f,_40,_41,_42){
    this.fromColumn=_3c;
    this.fromRow=_3d;
    this.toColumn=_3e;
    this.toRow=_3f;
    this.take=_40;
    this.promotion=_41;
    this.moveString=_42;
    this.prev=null;
    this.next=null;
    this.numVars=0;
    this.prevMoveEnpassant=false;
    this.ravLevel=0;
    this.atEnd=false;
    this.obj_id=move_obj_id_counter++;
    this.beforeComment="";
    this.afterComment="";
};
Move.prototype.freeMove=function(){
    if(this.taken){
        this.taken=null;
    }
    if(this.vars&&this.vars.length>0){
        var i=0;
        for(var i=0;i<this.vars.length;i++){
            this.vars[i].freeMove();
        }
    }
};
Move.prototype.clone=function(_44){
    var _45=this.take;
    if(_44&&_45){
        _45=_45.makeLightWeight();
    }
    var _46=new Move(this.fromColumn,this.fromRow,this.toColumn,this.toRow,_45,this.promotion,this.moveString);
    _46.moveNum=this.moveNum;
    _46.atEnd=this.atEnd;
    _46.beforeComment=this.beforeComment;
    _46.afterComment=this.afterComment;
    _46.prevMoveEnpassant=this.prevMoveEnpassant;
    _46.index=this.index;
    if(this.vars){
        _46.vars=[];
        var cnt=0;
        for(var i=0;i<this.vars.length;i++){
            _46.vars[i]=this.vars[i].clone(_44);
            cnt++;
        }
        _46.numVars=cnt;
    }
    return _46;
};
Move.columnToChar=function(col){
    var a=String.fromCharCode("a".charCodeAt(0)+col);
    return a;
};
Move.prototype.output=function(){
    return Move.columnToChar(this.fromColumn)+""+(this.fromRow+1)+":"+Move.columnToChar(this.toColumn)+""+(this.toRow+1)+" prom:"+this.promotion+" objid:"+this.obj_id+" dummy:"+this.dummy+" endNode:"+this.endNode+" index:"+this.index+" moveNum:"+this.moveNum+" atEnd:"+this.atEnd+" beforeCom:"+this.beforeComment+" afterCom:"+this.afterComment;
};
Move.prototype.equals=function(m){
    return (m&&(this.fromColumn==m.fromColumn&&this.fromRow==m.fromRow&&this.promotion==m.promotion&&this.toColumn==m.toColumn&&this.toRow==m.toRow));
};
Move.moveArraysEqual=function(a1,a2){
    if(a1==a2){
        return true;
    }
    if(a1==null||a2==null){
        return false;
    }
    if(a1.length!=a2.length){
        return false;
    }
    for(var i=0;i<a1.length;i++){
        if(!a1[i].equals(a2[i])){
            return false;
        }
        if(!Move.moveArraysEqual(a1[i].vars,a2[i].vars)){
            return false;
        }
    }
    return true;
};
Move.findMoveInNewArray=function(a1,a2,_51){
    if(a1==a2){
        return _51;
    }
    if(a1==null||a2==null){
        return null;
    }
    if(a1.length!=a2.length){
        return null;
    }
    for(var i=0;i<a1.length;i++){
        if(!a1[i].equals(a2[i])){
            return null;
        }
        if(!Move.moveArraysEqual(a1[i].vars,a2[i].vars)){
            return null;
        }
        if(a1[i]==_51){
            return a2[i];
        }
    }
    return null;
};
Move.prototype.toMoveString=function(){
    var _53="";
    if(this.promotion){
        _53=this.promotion;
    }
    return Move.columnToChar(this.fromColumn)+""+(this.fromRow+1)+Move.columnToChar(this.toColumn)+""+(this.toRow+1)+_53;
};
function getTagValue(_54,_55){
    var _56=_54.getElementsByTagName(_55);
    if(_56==null){
        YAHOO.log("got null node for tag:"+_55);
        return null;
    }
    if(_56.length==0){
        YAHOO.log("got empty array node for tag:"+_55);
        return null;
    }
    if(_56[0].firstChild==null){
        YAHOO.log("firstChild is null for tag:"+_55);
        return null;
    }
    if(_56[0].firstChild.nodeValue==null){
        YAHOO.log("firstChild.nodeValue is null for tag:"+_55);
        return null;
    }
    if(typeof (_56[0].textContent)!="undefined"){
        return _56[0].textContent;
    }
    return _56[0].firstChild.nodeValue;
}
var ua=navigator.userAgent.toLowerCase();
var isOpera=(ua.indexOf("opera")>-1);
var isIphone=(navigator.userAgent.match(/iPhone/i))||(navigator.userAgent.match(/iPod/i));
var isIpad=(navigator.userAgent.match(/iPad/i));
var isSafari=(ua.indexOf("safari")>-1);
var isGecko=(!isOpera&&!isSafari&&ua.indexOf("gecko")>-1);
var isIE=(!isOpera&&ua.indexOf("msie")>-1);
function unescapeHtml(s){
    var n=document.createElement("div");
    n.innerHTML=s;
    if(n.innerText){
        return n.innerText;
    }else{
        return n.textContent;
    }
}
ChessPiece=function(div,_5a,_5b,_5c){
    var id=div.id;
    this.board=_5c;
    this.icon=get_image_str(ChessPiece.pieceIconNames[_5a][_5b],this.board.boardImagePath,this.board.pieceSet,this.board.pieceSize,this.board.addVersion);
    this.colour=_5a;
    this.piece=_5b;
    this.id=id;
    this.div=div;
    var _5e=_5c.getPieceDragDiv();
    var _5f=false;
    var _60="";
    if(_5e==null){
        _5e=document.createElement("div");
        _5e.id="pieceDragDiv";
        _5f=true;
        YAHOO.util.Dom.setStyle(_5e,"visibility","hidden");
        YAHOO.util.Dom.setStyle(_5e,"border","0px");
        YAHOO.util.Dom.setStyle(_5e,"position","absolute");
    }
    this.pieceDragEl=_5e;
    this.pieceDragElId="pieceDragDiv";
    if(_5f){
        var _61=this.board.getDocBody();
        if(_61){
            _61.appendChild(_5e);
        }
    }
    if(YAHOO.util.Event.isIE||isOpera){
        var _62=this.div;
        _62.innerHTML="<img src=\""+this.icon+"\"/>";
        var img=_62.firstChild;
        fix_ie_png(img);
    }else{
        YAHOO.util.Dom.setStyle([this.div],"backgroundImage","url("+this.icon+")");
    }
    YAHOO.util.Dom.setStyle([this.div],"height",this.board.pieceSize+"px");
    YAHOO.util.Dom.setStyle([this.div],"width",this.board.pieceSize+"px");
    if(isIphone||isIpad){
        if(!this.board.clickAndClick){
            initIphone(this.div);
        }
    }
    YAHOO.util.Dom.setStyle([this.div],"position","relative");
    if(!this.board.clickAndClick){
        this.init(id,"ct-"+this.board.boardName+"-boardandpieces",{dragElId:this.pieceDragElId,resizeFrame:true,centerFrame:false,isTarget:false});
        this.initFrame();
    }
};
ChessPiece.prototype=new YAHOO.util.DDProxy();
ChessPiece.PAWN=0;
ChessPiece.BISHOP=1;
ChessPiece.KNIGHT=2;
ChessPiece.ROOK=3;
ChessPiece.KING=4;
ChessPiece.QUEEN=5;
ChessPiece.WHITE=0;
ChessPiece.BLACK=1;
ChessPiece.init=function(){
    ChessPiece.pieceIconNames=new Array(2);
    ChessPiece.pieceIconNames[0]=new Array(6);
    ChessPiece.pieceIconNames[1]=new Array(6);
    ChessPiece.pieceIconNames[ChessPiece.WHITE][ChessPiece.PAWN]="whitepawn";
    ChessPiece.pieceIconNames[ChessPiece.WHITE][ChessPiece.BISHOP]="whitebishop";
    ChessPiece.pieceIconNames[ChessPiece.WHITE][ChessPiece.KNIGHT]="whiteknight";
    ChessPiece.pieceIconNames[ChessPiece.WHITE][ChessPiece.ROOK]="whiterook";
    ChessPiece.pieceIconNames[ChessPiece.WHITE][ChessPiece.KING]="whiteking";
    ChessPiece.pieceIconNames[ChessPiece.WHITE][ChessPiece.QUEEN]="whitequeen";
    ChessPiece.pieceIconNames[ChessPiece.BLACK][ChessPiece.PAWN]="blackpawn";
    ChessPiece.pieceIconNames[ChessPiece.BLACK][ChessPiece.BISHOP]="blackbishop";
    ChessPiece.pieceIconNames[ChessPiece.BLACK][ChessPiece.KNIGHT]="blackknight";
    ChessPiece.pieceIconNames[ChessPiece.BLACK][ChessPiece.ROOK]="blackrook";
    ChessPiece.pieceIconNames[ChessPiece.BLACK][ChessPiece.KING]="blackking";
    ChessPiece.pieceIconNames[ChessPiece.BLACK][ChessPiece.QUEEN]="blackqueen";
};
ChessPiece.materialValue=function(_64){
    switch(_64){
        case ChessPiece.PAWN:
            return 1;
            break;
        case ChessPiece.BISHOP:
            return 3;
            break;
        case ChessPiece.KNIGHT:
            return 3;
            break;
        case ChessPiece.ROOK:
            return 5;
            break;
        case ChessPiece.KING:
            return 0;
            break;
        case ChessPiece.QUEEN:
            return 9;
            break;
    }
    return 0;
};
ChessPiece.prototype.free=function(){
    if(!this.board.clickAndClick){
        this.unreg();
    }
};
ChessPiece.prototype.clickValidator=function(e){
    if(this.board.dragDisabled){
        return false;
    }
    if(!this.board.allowPreMoveSelection&&(this.board.toMove!=this.colour)){
        return false;
    }
    if(this.board.restrictedColourMovement!=-1&&this.colour!=this.board.restrictedColourMovement){
        return;
    }
    if(false&&this.board.clickAndClick){
        return false;
    }
    var _66=YAHOO.util.Event.getTarget(e);
    var _67=(this.isValidHandleChild(_66)&&(this.id==this.handleElId||this.DDM.handleWasClicked(_66,this.id)));
    this.board.selectDestSquare(e);
    if(true||!_67){
        YAHOO.util.Event.preventDefault(e);
    }
    return _67;
};
ChessPiece.prototype.onDragOut=function(e,id){
    this.insideBoard=false;
};
ChessPiece.prototype.onDragEnter=function(e,id){
    this.insideBoard=true;
};
ChessPiece.prototype.endDrag=function(e){
    if(this.board.lastOverSquare){
        YAHOO.util.Dom.removeClass(this.board.lastOverSquare,"ct-over-valid-square");
        YAHOO.util.Dom.removeClass(this.board.lastOverSquare,"ct-over-invalid-square");
    }
    this.board.lastOverSquare=null;
    if(!this.insideBoard){
        this.board.board_xy=null;
        this.setPosition(this.column,this.row,false,null,this.board.moveAnimationLength);
    }
    if(!this.hideAfterDragEnd){
        YAHOO.util.Dom.setStyle(this.getEl(),"visibility","visible");
    }else{
        this.hideAfterDragEnd=false;
    }
};
ChessPiece.prototype.startDrag=function(x,y){
    this.insideBoard=true;
    var _6f=null;
    if(this.board.currentMove){
        if(this.board.currentMove.prev){
            _6f=this.board.currentMove.prev;
        }else{
            _6f=this.board.prev_move;
        }
    }else{
        _6f=this.board.prev_move;
    }
    if(this.board.highlightValidSquares){
        this.candidates=null;
        this.candidates=new Array(8);
        for(var i=0;i<8;i++){
            this.candidates[i]=new Array(8);
            for(var j=0;j<8;j++){
                this.candidates[i][j]=false;
            }
        }
    }
    this.pieceDragEl.innerHTML="<img src=\""+this.icon+"\"/>";
    var img=this.pieceDragEl.firstChild;
    fix_ie_png(img);
    YAHOO.util.Dom.setStyle(this.pieceDragEl,"zIndex",1000);
    YAHOO.util.Dom.setStyle(this.pieceDragEl,"height",this.board.pieceSize+"px");
    YAHOO.util.Dom.setStyle(this.pieceDragEl,"width",this.board.pieceSize+"px");
    YAHOO.util.Dom.setStyle(this.getEl(),"visibility","hidden");
    if(this.board.highlightValidSquares){
        for(var i=0;i<8;i++){
            for(var j=0;j<8;j++){
                var _73=7-i;
                var _74=j;
                if(this.board.isFlipped){
                    _73=7-_73;
                    _74=7-_74;
                }
                if((_73==this.row&&_74==this.column)||this.board.canMove(this.makeLightWeight(),_74,_73,_6f,true)){
                    this.candidates[j][i]=true;
                }
            }
        }
    }
};
ChessPiece.prototype.onDragOver=function(e,id){
    var x=YAHOO.util.Event.getPageX(e);
    var y=YAHOO.util.Event.getPageY(e);
    var _79=YAHOO.util.Dom.getX("ctb-"+this.board.boardName);
    var _7a=YAHOO.util.Dom.getY("ctb-"+this.board.boardName);
    var c=parseInt((x-_79)/this.board.pieceSize);
    var r=parseInt((y-_7a)/this.board.pieceSize);
    var _7d=this.board.boardName+"-s"+c+""+(7-r);
    var _7e=YAHOO.util.Dom.get(_7d);
    if(this.board.highlightValidSquares){
        if(this.board.lastOverSquare){
            if(this.board.lastOverSquare!=_7e){
                YAHOO.util.Dom.removeClass(this.board.lastOverSquare,"ct-over-valid-square");
                YAHOO.util.Dom.removeClass(this.board.lastOverSquare,"ct-over-invalid-square");
                this.board.lastOverSquare=null;
                if(this.candidates&&c<8&&c>=0&&r<8&&r>=0&&this.candidates[c][r]){
                    YAHOO.util.Dom.addClass(_7e,"ct-over-valid-square");
                }else{
                    YAHOO.util.Dom.addClass(_7e,"ct-over-invalid-square");
                }
            }
        }
        this.board.lastOverSquare=_7e;
    }
};
ChessPiece.prototype.onDragDrop=function(e,id){
    if(this.board.blockFowardBack||this.board.deferredBlockForwardBack){
        return false;
    }
    if(this.board.allowPreMoveSelection&&this.board.toMove!=this.colour){
        return false;
    }
    if(this.board.lastOverSquare){
        YAHOO.util.Dom.removeClass(this.board.lastOverSquare,"ct-over-valid-square");
        YAHOO.util.Dom.removeClass(this.board.lastOverSquare,"ct-over-invalid-square");
    }
    var x=YAHOO.util.Event.getPageX(e);
    var y=YAHOO.util.Event.getPageY(e);
    var _83=YAHOO.util.Dom.getX("ctb-"+this.board.boardName);
    var _84=YAHOO.util.Dom.getY("ctb-"+this.board.boardName);
    var c=parseInt((x-_83)/this.board.pieceSize);
    var r=parseInt((y-_84)/this.board.pieceSize);
    if(this.board.isFlipped){
        r=7-r;
        c=7-c;
    }
    if(this.board.allowPreMoveSelection&&(this.board.boardPieces[this.column][this.row]!=this)){
        this.setVisible(false);
        this.hideAfterDragEnd=true;
        return false;
    }
    var _87=false;
    if(!this.board.currentMove||this.board.currentMove.atEnd){
        _87=true;
    }
    this.board.updatePiece(this,c,7-r,false,false,true);
    if(!_87&&this.board.currentMove&&!this.board.allowingFreeMovement&&this.board.currentMove.atEnd){
        this.board.toggleToMove();
        this.board.updateToPlay();
    }
};
ChessPiece.prototype.makeLightWeight=function(){
    var cp=this.board.createPiece(this.colour,this.piece,true);
    cp.column=this.column;
    cp.row=this.row;
    cp.enPassant=this.enPassant;
    cp.castled=this.castled;
    return cp;
};
ChessPiece.prototype.removeFromParent=function(){
    var _89=this.div;
    if(_89.parentNode){
        _89.parentNode.removeChild(_89);
    }
};
ChessPiece.prototype.setVisible=function(_8a){
    var _8b;
    var _8c;
    if(_8a){
        _8c="block";
        _8b="visible";
    }else{
        _8c="none";
        _8b="hidden";
    }
    YAHOO.util.Dom.setStyle(this.id,"visibility",_8b);
};
ChessPiece.prototype.moveResponse=function(o){
};
ChessPiece.prototype.getIcon=function(){
    return this.icon;
};
ChessPiece.prototype.makeHeavyWeight=function(){
    return this.copyPiece();
};
ChessPiece.prototype.copyPiece=function(){
    var cp=new ChessPiece(this.div,this.colour,this.piece,this.board);
    cp.column=this.column;
    cp.row=this.row;
    cp.enPassant=this.enPassant;
    cp.castled=this.castled;
    return cp;
};
ChessPiece.prototype.changePieceKeepImage=function(_8f){
    var _90=(_8f+"").toLowerCase().charAt(0);
    switch(_90){
        case "k":
            this.piece=ChessPiece.KING;
            break;
        case "q":
            this.piece=ChessPiece.QUEEN;
            break;
        case "r":
            this.piece=ChessPiece.ROOK;
            break;
        case "b":
            this.piece=ChessPiece.BISHOP;
            break;
        case "n":
            this.piece=ChessPiece.KNIGHT;
            break;
        case "p":
            this.piece=ChessPiece.PAWN;
            break;
        default:
    }
};
ChessPiece.prototype.changePiece=function(_91){
    this.changePieceKeepImage(_91);
    this.icon=get_image_str(ChessPiece.pieceIconNames[this.colour][this.piece],this.board.boardImagePath,this.board.pieceSet,this.board.pieceSize,this.board.addVersion);
    if(YAHOO.util.Event.isIE||isOpera){
        var _92=this.div;
        _92.innerHTML="<img src=\""+this.icon+"\"/>";
        var img=_92.firstChild;
        if(!isOpera){
            fix_ie_png(img);
        }
    }else{
        YAHOO.util.Dom.setStyle(this.div,"backgroundImage","url("+this.icon+")");
        YAHOO.util.Dom.setStyle(this.div,"background-repeat","no-repeat");
    }
};
ChessPiece.prototype.getNewXYPosition=function(_94,row){
    var _96=this.board.getBoardDiv();
    var _97=this.board.getXY();
    var _98=_97[0];
    var _99=_97[1];
    var _9a=[0,0];
    if(this.board.isFlipped){
        _9a[0]=_98+((7-_94)*this.board.pieceSize);
        _9a[1]=_99+((row)*this.board.pieceSize);
    }else{
        _9a[0]=_98+((_94)*this.board.pieceSize);
        _9a[1]=_99+((7-row)*this.board.pieceSize);
    }
    return _9a;
};
ChessPiece.prototype.setPosition=function(_9b,row,_9d,_9e,_9f,_a0,_a1){
    this.column=_9b;
    this.row=row;
    if(this.board.pieceMoveDisabled){
        return;
    }
    var _a2=this.div;
    var _a3=null;
    if(this.board.isFlipped){
        _a3=this.board.boardName+"-s"+(7-this.column)+""+(7-this.row);
    }else{
        _a3=this.board.boardName+"-s"+(this.column)+""+(this.row);
    }
    var _a4=this.board.getBoardDivFromId(_a3);
    var _a5=null;
    if(!_a0){
        _a5=(this.colour==ChessPiece.WHITE)?this.board.whiteMoveSoundName:this.board.blackMoveSoundName;
    }else{
        _a5=(this.colour==ChessPiece.WHITE)?this.board.whiteTakeSoundName:this.board.blackTakeSoundName;
    }
    if(!_9d){
        if(!this.board.settingUpPosition){
            var _a6=this.getNewXYPosition(_9b,row);
            YAHOO.util.Dom.setXY(_a2,_a6,false);
        }else{
            if(_a2.parentNode){
                _a2.parentNode.removeChild(_a2);
            }
            _a4.appendChild(_a2);
        }
        this.setVisible(true);
        if(_a1&&this.board.soundEnabled){
            boardSounds.playSound(_a5);
        }
        if(_9e){
            _9e();
        }
    }else{
        var _a6=this.getNewXYPosition(_9b,row);
        if(this.board.oldAnim&&this.board.oldAnim.isAnimated()){
            this.board.oldAnim.stop();
            YAHOO.util.Dom.setXY(this.board.oldAnimPieceDiv,this.board.old_new_xy,false);
        }
        var _a7=new YAHOO.util.Motion(_a2,{points:{to:_a6}});
        this.board.oldAnim=_a7;
        this.board.oldAnimPieceDiv=_a2;
        this.board.old_new_xy=_a6;
        _a7.duration=_9f;
        var _a8=this;
        _a7.onComplete.subscribe(function(){
            if(_a8.board.soundEnabled){
                boardSounds.playSound(_a5);
            }
        });
        if(_9e){
            _a7.onComplete.subscribe(_9e);
        }
        _a7.animate();
    }
};
ChessPiece.prototype.getFenLetter=function(){
    var _a9=ChessPiece.pieceTypeToChar(this.piece)+"";
    if(this.colour!=ChessPiece.WHITE){
        _a9=_a9.toLowerCase();
    }
    return _a9;
};
ChessPiece.pieceTypeToChar=function(_aa){
    switch(_aa){
        case ChessPiece.KING:
            return "K";
        case ChessPiece.QUEEN:
            return "Q";
        case ChessPiece.ROOK:
            return "R";
        case ChessPiece.BISHOP:
            return "B";
        case ChessPiece.KNIGHT:
            return "N";
        case ChessPiece.PAWN:
            return "P";
    }
    return "?";
};
LightweightChessPiece=function(div,_ac,_ad,_ae){
    this.board=_ae;
    this.colour=_ac;
    this.piece=_ad;
    this.div=div;
};
LightweightChessPiece.prototype.getFenLetter=ChessPiece.prototype.getFenLetter;
LightweightChessPiece.prototype.makeLightWeight=function(){
    return this.copyPiece();
};
LightweightChessPiece.prototype.makeHeavyWeight=function(){
    var cp=this.board.createPiece(this.colour,this.piece,false);
    cp.column=this.column;
    cp.row=this.row;
    cp.enPassant=this.enPassant;
    cp.castled=this.castled;
    return cp;
};
LightweightChessPiece.prototype.setVisible=function(_b0){
};
LightweightChessPiece.prototype.free=function(){
};
LightweightChessPiece.prototype.setPosition=function(_b1,row,_b3,_b4,_b5){
    this.column=_b1;
    this.row=row;
};
LightweightChessPiece.prototype.copyPiece=function(){
    var cp=new LightweightChessPiece(this.id,this.colour,this.piece,this.board);
    cp.column=this.column;
    cp.row=this.row;
    return cp;
};
LightweightChessPiece.prototype.changePiece=function(_b7){
    this.changePieceKeepImage(_b7);
};
LightweightChessPiece.prototype.changePieceKeepImage=function(_b8){
    var _b9=(_b8+"").toLowerCase().charAt(0);
    switch(_b9){
        case "k":
            this.piece=ChessPiece.KING;
            break;
        case "q":
            this.piece=ChessPiece.QUEEN;
            break;
        case "r":
            this.piece=ChessPiece.ROOK;
            break;
        case "b":
            this.piece=ChessPiece.BISHOP;
            break;
        case "n":
            this.piece=ChessPiece.KNIGHT;
            break;
        case "p":
            this.piece=ChessPiece.PAWN;
            break;
        default:
    }
};
MovesDisplay=function(_ba,_bb){
    this.board=_ba;
    this.displayType=_bb;
};
MovesDisplay.DEFAULT_DISPLAY_TYPE=0;
MovesDisplay.MAIN_ON_OWN_LINE=1;
Board=function(_bc){
    this.boardName=_bc;
    if(_bc){
        this.initTarget("ctb-"+_bc,"ct-"+this.boardName+"-boardandpieces");
        this.boardPieces=Board.createBoardArray();
    }
    this.imagesLoaded=false;
    this.disableNavigation=false;
    this.currentMove=null;
    this.outputWithoutDisplay=false;
    this.moveIndex=-1;
    this.dontUpdatePositionReachedTable=false;
    this.restrictedColourMovement=-1;
    this.settingUpPosition=false;
    this.pendingLevelZeroCommentaryClose=false;
    this.isUserFlipped=false;
    this.registeredFlipListeners=[];
    this.registeredSpaceListeners=[];
    this.registeredForwardAtEndListeners=[];
    this.registeredPasteFenClickedListeners=[];
    this.registeredGotoMoveIndexListeners=[];
    this.registeredBackMovePreCurrentListeners=[];
    this.registeredForwardMovePostUpdateListeners=[];
    this.registeredUpdateListeners=[];
    this.registeredUpdatePieceFinishedListeners=[];
    this.registeredUpdateEndOfMovesListeners=[];
    this.registeredUpdateHaveAltListeners=[];
    this.registeredUpdateWrongMoveListeners=[];
    this.registeredUpdateAllowMoveListeners=[];
    this.registeredMakeMoveListeners=[];
    this.moveNumber=1;
    this.halfMoveNumber=0;
};
Board.prototype=new YAHOO.util.DDTarget();
Board.invertToMove=function(_bd){
    if(_bd==ChessPiece.WHITE){
        return ChessPiece.BLACK;
    }else{
        return ChessPiece.WHITE;
    }
};
Board.boardStyleToClassName=function(_be){
    var _bf="";
    switch(_be){
        case 0:
            _bf="-lightgrey";
            break;
        case 1:
            _bf="-grey";
            break;
        case 2:
            _bf="-brown";
            break;
        case 3:
            _bf="-green";
            break;
        case 4:
            _bf="-woodlight";
            break;
        case 5:
            _bf="-wooddark";
            break;
        case 6:
            _bf="-metal";
            break;
        case 7:
            _bf="-marblebrown";
            break;
        case 8:
            _bf="-stucco";
            break;
        case 9:
            _bf="-goldsilver";
            break;
        case 10:
            _bf="-sandsnow";
            break;
        case 11:
            _bf="-crackedstone";
            break;
        case 12:
            _bf="-granite";
            break;
        case 13:
            _bf="-marblegreen";
            break;
        case 14:
            _bf="-greenwhite";
            break;
        default:
    }
    return _bf;
};
Board.createBoardArray=function(){
    var _c0=boardPool.getObject();
    if(_c0==null){
        _c0=new Array(8);
        for(var i=0;i<8;i++){
            _c0[i]=new Array(8);
        }
    }
    return _c0;
};
Board.prototype.preloadPieces=function(){
    var _c2=[];
    for(var i=0;i<ChessPiece.QUEEN;i++){
        for(var j=0;j<2;j++){
            var _c5=get_image_str(ChessPiece.pieceIconNames[j][i],this.boardImagePath,this.pieceSet,this.pieceSize,true);
            _c2.push(_c5);
        }
    }
    var _c6=this;
    function checkImages(){
        var _c7=true;
        for(var i=0;i<_c2.length;i++){
            var img=document.createElement("img");
            img.src=_c2[i];
            if(!img.complete||(typeof img.naturalWidth!="undefined"&&img.naturalWidth==0)){
                _c7=false;
            }
        }
        if(!_c7){
            setTimeout(checkImages,1000);
        }else{
            _c6.imagesLoaded=true;
        }
    }
    checkImages();
};
Board.prototype.selectDestSquare=function(e){
    if(this.clickAndClickDisabled){
        return true;
    }
    var _cb=(new Date()).getTime();
    var _cc=false;
    if(_cb-this.lastDestClick<100){
        _cc=true;
    }
    this.lastDestClick=_cb;
    var x=YAHOO.util.Event.getPageX(e);
    var y=YAHOO.util.Event.getPageY(e);
    var _cf=YAHOO.util.Dom.getX("ctb-"+this.boardName);
    var _d0=YAHOO.util.Dom.getY("ctb-"+this.boardName);
    var c=parseInt((x-_cf)/this.pieceSize);
    var r=parseInt((y-_d0)/this.pieceSize);
    var _d3=this.boardName+"-s"+c+""+(7-r);
    var _d4=YAHOO.util.Dom.get(_d3);
    if(_d4==this.oldSelectedSquare){
        if(!_cc){
            YAHOO.util.Dom.removeClass(_d4,"ct-source-square");
            this.oldSelectedSquare=null;
            this.oldSelectedPiece=null;
            if(this.oldDestSquare){
                YAHOO.util.Dom.removeClass(this.oldDestSquare,"ct-dest-square");
                this.oldDestSquare=null;
            }
        }
        return true;
    }
    if(this.isFlipped){
        c=7-c;
        r=7-r;
    }
    r=7-r;
    var _d5=this.boardPieces[c][r];
    if(_d5&&(_d5.colour==this.toMove||this.allowPreMoveSelection)&&(this.restrictedColourMovement==-1||(_d5.colour==this.restrictedColourMovement))){
        if(this.oldSelectedSquare){
            YAHOO.util.Dom.removeClass(this.oldSelectedSquare,"ct-source-square");
        }
        if(this.oldDestSquare){
            YAHOO.util.Dom.removeClass(this.oldDestSquare,"ct-dest-square");
            this.oldDestSquare=null;
        }
        YAHOO.util.Dom.addClass(_d4,"ct-source-square");
        this.oldSelectedSquare=_d4;
        this.oldSelectedPiece=_d5;
    }else{
        if(this.oldSelectedSquare){
            if(this.oldSelectedPiece&&this.oldSelectedPiece.colour!=this.toMove){
                return false;
            }
            var _d6=null;
            if(this.currentMove){
                if(this.currentMove.prev){
                    _d6=this.currentMove.prev;
                }else{
                    _d6=this.prev_move;
                }
            }else{
                _d6=this.prev_move;
            }
            if(this.canMove(this.oldSelectedPiece.makeLightWeight(),c,r,_d6,true)){
                this.lastDestSquare=_d4;
                this.lastDestRow=r;
                this.lastDestColumn=c;
                YAHOO.util.Dom.removeClass(this.oldSelectedSquare,"ct-source-square");
                var _d7=false;
                if(!this.currentMove||this.currentMove.atEnd){
                    _d7=true;
                }
                this.updatePiece(this.oldSelectedPiece,c,r,false,false,true);
                this.oldSelectedPiece=null;
                this.oldSelectedSquare=null;
                if(!_d7&&this.currentMove&&!this.allowingFreeMovement&&this.currentMove.atEnd){
                    this.toggleToMove();
                    this.updateToPlay();
                }
            }else{
            }
        }else{
            return true;
        }
    }
};
Board.prototype.selectSourcePiece=function(_d8){
    if(this.lastSourceSquare){
        YAHOO.util.Dom.removeClass(_d9,"ct-source-square");
    }
    var r=_d8.row;
    var c=_d8.column;
    if(this.isFlipped){
        r=7-r;
        c=7-c;
    }
    var _dc=this.boardName+"-s"+c+""+r;
    var _d9=YAHOO.util.Dom.get(_dc);
    YAHOO.util.Dom.addClass(_d9,"ct-source-square");
    this.lastSourceSquare=_d9;
    this.lastSourcePiece=_d8;
    this.lastSourceRow=_d8.row;
    this.lastSourceColumn=_d8.column;
};
Board.prototype.toggleToMove=function(){
    if(this.toMove==ChessPiece.WHITE){
        this.toMove=ChessPiece.BLACK;
    }else{
        this.toMove=ChessPiece.WHITE;
    }
};
Board.prototype.setupPieceDivs=function(){
    var _dd=this.getBoardDiv();
    if(this.pieces){
        for(var i=0;i<32;i++){
            if(this.pieces[i]){
                this.pieces[i].setVisible(false);
                this.pieces[i].free();
                this.pieces[i]=null;
            }
        }
    }
    if(this.availPieceDivs){
        for(var i=0;i<32;i++){
            if(this.availPieceDivs[i]){
                if(this.availPieceDivs[i].parentNode){
                    this.availPieceDivs[i].parentNode.removeChild(this.availPieceDivs[i]);
                }
            }
        }
    }
    this.availids=null;
    this.availIds=new Array(32);
    this.availPieceDivs=null;
    this.availPieceDivs=new Array(32);
    this.pieces=null;
    this.pieces=new Array(32);
    this.uptoId=0;
    this.uptoPiece=0;
};
Board.prototype.getXY=function(){
    if(true||!this.board_xy){
        this.board_xy=YAHOO.util.Dom.getXY("ctb-"+this.boardName);
    }
    return this.board_xy;
};
Board.prototype.updateFromTo=function(_df,_e0,_e1,_e2,_e3,_e4){
    YAHOO.util.Dom.removeClass(this.lastFromSquare,"ct-from-square");
    YAHOO.util.Dom.removeClass(this.lastToSquare,"ct-to-square");
    if(_e1==null){
        return;
    }
    this.lastFromSquare=_df;
    this.lastToSquare=_e0;
    this.lastFromRow=_e1;
    this.lastFromColumn=_e2;
    this.lastToRow=_e3;
    this.lastToColumn=_e4;
    if(this.highlightFromTo){
        YAHOO.util.Dom.addClass(_df,"ct-from-square");
        YAHOO.util.Dom.addClass(_e0,"ct-to-square");
    }
};
Board.prototype.makeMove=function(_e5,_e6,_e7,_e8,_e9,_ea,_eb,_ec,_ed){
    var _ee;
    var _ef;
    if(!this.isFlipped){
        _ee=YAHOO.util.Dom.get(this.boardName+"-s"+_e5.fromColumn+""+_e5.fromRow);
        _ef=YAHOO.util.Dom.get(this.boardName+"-s"+_e5.toColumn+""+_e5.toRow);
    }else{
        _ee=YAHOO.util.Dom.get(this.boardName+"-s"+(7-_e5.fromColumn)+""+(7-_e5.fromRow));
        _ef=YAHOO.util.Dom.get(this.boardName+"-s"+(7-_e5.toColumn)+""+(7-_e5.toRow));
    }
    if(this.oldSelectedSquare){
        if(!this.allowPreMoveSelection||(this.oldSelectedPiece&&_e6&&(this.oldSelectedPiece.colour==_e6.colour))){
            YAHOO.util.Dom.removeClass(this.oldSelectedSquare,"ct-source-square");
            this.oldSelectedSquare=null;
            this.oldSelectedPiece=null;
        }
    }
    if(_ea){
        this.updateFromTo(_ee,_ef,_e5.fromRow,_e5.fromColumn,_e5.toRow,_e5.toColumn);
    }
    var _f0=this.boardPieces[_e5.toColumn][_e5.toRow];
    if(_f0!=null){
        _f0.enPassant=false;
        _f0.castled=false;
    }
    if(_e6.piece==ChessPiece.PAWN&&_e5.toColumn!=_e5.fromColumn&&this.boardPieces[_e5.toColumn][_e5.toRow]==null){
        _f0=this.boardPieces[_e5.toColumn][_e5.fromRow];
        this.boardPieces[_e5.toColumn][_e5.fromRow]=null;
        if(_f0!=null){
            _f0.enPassant=true;
        }
    }
    var _f1=null;
    if(_e6.piece==ChessPiece.KING&&Math.abs(_e5.toColumn-_e5.fromColumn)>1){
        var _f2;
        var _f3;
        if(_e5.toColumn>_e5.fromColumn){
            _f1=this.boardPieces[7][_e5.fromRow];
            _f2=_e5.fromRow;
            _f3=5;
            this.boardPieces[7][_e5.toRow]=null;
        }else{
            _f1=this.boardPieces[0][_e5.fromRow];
            _f2=_e5.fromRow;
            _f3=3;
            this.boardPieces[0][_e5.toRow]=null;
        }
        if(!_f1){
            alert("No castle piece");
        }else{
            _f1.setPosition(_f3,_f2,_e7,null,_e8,null,_ed);
            this.boardPieces[_f1.column][_f1.row]=_f1;
            _f1.castled=true;
        }
    }
    _e5.taken=_f0;
    if(_f0&&_e9){
        this.processTaken(_f0,true);
    }
    this.moveNumber++;
    _e5.preHalfMoveNumber=this.halfMoveNumber;
    this.halfMoveNumber++;
    if(_f0||_e6.piece==ChessPiece.PAWN){
        this.halfMoveNumber=0;
    }
    this.board_xy=null;
    if(_e5.promotion!=null){
        _e6.changePieceKeepImage(_e5.promotion);
    }
    _e6.setPosition(_e5.toColumn,_e5.toRow,_e7,function(){
        var tp=_f0;
        if(tp){
            tp.setVisible(false);
        }
        if(_e5.promotion!=null){
            _e6.changePiece(_e5.promotion);
        }
        if(_eb){
            _eb.call(_ec);
        }
    },_e8,_f0,_ed);
    if(!_e7){
        if(_e5.promotion!=null){
            _e6.changePiece(_e5.promotion);
        }
    }
    this.boardPieces[_e5.fromColumn][_e5.fromRow]=null;
    this.boardPieces[_e5.toColumn][_e5.toRow]=_e6;
    if(_f1!=null){
        _e5.taken=_f1;
    }
    _e5.preCastleQueenSide=new Array(2);
    _e5.preCastleKingSide=new Array(2);
    _e5.preCastleQueenSide[0]=this.canCastleQueenSide[0];
    _e5.preCastleQueenSide[1]=this.canCastleQueenSide[1];
    _e5.preCastleKingSide[0]=this.canCastleKingSide[0];
    _e5.preCastleKingSide[1]=this.canCastleKingSide[1];
    if(_e6.piece==ChessPiece.ROOK){
        if(((_e6.colour==ChessPiece.WHITE)&&_e5.fromRow==0)||((_e6.colour==ChessPiece.BLACK)&&_e5.fromRow==7)){
            if(_e5.fromColumn==0){
                this.canCastleQueenSide[_e6.colour]=false;
            }else{
                if(_e5.fromColumn==7){
                    this.canCastleKingSide[_e6.colour]=false;
                }
            }
        }
    }else{
        if(_e6.piece==ChessPiece.KING){
            this.canCastleQueenSide[_e6.colour]=false;
            this.canCastleKingSide[_e6.colour]=false;
        }
    }
    if(_f0&&(_f0.piece==ChessPiece.ROOK)){
        if(_e5.toColumn==0){
            if(((_f0.colour==ChessPiece.WHITE)&&_e5.toRow==0)||((_f0.colour==ChessPiece.BLACK)&&_e5.toRow==7)){
                this.canCastleQueenSide[_f0.colour]=false;
            }
        }else{
            if(_e5.toColumn==7){
                if(((_f0.colour==ChessPiece.WHITE)&&_e5.toRow==0)||((_f0.colour==ChessPiece.BLACK)&&_e5.toRow==7)){
                    this.canCastleKingSide[_f0.colour]=false;
                }
            }
        }
    }
    this.updatePositionReached(_e6.colour);
    for(var i=0;i<this.registeredMakeMoveListeners.length;i++){
        var _f6=this.registeredMakeMoveListeners[i].makeMoveCallback(_e5);
    }
};
Board.prototype.isThreeFoldRep=function(_f7){
    var _f8=this.toMove;
    if(_f7){
        if(_f8==ChessPiece.WHITE){
            _f8=ChessPiece.BLACK;
        }else{
            _f8=ChessPiece.WHITE;
        }
    }
    var _f9=this.boardToUniqueFen(_f8);
    return (this.positionsSeen[_f9]>=3);
};
Board.prototype.updatePositionReached=function(_fa){
    if(this.dontUpdatePositionReachedTable){
        return;
    }
    var _fb=this.boardToUniqueFen(_fa);
    if(!this.positionsSeen){
        this.positionsSeen=[];
    }
    if(this.positionsSeen[_fb]){
        this.positionsSeen[_fb]++;
    }else{
        this.positionsSeen[_fb]=1;
    }
};
Board.prototype.promptPromotion=function(_fc,col,row,_ff,_100){
    _fc.prePromotionColumn=_fc.column;
    _fc.prePromotionRow=_fc.row;
    _fc.setPosition(col,row,false,null,this.moveAnimationLength);
    var _101=this;
    var _102=new YAHOO.widget.Dialog("promotionDialogId",{width:"300px",fixedcenter:true,visible:true,modal:true,close:false,constraintoviewport:true,buttons:[{text:_js("Queen"),handler:function(){
        _102.hide();
        _101.updatePiece(_fc,col,row,_ff,_100,false,"q");
    },isDefault:true},{text:_js("Rook"),handler:function(){
        _102.hide();
        _101.updatePiece(_fc,col,row,_ff,_100,false,"r");
    },isDefault:false},{text:_js("Bishop"),handler:function(){
        _102.hide();
        _101.updatePiece(_fc,col,row,_ff,_100,false,"b");
    },isDefault:false},{text:_js("Knight"),handler:function(){
        _102.hide();
        _101.updatePiece(_fc,col,row,_ff,_100,false,"n");
    },isDefault:false}]});
    _102.setHeader(_js("Select Promotion Piece"));
    _102.setBody("<div></div>");
    _102.render(document.body);
};
Board.moveToLocale=function(_103){
    if(!_103||_103==""){
        return _103;
    }
    var _104="";
    for(var i=0;i<_103.length;i++){
        var _106=_103.charAt(i);
        switch(_106){
            case "K":
                _106=_js("K");
                break;
            case "Q":
                _106=_js("Q");
                break;
            case "R":
                _106=_js("R");
                break;
            case "N":
                _106=_js("N");
                break;
            case "B":
                _106=_js("B");
                break;
            case "P":
                _106=_js("P");
                break;
            case "a":
                _106=_js("a");
                break;
            case "b":
                _106=_js("b");
                break;
            case "c":
                _106=_js("c");
                break;
            case "d":
                _106=_js("d");
                break;
            case "e":
                _106=_js("e");
                break;
            case "f":
                _106=_js("f");
                break;
            case "g":
                _106=_js("g");
                break;
            case "h":
                _106=_js("h");
                break;
            case "x":
                _106=_js("x");
                break;
            case "#":
                _106=_js("#");
                break;
        }
        _104+=_106;
    }
    return _104;
};
Board.prototype.updatePiece=function(_107,col,row,_10a,_10b,_10c,_10d,_10e){
    if(_10d){
        this.board_xy=null;
        if(_107.prePromotionRow){
            _107.row=_107.prePromotionRow;
            _107.column=_107.prePromotionColumn;
        }
    }
    if(_10d==null&&_107.column==col&&_107.row==row){
        this.board_xy=null;
        _107.setPosition(_107.column,_107.row,false,null,this.moveAnimationLength);
        if(clog){
            console.log("moved piece back to its orig position");
        }
        return;
    }
    var _10f=null;
    if(this.currentMove){
        if(this.currentMove.prev){
            _10f=this.currentMove.prev;
        }else{
            _10f=this.prev_move;
        }
    }else{
        _10f=this.prev_move;
    }
    if(clog){
        if(this.currentMove){
            console.log("updatepiece currentMove:"+this.currentMove.output());
        }else{
            console.log("updatepiece currentmove null");
        }
    }
    if(!_10a&&!this.canMove(_107.makeLightWeight(),col,row,_10f,true)){
        this.board_xy=null;
        _107.setPosition(_107.column,_107.row,false,null,0.5);
        if(clog){
            console.log("move not legal , move back to orig:"+this.toMove);
            if(_10f){
                console.log("prevMove was:"+_10f.output());
            }else{
                console.log("prevMove was null");
            }
        }
        return;
    }
    var _110="";
    if(_10c&&_107.piece==ChessPiece.PAWN&&(row==7||row==0)){
        this.promptPromotion(_107,col,row,_10a,_10b);
        return;
    }else{
        if(_10d!=null){
            _110=_10d;
        }
    }
    var _111=true;
    var _112="";
    _112+=Move.columnToChar(_107.column);
    _112+=String.fromCharCode("1".charCodeAt(0)+_107.row);
    _112+=Move.columnToChar(col);
    _112+=String.fromCharCode("1".charCodeAt(0)+(row));
    if(_110){
        _112+=_110;
    }
    var _113=this.createMoveFromString(_112);
    var move=this.currentMove;
    if(move){
        _113.moveNum=move.moveNum;
    }
    var res=null;
    for(var i=0;i<this.registeredUpdateListeners.length;i++){
        _117=this.registeredUpdateListeners[i].updatePieceCallback(_110,_107,col,row,_10a,_10b,_10c,_10d,_10e,_10f,this.currentMove,_113);
        if(!_117){
            return false;
        }
        if(!_117.ignoreRetVal){
            res=_117;
        }
    }
    if(!res){
        if(clog){
            console.log("Got no update piece callbak");
        }
        return false;
    }
    if(res.allowMove){
        if(this.oldSelectedSquare){
            YAHOO.util.Dom.removeClass(this.oldSelectedSquare,"ct-source-square");
        }
        var move=res.move;
        for(var i=0;i<this.registeredUpdateAllowMoveListeners.length;i++){
            var res2=this.registeredUpdateAllowMoveListeners[i].updateAllowMoveCallback(_110,_107,col,row,_10a,_10b,_10c,_10d,_10e,move);
        }
        this.makeMove(move,_107,_10b,this.moveAnimationLength,true,true,null,null,true);
        var _119=!res.dontMakeOpponentMove&&!_10a&&(this.currentMove&&this.currentMove.next&&!this.currentMove.next.atEnd);
        if(clog){
            if(move.next){
                console.log("setting current move in updatepiece to:"+move.next.output());
            }else{
                console.log("in updatepiece, current move being set to null");
            }
        }
        this.setCurrentMove(move.next,false,_119);
        if(this.currentMove.atEnd){
            for(var i=0;i<this.registeredUpdateEndOfMovesListeners.length;i++){
                var res=this.registeredUpdateEndOfMovesListeners[i].updateEndOfMovesCallback(_110,_107,col,row,_10a,_10b,_10c,_10d,_10e);
            }
        }
        if(_119){
            opponentMove=this.currentMove;
            if(this.currentMove&&this.currentMove.next.atEnd){
                this.toggleToMove();
            }
            this.updatePiece(this.boardPieces[opponentMove.fromColumn][opponentMove.fromRow],opponentMove.toColumn,opponentMove.toRow,true,true,false);
        }
    }else{
        var move=res.move;
        var _11a=_107.column;
        var _11b=_107.row;
        this.board_xy=null;
        _107.setPosition(_107.column,_107.row,false,null,this.moveAnimationLength);
        for(var i=0;i<this.registeredUpdateWrongMoveListeners.length;i++){
            var res=this.registeredUpdateWrongMoveListeners[i].updateWrongMoveCallback(_110,_107,col,row,_10a,_10b,_10c,_10d,_10e,move);
        }
    }
    for(var i=0;i<this.registeredUpdatePieceFinishedListeners.length;i++){
        var _117=this.registeredUpdatePieceFinishedListeners[i].updatePieceFinishedCallback(_110,_107,col,row,_10a,_10b,_10c,_10d,_10e,_10f,this.currentMove,_113);
    }
};
Board.prototype.addGotoMoveIndexListener=function(_11c){
    this.registeredGotoMoveIndexListeners.push(_11c);
};
Board.prototype.addPasteFenClickedListener=function(_11d){
    this.registeredPasteFenClickedListeners.push(_11d);
};
Board.prototype.addBackMovePreCurrentListener=function(_11e){
    this.registeredBackMovePreCurrentListeners.push(_11e);
};
Board.prototype.addForwardMovePostUpdateListener=function(_11f){
    this.registeredForwardMovePostUpdateListeners.push(_11f);
};
Board.prototype.addForwardAtEndListener=function(_120){
    this.registeredForwardAtEndListeners.push(_120);
};
Board.prototype.addUpdatePieceListener=function(_121){
    this.registeredUpdateListeners.push(_121);
};
Board.prototype.addUpdatePieceFinishedListener=function(_122){
    this.registeredUpdatePieceFinishedListeners.push(_122);
};
Board.prototype.addUpdatePieceEndOfMovesListener=function(_123){
    this.registeredUpdateEndOfMovesListeners.push(_123);
};
Board.prototype.addUpdatePieceHaveAltListener=function(_124){
    this.registeredUpdateHaveAltListeners.push(_124);
};
Board.prototype.addUpdatePieceAllowMoveListener=function(_125){
    this.registeredUpdateAllowMoveListeners.push(_125);
};
Board.prototype.addMakeMoveListener=function(_126){
    this.registeredMakeMoveListeners.push(_126);
};
Board.prototype.addUpdatePieceWrongMoveListener=function(_127){
    this.registeredUpdateWrongMoveListeners.push(_127);
};
Board.prototype.scoreToShortString=function(_128){
    if(_128=="draw"){
        return "D";
    }
    if(_128>=0){
        return "M"+_128;
    }else{
        return "L"+(-1*_128);
    }
};
Board.prototype.scoreToLongString=function(_129){
    if(_129=="draw"){
        return _js("Draw");
    }
    if(_129==0){
        return _js("Mate");
    }else{
        if(_129>0){
            return __js("Mate in {NUMBER_MOVES}",[["NUMBER_MOVES",_129]]);
        }else{
            return __js("Lose in {NUMBER_MOVES}",[["NUMBER_MOVES",(-1*_129)]]);
        }
    }
};
Board.prototype.egMoveToScoreString=function(_12a){
    var _12b=_12a.score;
    var _12c=_12a.optimal_score;
    var s=this.scoreToShortString(_12b);
    var opt=this.scoreToShortString(_12c);
    var _12f=this.scoreToLongString(_12b);
    var _130=this.scoreToLongString(_12c);
    if(_12b==_12c){
        return ["",_12f];
    }else{
        var _131="ct-subopt-move-score";
        if(_12b=="draw"||_12b<0){
            _131="ct-bad-move-score";
        }
        return ["<span class=\""+_131+"\">"+s+"("+opt+")</span>",_12f+"("+_130+")"];
    }
};
Board.prototype.makeShortAlgabraic=function(_132,_133,_134,_135,_136){
    if(clog){
        console.log("fromCol:"+_132+" fromRow:"+_133+" toCol:"+_134+" toRow:"+_135);
    }
    var _137=this.boardPieces[_132][_133];
    var _138=_137.piece;
    var _139=ChessPiece.pieceTypeToChar(_138);
    var move="";
    if(_138==ChessPiece.PAWN){
        if(_132==_134){
            move=Move.columnToChar(_132)+""+(_135+1);
        }else{
            move=Move.columnToChar(_132)+"x"+Move.columnToChar(_134)+""+(_135+1);
            if(!this.boardPieces[_134][_135]){
                move+=" e.p.";
            }
        }
    }else{
        if(_138==ChessPiece.KING){
            var _13b=Math.abs(_132-_134);
            if(_13b==1||_13b==0){
                move=_139;
                if(this.boardPieces[_134][_135]){
                    move+="x";
                }
                move+=Move.columnToChar(_134)+""+(_135+1);
            }else{
                if(_134==6){
                    move="O-O";
                }else{
                    move="O-O-O";
                }
            }
        }else{
            var _13c=[];
            for(var row=0;row<8;row++){
                for(var col=0;col<8;col++){
                    var cp=this.boardPieces[col][row];
                    if(cp&&cp.colour==_137.colour&&cp.piece==_138&&!(_137.column==cp.column&&_137.row==cp.row)){
                        var prev=null;
                        if(this.currentMove){
                            prev=this.currentMove.prev;
                        }
                        if(this.canMove(cp.makeLightWeight(),_134,_135,prev,true)){
                            _13c.push(cp);
                        }
                    }
                }
            }
            move=_139;
            if(_13c.length>0){
                var _141=false;
                var _142=false;
                for(var i=0;i<_13c.length;i++){
                    if(_13c[i].row==_133){
                        _142=true;
                    }
                    if(_13c[i].column==_132){
                        _141=true;
                    }
                }
                if(_142||!(_142||_141)){
                    move+=Move.columnToChar(_132);
                }
                if(_141){
                    move+=""+(_133+1);
                }
            }
            if(this.boardPieces[_134][_135]){
                move+="x";
            }
            move+=Move.columnToChar(_134)+""+(_135+1);
        }
    }
    var _144="";
    var _145="";
    if(_136){
        var _146=this.cloneBoard();
        var _147=ChessPiece.WHITE;
        if(_146.boardPieces[_136.fromColumn][_136.fromRow].colour==ChessPiece.WHITE){
            _147=ChessPiece.BLACK;
        }
        _146.makeMove(_136,_146.boardPieces[_136.fromColumn][_136.fromRow],false,_146.moveAnimationLength,false,false);
        if(!_146.isKingSafe(_147,_136)){
            _144="+";
            if(_146.isKingMated(_147,_136)){
                _144="#";
            }
        }
        if(_136.promotion){
            _145="="+((_136.promotion+"").toUpperCase());
        }
    }
    move+=_145+_144;
    return move;
};
Board.getVarMove=function(move,row,col,_14b,_14c){
    if(move.vars&&move.vars.length>0){
        var i=0;
        for(var i=0;i<move.vars.length;i++){
            var _14e=move.vars[i];
            if(_14e.fromColumn==_14b.column&&_14e.fromRow==_14b.row&&_14e.toRow==row&&_14e.toColumn==col&&(_14c==""||(_14c==_14e.promotion))){
                return _14e;
            }
        }
    }
};
Board.prototype.createMoveFromString=function(_14f){
    var _150=0;
    var take=false;
    var _152=null;
    var _153=_14f.charCodeAt(_150++);
    var _154=_14f.charCodeAt(_150++);
    var _155=_14f.split("|");
    var pgn=null;
    if(_155.length>1){
        pgn=_155[1];
        _14f=_155[0];
    }else{
        _14f=_155[0];
    }
    if(_14f.charAt(_150)=="x"){
        _150++;
        take=true;
    }
    var _157=_14f.charCodeAt(_150++);
    var _158=_14f.charCodeAt(_150++);
    if(_150<_14f.length){
        _152=_14f.charAt(_150);
    }
    var move=new Move(_153-("a".charCodeAt(0)),_154-("1".charCodeAt(0)),_157-("a".charCodeAt(0)),_158-("1".charCodeAt(0)),take,_152,_14f);
    move.pgn=pgn;
    return move;
};
Board.prototype.getBackButton=function(){
    if(!this.backButton){
        this.backButton=YAHOO.util.Dom.get(this.boardName+"-back");
    }
    return this.backButton;
};
Board.prototype.getForwardButton=function(){
    if(!this.forwardButton){
        this.forwardButton=YAHOO.util.Dom.get(this.boardName+"-forward");
    }
    return this.forwardButton;
};
Board.prototype.getEndButton=function(){
    if(!this.endButton){
        this.endButton=YAHOO.util.Dom.get(this.boardName+"-end");
    }
    return this.endButton;
};
Board.prototype.getStartButton=function(){
    if(!this.startButton){
        this.startButton=YAHOO.util.Dom.get(this.boardName+"-start");
    }
    return this.startButton;
};
Board.prototype.setForwardBack=function(){
    var back=this.getBackButton();
    var _15b=this.getForwardButton();
    var end=this.getEndButton();
    var _15d=this.getStartButton();
    if(!this.currentMove){
        if(back){
            back.src=this.boardImagePath+"/static/images/resultset_previous_disabled"+this.getVersString()+".gif";
        }
        if(_15d){
            _15d.src=this.boardImagePath+"/static/images/disabled_resultset_first"+this.getVersString()+".gif";
        }
        if(_15b){
            _15b.src=this.boardImagePath+"/static/images/resultset_next_disabled"+this.getVersString()+".gif";
        }
        if(end){
            end.src=this.boardImagePath+"/static/images/disabled_resultset_last"+this.getVersString()+".gif";
        }
        return;
    }
    if(this.currentMove.prev==null){
        if(back){
            back.src=this.boardImagePath+"/static/images/resultset_previous_disabled"+this.getVersString()+".gif";
        }
        if(_15d){
            _15d.src=this.boardImagePath+"/static/images/disabled_resultset_first"+this.getVersString()+".gif";
        }
    }else{
        if(back){
            back.src=this.boardImagePath+"/static/images/resultset_previous"+this.getVersString()+".gif";
        }
        if(_15d){
            _15d.src=this.boardImagePath+"/static/images/resultset_first"+this.getVersString()+".gif";
        }
    }
    if(this.currentMove.atEnd){
        if(_15b){
            _15b.src=this.boardImagePath+"/static/images/resultset_next_disabled"+this.getVersString()+".gif";
        }
        if(end){
            end.src=this.boardImagePath+"/static/images/disabled_resultset_last"+this.getVersString()+".gif";
        }
    }else{
        if(_15b){
            _15b.src=this.boardImagePath+"/static/images/resultset_next"+this.getVersString()+".gif";
        }
        if(end){
            end.src=this.boardImagePath+"/static/images/resultset_last"+this.getVersString()+".gif";
        }
    }
};
Board.prototype.convertPiecesFromLightWeight=function(_15e){
    var _15f=this.settingUpPosition;
    this.settingUpPosition=true;
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            if(this.boardPieces[i][j]!=null){
                var _162=this.boardPieces[i][j];
                var p=_162.makeHeavyWeight();
                this.boardPieces[i][j]=p;
                p.setPosition(p.column,p.row,false,null,this.moveAnimationLength);
                p.setVisible(true);
            }
        }
    }
    var move=this.moveArray[_15e];
    while(move!=null){
        if(move.taken){
            move.taken=move.taken.makeHeavyWeight();
        }
        move=move.prev;
    }
    this.settingUpPosition=_15f;
};
MovesDisplay.prototype.setToMove=function(_165){
    this.toMove=_165;
};
MovesDisplay.prototype.clickComment=function(e){
    var t=e.currentTarget?e.currentTarget:e.targetElement?e.targetElement:false;
    if(!t){
        t=YAHOO.util.Event.getTarget(e);
    }
    if(!t.id){
        t=t.parentNode;
    }
    var _168=t.id.substr((this.board.boardName+"-mcX").length);
    var _169=true;
    if(t.id.indexOf("-mca")>=0){
        _169=false;
    }
    var move=this.board.moveArray[_168];
    var _16b="";
    if(_169){
        _16b=move.beforeComment;
    }else{
        _16b=move.afterComment;
    }
    mySimpleDialog=new YAHOO.widget.SimpleDialog(this.boardName+"-editCommentDialog",{width:"20em",fixedcenter:true,modal:true,visible:false,draggable:false});
    mySimpleDialog.setHeader("Edit Comment");
    mySimpleDialog.setBody("<textarea id=\""+this.board.boardName+"-editComment\">"+_16b+"</textarea>");
    mySimpleDialog.cfg.setProperty("icon",YAHOO.widget.SimpleDialog.ICON_INFO);
    var me=this;
    var _16d=function(){
        if(_169){
            move.beforeComment=null;
        }else{
            move.afterComment=null;
        }
        t.innerHTML="";
        this.hide();
    };
    var _16e=function(){
        var _16f=YAHOO.util.Dom.get(me.board.boardName+"-editComment");
        var txt=trimStr(_16f.value);
        if(_169){
            move.beforeComment=txt;
        }else{
            move.afterComment=txt;
        }
        if(_169){
            t.innerHTML=me.outputComment(txt,0)+" ";
        }else{
            t.innerHTML=" "+me.outputComment(txt,0);
        }
        this.hide();
    };
    var _171=function(){
        this.hide();
    };
    var _172=[{text:"Delete",handler:_16d},{text:"Save",handler:_16e},{text:"Cancel",handler:_171,isDefault:true}];
    mySimpleDialog.cfg.queueProperty("buttons",_172);
    mySimpleDialog.render(document.body);
    mySimpleDialog.show();
};
MovesDisplay.prototype.gotoMove=function(e){
    if(this.board.disableNavigation){
        return;
    }
    if(this.board.tactics&&this.board.tactics.problemActive){
        return;
    }
    if(this.board.blockFowardBack||this.board.deferredBlockForwardBack){
        return;
    }
    activeBoard=this.board;
    var t=e.currentTarget?e.currentTarget:e.targetElement?e.targetElement:false;
    if(!t){
        t=YAHOO.util.Event.getTarget(e);
    }
    if(!t.id){
        t=t.parentNode;
    }
    var _175=t.id.substr((this.board.boardName+"-m").length);
    if(clog){
        console.log("got goto move index:"+_175);
    }
    this.board.gotoMoveIndex(_175,false,false,false,false);
    if(this.board.problem){
        if(this.board.currentMove.bestMoves){
            this.board.problem.showBestMoves(this.board.currentMove,this.board.currentMove.bestMoves,this.board.currentMove.correctMove,this.board.currentMove.wrongMove);
        }else{
            this.board.problem.clearBestMoves();
        }
    }
};
MovesDisplay.prototype.getMovesDisplay=function(){
    if(!this.cachedMovesDisplay&&!this.allreadyCachedMovesDisplay){
        var name=this.board.boardName+"-moves";
        if(this.moveListName){
            name=this.moveListName;
        }
        this.cachedMovesDisplay=YAHOO.util.Dom.get(name);
        this.allreadyCachedMovesDisplay=true;
    }
    return this.cachedMovesDisplay;
};
MovesDisplay.prototype.outputVariationStart=function(_177,_178,_179,_17a){
    var _17b="";
    if(_178>this.board.ml){
        return _17b;
    }
    if(this.board.ml==1&&_17a>1){
        return _17b;
    }
    var _17c=this.getMovesDisplay();
    if(_17c||this.board.outputWithoutDisplay){
        if(_177==0&&this.displayType==MovesDisplay.MAIN_ON_OWN_LINE){
            if(this.firstNonMove){
                if(this.board.useDivClearForNewline){
                    _17b+="<div style=\"clear:both;\"></div>";
                }
                _17b+="<div class=\"ct-mainline-commentary\"/>";
                this.pendingLevelZeroCommentaryClose=true;
            }
        }
        if(this.variationOnOwnLine){
            if(this.board.useDivClearForNewline){
                _17b+="<div style=\"clear:both;\"></div>";
            }else{
                _17b+="<br/>";
            }
        }
        if(this.board.showBracketsOnVariation&&(!this.board.hideBracketsOnTopLevelVariation||_177>0)){
            _17b+="<span>"+this.board.variationStartString+"</span>";
        }
    }
    this.firstNonMove=false;
    return _17b;
};
MovesDisplay.prototype.outputVariationEnd=function(_17d,_17e,_17f,_180){
    var _181=this.getMovesDisplay();
    var _182="";
    if(this.board.ml==1&&_17e>0&&this.board.outputFirstVar){
        return _182;
    }
    this.board.outputFirstVar=true;
    if(_181||this.board.outputWithoutDisplay){
        if(this.board.showBracketsOnVariation&&(!this.board.hideBracketsOnTopLevelVariation||_17d>1)){
            _182+="<span>"+this.board.variationEndString+"</span>";
        }
    }
    if(_17d==1&&this.displayType==MovesDisplay.MAIN_ON_OWN_LINE){
    }
    this.firstNonMove=false;
    return _182;
};
MovesDisplay.prototype.outputComment=function(_183,_184,_185,_186){
    if(this.board.ignoreCommentRegex){
        var _187=new RegExp(this.board.ignoreCommentRegex);
        if(_187.test(_183)){
            return "";
        }
    }
    var _188="";
    if(this.board.ml==1){
        return _188;
    }
    var _189=this.getMovesDisplay();
    if(_189||this.board.outputWithoutDisplay){
        if(_184==0&&this.displayType==MovesDisplay.MAIN_ON_OWN_LINE){
            if(this.firstNonMove){
                _188+="<br/>";
            }
            _188+="<div class=\"ct-mainline-commentary\">";
            this.pendingLevelZeroCommentaryClose=true;
        }
        var _18a="ct-board-move-comment";
        if(_185){
            _18a="ct-board-move-alt-comment";
        }
        if(this.board.handleCommentClicks){
            _18a+=" ct-board-clickable-comment";
        }
        _188+="<span class=\""+_18a+"\"> "+_183+" </span>";
        if(_184==0&&this.displayType==MovesDisplay.MAIN_ON_OWN_LINE){
        }
    }
    if(!_186){
        this.firstNonMove=false;
    }
    return _188;
};
MovesDisplay.prototype.outputNag=function(_18b){
    var _18c="";
    var _18d=this.getMovesDisplay();
    if(_18d||this.board.outputWithoutDisplay){
        var _18e=null;
        switch(_18b){
            case 11:
                _18e="=";
                break;
            case 14:
                _18e="+=";
                break;
            case 15:
                _18e="=+";
                break;
            case 16:
                _18e="+/-";
                break;
            case 17:
                _18e="-/+";
                break;
            case 18:
                _18e="+-";
                break;
            case 19:
                _18e="-+";
                break;
            case 20:
                _18e="+--";
                break;
            case 21:
                _18e="--+";
                break;
            default:
        }
        if(_18e){
            _18c+="<span> "+_18e+" </span>";
        }
    }
    return _18c;
};
MovesDisplay.prototype.outputResult=function(_18f){
    return "<span class=\"ct-result\">"+_18f+"</span>";
};
MovesDisplay.prototype.outputMove=function(_190,_191,_192,_193,_194,_195,_196,move,_198,_199,_19a,_19b,_19c,_19d,_19e){
    var clog=false;
    if(clog){
        console.log("outputMove:"+_193+" hideScore:"+_198+" this.board:"+this.board);
    }
    var _1a0="";
    var _1a1=this.getMovesDisplay();
    if(this.board.tr&&_191>0&&(_195>1||_196>3)&&!_194){
        return _1a0;
    }
    if(clog){
        console.log("ravLevel:"+_191+" ravCount:"+_195+" topCount:"+_196+" output:"+_193);
    }
    if(this.board.ml==1&&_195>0&&this.board.outputFirstVar){
        return _1a0;
    }
    if(clog){
        console.log("movesDisplay:"+_1a1);
    }
    if(_1a1||this.board.outputWithoutDisplay){
        var _1a2=""+Math.round(_192/2)+". ";
        var _1a3=false;
        if(_192%2!=1){
            if(clog){
                console.log("firstRav:"+_194+" firstNonMove:"+this.firstNonMove);
            }
            if(_194||!this.firstNonMove){
                _1a2=Math.round(_192/2)+"... ";
                _1a3=true;
            }else{
                _1a2="";
            }
        }
        if(clog){
            console.log("moveNum:"+_192+" moveNumOut:"+_1a2);
        }
        if(this.displayType==MovesDisplay.MAIN_ON_OWN_LINE&&_191==0&&(!this.firstNonMove||_192%2==1)){
            if(this.pendingLevelZeroCommentaryClose){
                this.pendingLevelZeroCommentaryClose=false;
                _1a0+="</div>";
            }
            if(this.board.newlineForEachMainMove){
                if(this.board.useDivClearForNewline){
                    _1a0+="<div style=\"clear:both;\"></div>";
                }else{
                    _1a0+="<br/>";
                }
            }
        }
        var _1a4="";
        var _1a5="";
        if(move&&move.eg_move){
            var res=this.board.egMoveToScoreString(move.eg_move);
            _1a4=res[0];
            _1a5=res[1];
        }
        var _1a7="";
        if(_198){
            _1a7="initially_hidden";
        }
        if(_1a4!=""){
            _1a4=" "+_1a4;
        }
        var _1a8="title";
        if(_198){
            _1a8="alt";
        }
        var _1a9="";
        if(_199){
            _1a9=" rel=\""+_193+"\" ";
            _193="___";
        }
        var _1aa="";
        if(_1a3&&_191==0){
            _1aa="<span class=\"ct-board-move-dottedempty\">&nbsp;</span>";
        }
        var _1ab="";
        if(_1a2){
            _1ab="<span class=\"ct-board-move-movenum\">"+_1a2+"</span>";
        }
        var _1ac="";
        if(_190==0){
            if(_19a){
                _1ac=" ct-best-move ";
            }else{
                if(_19c){
                    _1ac=" ct-bad-move ";
                }else{
                    if(_19b){
                        _1ac=" ct-good-move ";
                    }else{
                        if(_19d){
                            _1ac=" ct-current-move ";
                        }else{
                            _1ac=" ct-first-move ";
                        }
                    }
                }
            }
        }
        if(_19e){
            _1ac=" ct-current-move ";
        }
        _1a0+="<span "+_1a9+_1a8+"=\""+_1a5+"\" id=\""+this.board.boardName+"-m"+_190+"\" class=\""+((_191==0)?"ct-board-move-mainline":"ct-board-move-variation")+_1ac+"\">"+_1ab+_1aa+"<span class=\"ct-board-move-movetext\">"+_193+"</span><span id=\""+this.board.boardName+"-msc"+_190+"\" class=\""+_1a7+"\">"+_1a4+"</span></span>";
    }
    this.firstNonMove=true;
    return _1a0;
};
Board.prototype.setMoveSeqLalg=function(_1ad,_1ae,_1af,_1b0,_1b1,_1b2,_1b3,_1b4,_1b5,_1b6,_1b7,_1b8){
    var _1b9=new Array();
    if(_1ad&&_1ad.length>0){
        _1b9=_1ad.replace(/\s+$/g,"").split(" ");
    }
    this.setupFromLalgArray(_1b9,_1b0,_1af,_1ae,_1b1,_1b2,_1b3,_1b4,_1b5,_1b6,_1b7,_1b8);
};
Board.prototype.setupFromLalgArray=function(_1ba,_1bb,_1bc,_1bd,_1be,_1bf,_1c0,_1c1,_1c2,_1c3,_1c4,_1c5){
    var clog=false;
    if(clog){
        console.log("top of setupFromLalgArray");
    }
    this.outputFirstVar=false;
    if(this.movesDisplay){
        this.movesDisplay.pendingLevelZeroCommentaryClose=false;
        var md=this.movesDisplay.getMovesDisplay();
        if(md){
            if(!_1bf){
                YAHOO.util.Event.purgeElement(md,true);
            }
            md.innerHTML="";
        }
    }
    if(!_1bd){
        _1bd=new Array();
    }
    var _1c8=this.cloneBoard();
    this.movesDisplay.firstNonMove=false;
    var _1c9=null;
    var _1ca=null;
    if(!_1be){
        _1c9=new Array();
        _1ca=new Array();
    }
    if(!_1c3&&this.prev_move){
        if(clog){
            console.log("this.prev_move:"+this.prev_move.output());
        }
        if(_1c8.boardPieces[this.prev_move.fromColumn][this.prev_move.fromRow]){
            _1c8.makeMove(this.prev_move,_1c8.boardPieces[this.prev_move.fromColumn][this.prev_move.fromRow],false,_1c8.moveAnimationLength,false,false);
        }
    }
    var _1cb=null;
    if(!_1be){
        _1cb=_1c8.cloneBoard();
    }
    var _1cc=null;
    var _1cd=0;
    var _1ce="";
    var _1cf=false;
    var _1d0=false;
    var _1d1=0;
    var _1d2=false;
    var _1d3=new Array();
    var _1d4=new Array();
    _1d4[0]=0;
    var _1d5=new Array();
    var _1d6=new Array();
    var _1d7=_1bc*2-1;
    var _1d8=_1bc*2-1;
    var _1d9=new Array();
    var _1da=ChessPiece.WHITE;
    var _1db=0;
    var eval="";
    var _1dd="";
    var _1de="";
    var time="";
    var _1e0=-1;
    var _1e1=0;
    for(var i=0;i<_1ba.length;i++){
        var _1e3=0;
        if(clog){
            console.log("movesArr["+i+"]:"+_1ba[i]);
        }
        if(_1ba[i]=="ALT"){
            _1d0=true;
            continue;
        }
        if(_1ba[i].indexOf("EVAL")==0){
            eval=_1ba[i].split(":")[1];
            if(parseInt(eval)>=175&&_1d1>0&&_1d4[_1d1]>1){
                _1d0=true;
            }
            continue;
        }
        if(_1ba[i].indexOf("DEPTH")==0){
            _1dd=_1ba[i].split(":")[1];
            continue;
        }
        if(_1ba[i].indexOf("NODES")==0){
            _1de=_1ba[i].split(":")[1];
            continue;
        }
        if(_1ba[i].indexOf("TIME")==0){
            time=_1ba[i].split(":")[1];
            var e=eval;
            if(eval.indexOf("mate")!=0){
                e=(parseFloat(eval)/100).toFixed(2);
                if(e>0){
                    e="+"+e;
                }
            }else{
                e=e.replace(/_/," ");
                var _1e5=e.split(" ");
                _1e3=parseInt(_1e5[1]);
                e=_js("mate")+" "+_1e5[1];
                if(_1d4[_1d1]==1){
                    _1e0=_1e3;
                }
            }
            _1e1=_1e3;
            if(_1e3<0){
                _1d0=false;
            }else{
                if(_1e3>0&&_1e3<8&&_1d1>0&&_1d4[_1d1]>1){
                    _1d0=true;
                }
            }
            var _1e6="";
            if(_1d0){
                _1e6=_js("ALT")+" ";
            }
            var t=parseInt(time);
            var nps=" "+__js("nps:{NODES_PER_SECOND}",[["NODES_PER_SECOND",Math.round(parseInt(_1de)/(parseInt(time)/1000))]]);
            if(!this.showNPS){
                nps="";
            }
            if(!(_1d1>0&&_1d4[_1d1]>this.ml)){
                _1ba[i]=_1e6+e+" ("+__js("depth:{DEPTH}",[["DEPTH",_1dd]])+nps+")";
            }else{
                _1ba[i]="";
            }
        }
        if(_1ba[i]=="}"){
            _1cf=false;
            if(this.movesDisplay){
                _1ce=_1ce.replace(/\s+$/g,"");
                _1d9.push(this.movesDisplay.outputComment(_1ce,_1d1,_1d0));
            }
            continue;
        }else{
            if(_1cf){
                _1ce+=_1ba[i]+" ";
                continue;
            }else{
                if(_1ba[i]=="{"){
                    _1ce="";
                    _1cf=true;
                    continue;
                }else{
                    if(_1ba[i]=="("){
                        if(!_1d4[_1d1+1]){
                            _1d4[_1d1+1]=0;
                        }
                        _1d4[_1d1+1]++;
                        if(this.movesDisplay){
                            _1d9.push(this.movesDisplay.outputVariationStart(_1d1,_1d4[_1d1+1],_1d7,_1d3[0]));
                        }
                        _1d3[_1d1]=_1d7;
                        _1d5[_1d1]=_1cc;
                        _1d6[_1d1]=_1da;
                        _1c9[_1d1]=_1c8;
                        _1ca[_1d1]=_1cb;
                        _1c8=_1cb.cloneBoard();
                        _1d1++;
                        _1d7--;
                        _1d2=true;
                        continue;
                    }else{
                        if(_1ba[i]==")"){
                            if(this.movesDisplay){
                                _1d9.push(this.movesDisplay.outputVariationEnd(_1d1,_1d4[_1d1],_1d7,_1d3[0]));
                            }
                            var _1e9=new Move();
                            _1e9.atEnd=true;
                            _1cc.next=_1e9;
                            _1e9.prev=_1cc;
                            _1d1--;
                            _1d7=_1d3[_1d1];
                            _1cc=_1d5[_1d1];
                            _1da=_1d6[_1d1];
                            _1c8=_1c9[_1d1];
                            _1cb=_1ca[_1d1];
                            _1d0=false;
                            continue;
                        }else{
                            if(_1ba[i].charAt(0)=="$"){
                                if(this.movesDisplay){
                                    _1d9.push(this.movesDisplay.outputNag(parseInt(_1ba[i].substring(1))));
                                }
                                continue;
                            }
                        }
                    }
                }
            }
        }
        var move=this.createMoveFromString(_1ba[i]);
        var _1eb=false;
        if(_1d7==_1d8&&this.boardPieces[move.fromColumn][move.fromRow].colour==ChessPiece.BLACK){
            _1d7++;
            _1eb=true;
            _1da=ChessPiece.BLACK;
        }
        move.index=_1cd;
        var _1ec=(move.pgn)?move.pgn:move.moveString;
        if(move.pgn){
            _1ec=move.pgn;
        }else{
            _1ec=_1c8.makeShortAlgabraic(move.fromColumn,move.fromRow,move.toColumn,move.toRow,move);
            move.SAN=_1ec;
        }
        _1ec=Board.moveToLocale(_1ec);
        if(this.movesDisplay){
            this.movesDisplay.setToMove(_1da);
            var _1ed=false;
            if(_1c4&&_1c5&&!_1c5.atEnd){
                var _1ee=_1c5.toMoveString();
                _1c5=_1c5.next;
                if(_1ee==_1ba[i]){
                    _1ed=true;
                }else{
                    _1c4=false;
                }
            }
            _1d9.push(this.movesDisplay.outputMove(_1cd,_1d1,_1d7,_1ec+" ",_1d2,_1d4[_1d1],_1d3[0],null,false,false,_1c0,_1c1,_1c2,_1c4,_1ed));
        }
        _1da=(_1da==ChessPiece.BLACK)?ChessPiece.WHITE:ChessPiece.BLACK;
        move.moveNum=_1d7;
        _1d7++;
        if(_1d1>0){
            if(_1d2){
                var _1ef=_1cc;
                if(_1ef==null){
                    alert("Got no previous move for variation:"+movesArra[i]);
                }
                if(_1ef.numVars==0){
                    _1ef.vars=new Array();
                }
                move.isAlt=_1d0;
                move.mateInMoves=_1e1;
                _1ef.vars[_1ef.numVars++]=move;
                move.prev=_1ef.prev;
                _1d2=false;
            }else{
                move.prev=_1cc;
                if(_1cc!=null){
                    _1cc.next=move;
                }
            }
        }else{
            move.prev=_1cc;
            if(_1cc!=null){
                _1cc.next=move;
            }
        }
        _1d4[_1d1+1]=0;
        if(_1d1==0){
            _1db=_1cd;
        }
        _1bd[_1cd++]=move;
        _1c8.moveArray[_1cd-1]=move;
        _1cc=move;
        if(!_1be){
            _1cb=_1c8.cloneBoard();
        }
        _1c8.makeMove(move,_1c8.boardPieces[move.fromColumn][move.fromRow],false,_1c8.moveAnimationLength,false,false);
    }
    if(this.movesDisplay&&!this.disableMoveOutput){
        var _1f0=this.movesDisplay.getMovesDisplay();
        _1d9.push(this.movesDisplay.outputResult(_1bb));
        this.pendingMovesOutput=_1d9.join("");
        this.pendingMovesOutputCount=_1cd;
    }
    this.lastMoveIndex=_1db;
    if(_1cc!=null){
        var _1e9=new Move();
        _1e9.atEnd=true;
        _1cc.next=_1e9;
        _1e9.prev=_1cc;
    }
    this.lastCount=_1cd;
};
Board.prototype.getMaterialCount=function(){
    var _1f1=0;
    var _1f2=0;
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var _1f5=this.boardPieces[i][j];
            if(_1f5){
                if(_1f5.colour==ChessPiece.WHITE){
                    _1f1+=ChessPiece.materialValue(_1f5.piece);
                }else{
                    _1f2+=ChessPiece.materialValue(_1f5.piece);
                }
            }
        }
    }
    return [_1f1,_1f2];
};
Board.prototype.getMaterialBalance=function(){
    var cnt=this.getMaterialCount();
    return cnt[0]-cnt[1];
};
Board.prototype.getMaterialBalances=function(){
    var _1f7=this.cloneBoard();
    var mv=this.moveArray[0];
    _1f7.gotoMoveIndex(-1,true,true,true,true);
    var _1f9=[];
    while(mv&&!mv.atEnd){
        _1f7.makeMove(mv,_1f7.boardPieces[mv.fromColumn][mv.fromRow],false,this.moveAnimationLength,false,false);
        _1f9.push(_1f7.getMaterialBalance());
        mv=mv.next;
        _1f7.toggleToMove();
    }
    return _1f9;
};
Board.prototype.lalgToMoveList=function(_1fa,_1fb,_1fc,_1fd,_1fe,_1ff){
    if(ctime){
        console.time("lalgToMoveList");
    }
    if(clog){
        console.log("startMoveNum:"+_1fc);
    }
    if(!_1fd){
        _1fd=new Array();
    }
    var _200=this.cloneBoard();
    var _201=null;
    var _202=null;
    if(!_1ff){
        _201=new Array();
        _202=new Array();
    }
    if(!_1fe&&this.prev_move){
        _200.makeMove(this.prev_move,_200.boardPieces[this.prev_move.fromColumn][this.prev_move.fromRow],false,_200.moveAnimationLength,false,false);
    }
    var _203=null;
    if(!_1ff){
        _203=_200.cloneBoard();
    }
    var nags=[];
    var _205=null;
    var _206=0;
    var _207="";
    var _208=false;
    var _209=0;
    var _20a=false;
    var _20b=new Array();
    var _20c=new Array();
    _20c[0]=0;
    var _20d=new Array();
    var _20e=new Array();
    var _20f=_1fc*2-1;
    var _210=new Array();
    var _211=ChessPiece.WHITE;
    var _212=0;
    var _213=true;
    for(var i=0;i<_1fa.length;i++){
        if(_1fa[i]=="}"){
            _208=false;
            _207=_207.replace(/\s+$/g,"");
            continue;
        }else{
            if(_208){
                _207+=_1fa[i]+" ";
                continue;
            }else{
                if(_1fa[i]=="{"){
                    if(_207){
                        if(_205){
                            _205.afterComment=trimStr(_207);
                        }
                    }
                    _207="";
                    _208=true;
                    continue;
                }else{
                    if(_1fa[i]=="("){
                        if(clog){
                            console.log("var start comment:"+_207);
                        }
                        if(_205){
                            _205.afterComment=trimStr(_207);
                            _207="";
                        }
                        if(clog){
                            if(_205){
                                console.log("old:"+_205.output());
                            }else{
                                console.log("no old move");
                            }
                        }
                        if(!_20c[_209+1]){
                            _20c[_209+1]=0;
                        }
                        _20c[_209+1]++;
                        _20b[_209]=_20f;
                        _20d[_209]=_205;
                        _20e[_209]=_211;
                        _201[_209]=_200;
                        _202[_209]=_203;
                        _200=_203.cloneBoard();
                        _209++;
                        _20f--;
                        _20a=true;
                        continue;
                    }else{
                        if(_1fa[i]==")"){
                            if(_205){
                                if(clog){
                                    console.log("var end comment:"+_207);
                                    console.log("var end comment:"+_205.output());
                                }
                                _205.afterComment=trimStr(_207);
                                _207="";
                            }
                            var _215=new Move();
                            _215.atEnd=true;
                            _205.next=_215;
                            _215.prev=_205;
                            _209--;
                            _20f=_20b[_209];
                            _205=_20d[_209];
                            _211=_20e[_209];
                            _200=_201[_209];
                            _203=_202[_209];
                            continue;
                        }else{
                            if(_1fa[i].charAt(0)=="$"){
                                nags.push(parseInt(_1fa[i].substring(1)));
                                continue;
                            }
                        }
                    }
                }
            }
        }
        var move=this.createMoveFromString(_1fa[i]);
        move.nags=nags;
        move.beforeComment=trimStr(_207);
        _207=null;
        nags=[];
        if(_213){
            if(this.boardPieces[move.fromColumn][move.fromRow].colour==ChessPiece.BLACK){
                _20f++;
                _211=ChessPiece.BLACK;
                if(clog){
                    console.log("first move black new movenum:"+_20f);
                }
            }
            _213=false;
        }
        move.index=_206;
        var _217=(move.pgn)?move.pgn:move.moveString;
        if(move.pgn){
            _217=move.pgn;
            move.SAN=move.pgn;
        }else{
            _217=_200.makeShortAlgabraic(move.fromColumn,move.fromRow,move.toColumn,move.toRow,move);
            move.SAN=_217;
        }
        _211=(_211==ChessPiece.BLACK)?ChessPiece.WHITE:ChessPiece.BLACK;
        move.moveNum=_20f;
        _20f++;
        if(_209>0){
            if(_20a){
                var _218=_205;
                if(_218==null){
                    alert("Got no previous move for variation:"+movesArra[i]);
                }
                if(_218.numVars==0){
                    _218.vars=new Array();
                }
                _218.vars[_218.numVars++]=move;
                move.prev=_218.prev;
                _20a=false;
            }else{
                move.prev=_205;
                if(_205!=null){
                    _205.next=move;
                }
            }
        }else{
            move.prev=_205;
            if(_205!=null){
                _205.next=move;
            }
        }
        _20c[_209+1]=0;
        if(_209==0){
            _212=_206;
        }
        _1fd[_206++]=move;
        _200.moveArray[_206-1]=move;
        _205=move;
        if(!_1ff){
            _203=_200.cloneBoard();
        }
        _200.makeMove(move,_200.boardPieces[move.fromColumn][move.fromRow],false,_200.moveAnimationLength,false,false);
    }
    if(_205!=null){
        var _215=new Move();
        _215.atEnd=true;
        _205.next=_215;
        _215.prev=_205;
        if(_207){
            _205.afterComment=trimStr(_207);
        }
    }
    if(ctime){
        console.timeEnd("lalgToMoveList");
    }
    return _1fd;
};
Board.prototype.reset=function(fen,_21a){
    if(this.lastFromSquare){
        YAHOO.util.Dom.removeClass(this.lastFromSquare,"ct-from-square");
    }
    if(this.lastToSquare){
        YAHOO.util.Dom.removeClass(this.lastToSquare,"ct-to-square");
    }
    this.clearMoveList();
    if(fen){
        this.startFen=fen;
        this.setupFromFen(fen,false,this.isFlipped,false,_21a,true);
    }else{
        this.startFen=Board.INITIAL_FEN;
        this.setupFromFen(Board.INITIAL_FEN,false,this.isFlipped,false,false,true);
    }
    this.setForwardBack();
};
Board.prototype.clearMoveList=function(_21b){
    this.movesDisplay.firstNonMove=false;
    var _21c=this.movesDisplay.getMovesDisplay();
    if(_21c){
        YAHOO.util.Event.purgeElement(_21c,true);
        _21c.innerHTML="";
    }
    this.currentMove=null;
    this.moveIndex=-1;
    this.moveArray=new Array();
    if(_21b){
        _21b.prev=null;
        this.startMoveNum=_21b.moveNum;
    }else{
        this.startMoveNum=1;
    }
};
Board.prototype.insertMovesFromMoveList=function(_21d,_21e,_21f,_220,_221){
    var _222=!_21e;
    if(clog){
        console.log("insertMovesFromMoveList called");
    }
    if(ctime&&_222){
        console.time("insertMovesFromMoveList");
    }
    if(!this.movesDisplay){
        return;
    }
    if(_222){
        this.clearMoveList(_21d);
    }
    var _223=0;
    var _224=_21d.moveNum;
    var move=_21d;
    while(move!=null&&!move.atEnd){
        if(clog){
            console.log("move:"+move.output());
        }
        var _226=move.next;
        if(clog){
            if(this.currentMove){
                console.log("current move:"+this.currentMove.output());
            }else{
                console.log("no current move");
            }
            if(_226){
                console.log("next move:"+_226.output());
            }else{
                console.log("no next move");
            }
        }
        if(_222||_21d!=move||_21f==null){
            if(clog){
                console.log("about to call insertmoveafter");
            }
            if(_220!=null){
                if(clog){
                    console.log("inserting after moveToInsertAfter:"+_220.output());
                }
                this.insertMoveAfter(_220,move);
                _220=null;
            }else{
                if(clog){
                    console.log("inserting after current move");
                }
                this.insertMoveAfter(this.currentMove,move);
            }
            if(clog){
                console.log("finished call to insertmoveafter");
            }
        }else{
            if(clog){
                console.log("about to replace variationParent:"+_21f.output()+" with move:"+move.output()+" and board:"+this.boardToFen());
            }
            this.replaceMove(_21f,move,true,true,false,false,true);
        }
        if(move.beforeComment){
            this.insertCommentIntoMoveDisplay(move,move.beforeComment,false);
        }
        if(move.afterComment){
            this.insertCommentIntoMoveDisplay(move,move.afterComment,true);
        }
        if(clog){
            console.log("about to make move:"+move.output()+" with board pos:"+this.boardToFen());
        }
        this.makeMove(move,this.boardPieces[move.fromColumn][move.fromRow],false,this.moveAnimationLength,false,false);
        if(clog){
            console.log("made move");
        }
        this.setCurrentMove(move,true,true);
        if(move.numVars>0){
            var _227=move.index;
            var bm=move.prev;
            var _229=-1;
            if(bm){
                _229=bm.index;
            }
            var _22a=move.numVars;
            var vars=move.vars;
            move.numVars=0;
            move.vars=[];
            for(var i=0;i<_22a;i++){
                this.gotoMoveIndex(_229,true,true,true,true);
                if(clog){
                    console.log("about to call insertMovesFromMoveList with head of variation");
                }
                this.insertMovesFromMoveList(vars[i],true,move,null,0);
                if(clog){
                    console.log("about to reset currentMoveIndex  after variation insert:"+_227);
                }
            }
            this.gotoMoveIndex(_227,true,true,true,true);
            this.backMove();
            var cm=this.currentMove;
            this.makeMove(cm,this.boardPieces[cm.fromColumn][cm.fromRow],false,this.moveAnimationLength,false,false);
            if(clog){
                if(this.currentMove){
                    console.log("popped up from variation, current set back to:"+this.currentMove.output());
                }else{
                    console.log("popped up from variation, current set to null");
                }
            }
        }
        move=_226;
        _223++;
        if(_221>0&&_223>=_221){
            break;
        }
    }
    if(_222){
        this.gotoMoveIndex(-1,false,false,false,false);
    }
    if(clog){
        var m=this.currentMove;
        while(m){
            console.log("m:"+m.output());
            m=m.next;
        }
    }
    if(ctime&&_222){
        console.timeEnd("insertMovesFromMoveList");
    }
};
Board.prototype.setupFromLalgArrayIncremental=function(_22f,_230,_231,_232){
    this.outputFirstVar=false;
    if(this.movesDisplay&&this.lastCount){
        this.movesDisplay.pendingLevelZeroCommentaryClose=false;
        for(var i=0;i<this.lastCount;i++){
            var mv=YAHOO.util.Dom.get(this.boardName+"-m"+i);
            if(mv){
                YAHOO.util.Event.purgeElement(mv);
            }
        }
    }
    var _235=0;
    var _236=_231*2-1;
    var _237="";
    var _238=false;
    var _239=false;
    var _23a=ChessPiece.WHITE;
    var _23b=false;
    var _23c=true;
    this.currentMove=null;
    for(var i=0;i<_22f.length;i++){
        if(_22f[i]=="}"){
            _23b=false;
            if(this.movesDisplay){
                _237=_237.replace(/\s+$/g,"");
            }
            continue;
        }else{
            if(_23b){
                _237+=_22f[i]+" ";
                continue;
            }else{
                if(_22f[i]=="{"){
                    _237="";
                    _23b=true;
                    continue;
                }else{
                    if(_22f[i]=="("){
                        _238=true;
                        continue;
                    }else{
                        if(_22f[i]==")"){
                            _239=true;
                            continue;
                        }else{
                            if(_22f[i].charAt(0)=="$"){
                                continue;
                            }
                        }
                    }
                }
            }
        }
        var move=this.createMoveFromString(_22f[i]);
        var _23e=false;
        if(_23c&&this.boardPieces[move.fromColumn][move.fromRow].colour==ChessPiece.BLACK){
            _236++;
            _23e=true;
            _23a=ChessPiece.BLACK;
        }
        this.startMoveNum=_236;
        _23c=false;
        move.index=_235++;
        var _23f=move.moveString;
        _23f=Board.moveToLocale(_23f);
        _23a=(_23a==ChessPiece.BLACK)?ChessPiece.WHITE:ChessPiece.BLACK;
        this.insertMoveAfter(this.currentMove,move);
        if(clog){
            if(move.prev){
                if(move.prev.next){
                    console.log("move.prev.next:"+move.prev.next.output());
                }else{
                    console.log("move.prev:"+move.prev.output()+" next null");
                }
            }
        }
        this.makeMove(move,this.boardPieces[move.fromColumn][move.fromRow],false,this.moveAnimationLength,false,false);
        this.setCurrentMove(move);
    }
    this.gotoMoveIndex(-1,false,false,false,false);
};
Board.prototype.displayPendingMoveList=function(){
    if(this.pendingMovesOutput&&this.movesDisplay){
        var _240=this.movesDisplay.getMovesDisplay();
        if(_240){
            _240.innerHTML=this.pendingMovesOutput;
            var _241=new YAHOO.util.Scroll(_240,{scroll:{to:[0,0]}},0);
            _241.animate();
        }
        if(this.movesDisplay){
            for(var i=0;i<this.pendingMovesOutputCount;i++){
                var mv1=YAHOO.util.Dom.get(this.boardName+"-m"+i);
                if(mv1){
                    YAHOO.util.Event.addListener(mv1,"click",this.movesDisplay.gotoMove,this.movesDisplay,true);
                    if(this.handleCommentClicks){
                        var _244=YAHOO.util.Dom.get(this.boardName+"-mcb"+i);
                        if(_244){
                            YAHOO.util.Event.addListener(_244,"click",this.movesDisplay.clickComment,this.movesDisplay,true);
                        }
                        _244=YAHOO.util.Dom.get(this.boardName+"-mca"+i);
                        if(_244){
                            YAHOO.util.Event.addListener(_244,"click",this.movesDisplay.clickComment,this.movesDisplay,true);
                        }
                    }
                }
            }
        }
    }
};
Board.prototype.setMoveSequence=function(_245,_246,_247,_248){
    this.tacticMoveArray=new Array();
    this.moveArray=this.tacticMoveArray;
    this.setMoveSeqLalg(_245,this.tacticMoveArray,_247,_248);
    this.tacticsmoveArrayLastMoveIndex=this.lastMoveIndex;
    if(false&&_246!="NA"){
        this.fullmoveArray=new Array();
        this.disableMoveOutput=true;
        this.setMoveSeqLalg(_246,this.fullmoveArray,_247,_248);
        this.disableMoveOutput=false;
        this.fullmoveArrayLastMoveIndex=this.lastMoveIndex;
    }else{
        this.fullmoveArray=null;
    }
    this.lastMoveIndex=this.tacticsmoveArrayLastMoveIndex;
};
Board.prototype.resetVariationsPreviousNodes=function(_249,_24a){
    if(_249.numVars>0){
        for(var i=0;i<_249.numVars;i++){
            _249.vars[i].prev=_24a;
            this.resetVariationsPreviousNodes(_249.vars[i],_24a);
        }
    }
};
Board.prototype.reconnectNextNodeVariations=function(_24c,_24d){
    if(!_24d){
        return;
    }
    if(_24d.numVars>0){
        for(var i=0;i<_24d.numVars;i++){
            _24d.vars[i].prev=_24c;
            this.reconnectNextNodeVariations(_24c,_24d.vars[i]);
        }
    }
};
Board.prototype.findFirstMoveFromList=function(move){
    var m=move;
    while(m&&m.prev!=null){
        m=m.prev;
    }
    return m;
};
Board.prototype.findVariationHeadFromMove=function(move){
    var m=move;
    while(m&&m.prev&&m.prev.next==m){
        m=m.prev;
    }
    if(m&&m.prev&&m.prev.next!=m){
        return m;
    }else{
        if(m&&!m.prev){
            var _253=this.moveArray[0];
            if(m!=_253){
                return m;
            }
        }
        return null;
    }
};
Board.prototype.liftVariation=function(_254){
    if(!_254){
        return;
    }
    var _255=null;
    var _256=null;
    if(_254.prev){
        _255=_254.prev.next;
    }else{
        _255=this.moveArray[0];
        _256=_254;
    }
    var _257=null;
    if(this.currentMove&&this.currentMove.prev){
        _257=this.currentMove.prev;
    }
    if(_255){
        var _258=_255.numVars;
        var vars=_255.vars;
        _255.numVars=0;
        _255.vars=[];
        if(_254.numVars==0){
            _254.vars=[];
        }
        for(var i=0;i<_258;i++){
            var _25b=vars[i];
            if(clog){
                console.log("processing var:"+_25b.output());
            }
            if(_25b==_254){
                if(clog){
                    console.log("inserted parent var");
                }
                _254.vars.push(_255);
                _254.numVars++;
            }else{
                _254.vars.push(_25b);
                _254.numVars++;
            }
        }
        if(_254.prev){
            _254.prev.next=_254;
        }
        if(clog){
            console.log("finished moving variations");
        }
        if(!_256){
            _256=this.findFirstMoveFromList(_254);
        }
        this.moveArray[0]=_256;
        this.gotoMoveIndex(-1,true,true,true,true);
        if(clog){
            console.log("fm:"+_256.output());
        }
        this.insertMovesFromMoveList(_256);
    }
    if(_257){
        this.gotoMoveIndex(_257.index);
    }
};
Board.prototype.deleteMoveAndLine=function(move){
    var m=move;
    var oldM=m;
    var _25f=false;
    var _260=null;
    var _261=this.moveArray[0];
    var _262=null;
    if(clog){
        console.log("delete line:"+move.output());
    }
    if(clog){
        console.log("delete line prev:"+move.prev);
    }
    if(clog&&move.prev){
        console.log("delete line prev.next:"+move.prev.next);
    }
    if(move&&move.prev&&move.prev.next!=move){
        if(clog){
            console.log("var is head and not front of move list");
        }
        _25f=true;
        _260=move.prev.next;
    }else{
        if(move&&!move.prev&&move!=this.moveArray[0]){
            if(clog){
                console.log("var is head and front of move list");
            }
            _25f=true;
            _260=this.moveArray[0];
        }
    }
    if(clog){
        console.log("isVariationHead:"+_25f);
    }
    if(clog){
        console.log("fm:"+_261.output());
    }
    var _263=m.prev;
    if(_25f){
        _262=_260;
        if(_260){
            if(clog){
                console.log("delete variation from parent:"+_260.output());
            }
            var _264=[];
            for(var i=0;i<_260.numVars;i++){
                if(!(_260.vars[i]==oldM)){
                    if(clog){
                        console.log("saving var:"+_260.vars[i].output());
                    }
                    _264.push(_260.vars[i]);
                }else{
                    if(clog){
                        console.log("dropping var:"+_260.vars[i].output());
                    }
                }
            }
            _260.vars=_264;
            _260.numVars=_264.length;
        }
    }else{
        if(_263){
            _263.next=null;
            _262=_263;
        }else{
            if(clog){
                console.log("deleting entire list");
            }
            if(this.movesDisplay){
                this.movesDisplay.firstNonMove=false;
                YAHOO.util.Event.purgeElement(this.movesDisplay.getMovesDisplay(),true);
                this.movesDisplay.pendingLevelZeroCommentaryClose=false;
            }
            var _266=this.movesDisplay.getMovesDisplay();
            if(_266){
                _266.innerHTML="";
            }
            this.currentMove=null;
            this.startMoveNum=_261.moveNum;
            if(clog){
                console.log("startFen:"+this.startFen);
            }
            this.moveIndex=-1;
            this.moveArray=[];
            this.setupFromFen(this.startFen);
            if(this.lastFromSquare){
                YAHOO.util.Dom.removeClass(this.lastFromSquare,"ct-from-square");
            }
            if(this.lastToSquare){
                YAHOO.util.Dom.removeClass(this.lastToSquare,"ct-to-square");
            }
            this.setForwardBack();
            return;
        }
    }
    this.moveArray[0]=_261;
    this.gotoMoveIndex(-1,true,true,true,true);
    if(clog){
        console.log("fm:"+_261.output());
    }
    this.insertMovesFromMoveList(_261);
    if(_262){
        this.gotoMoveIndex(_262.index);
    }
};
Board.prototype.insertMoveAfter=function(_267,_268,_269,_26a,_26b,_26c){
    addToMovelist=!_269;
    if(clog){
        console.log("addToMovelist:"+addToMovelist);
    }
    var _26d="null";
    if(_267){
        _26d=_267.output();
    }
    if(clog){
        console.log("insert newMove:"+_268.output()+" after:"+_26d);
    }
    if(_267==null){
        this.currentMove=_268;
        _268.atEnd=0;
        _268.prev=null;
        _268.next=null;
        this.firstMove=_268;
        if(this.startMoveNum>0){
            this.currentMove.moveNum=this.startMoveNum;
        }else{
            if(this.toMove==ChessPiece.WHITE){
                this.currentMove.moveNum=1;
            }else{
                this.currentMove.moveNum=2;
            }
        }
        if(clog){
            console.log("startMoveNum:"+this.startMoveNum+" currMoveNum:"+this.currentMove.moveNum);
        }
    }else{
        _268.atEnd=_267.atEnd;
        _268.prev=_267;
        _267.atEnd=0;
        if(clog){
            if(_267.next){
                console.log("prevMove.next:"+_267.next.output());
            }
        }
        if(_268.equals(_267.next)||_268.equals(_267)){
            if(clog){
                console.log("inserting move that already exists in variation:"+_267.next.output());
            }
            var _26e=_267.next;
            if(this.firstMove==_26e){
                this.firstMove=_268;
            }
            if(_268.equals(_267)){
                _26e=_267;
            }
            if(_26e.prev&&(_26e.prev.next==_26e)){
                _26e.prev.next=_268;
            }
            if(_26e.next){
                _26e.next.prev=_268;
            }
            addToMovelist=false;
            _268.moveNum=_26e.moveNum;
            _268.ravLevel=_26e.ravLevel;
            _268.index=_26e.index;
            _268.fen=_26e.fen;
            _268.nextFen=_26e.nextFen;
            _268.bestMoves=_26e.bestMoves;
            _268.correctMove=_26e.correctMove;
            _268.wrongMove=_26e.wrongMove;
            _268.next=_26e.next;
            _268.vars=_26e.vars;
            _268.numVars=_26e.numVars;
            this.reconnectNextNodeVariations(_268,_26e.next);
            this.moveArray[_268.index]=_268;
            if(this.currentMove==_26e){
                this.setCurrentMove(_268);
            }
        }else{
            _268.moveNum=_267.moveNum+1;
            _268.ravLevel=_267.ravLevel;
            _268.next=_267.next;
            if(_268.next){
                _268.next.prev=_268;
            }
        }
        _267.next=_268;
    }
    if(addToMovelist){
        this.insertIntoMoveDisplay(_267,_268,_26a,_26b,_26c);
    }
    if(_268.next==null){
        var _26f=this.createMoveFromString("i1i2");
        _268.next=_26f;
        _26f.prev=_268;
        _26f.moveNum=_268.moveNum+1;
        _26f.ravLevel=_268.ravLevel;
        _26f.next=null;
        _26f.atEnd=1;
        _26f.endNode=true;
        if(clog){
            console.log("created endmove node in insertAfterMove:"+_26f.output());
        }
    }else{
        if(clog){
            console.log("allready had a node at end:"+_268.next.output());
        }
        _268.next.moveNum=_268.moveNum+1;
    }
};
function insertBefore(node,_271){
    if(_271){
        _271.parentNode.insertBefore(node,_271);
    }
}
function insertAfter(node,_273){
    var _274=_273.parentNode;
    _274.insertBefore(node,_273.nextSibling);
}
Board.prototype.replaceIntoMoveDisplay=function(_275,_276,_277,_278,_279){
    var _27a="null";
    if(_275){
        _27a=_275.output();
    }
    if(clog){
        console.log("replace display newMove:"+_276.output()+" after:"+_27a+" hideScore:"+_278);
    }
    if(!_275){
        if(clog){
            console.log("null oldMove");
        }
        this.insertIntoMoveDisplay(null,_276,false,_278);
    }else{
        if(clog){
            console.log("about to get movesdsiplay in replace into move display:"+this.movesDisplay);
        }
        var _27b=this.movesDisplay.getMovesDisplay();
        if(clog){
            console.log("got moves display");
        }
        if(!_27b){
            if(clog){
                console.log("no movesd disiplay in replace into move display");
            }
            return;
        }
        var san=_276.SAN;
        if(!san){
            if(clog){
                console.log("about to make san");
            }
            san=this.makeShortAlgabraic(_276.fromColumn,_276.fromRow,_276.toColumn,_276.toRow,_276);
            if(clog){
                console.log("about to made san:"+san);
            }
            _276.SAN=san;
        }
        if(clog){
            console.log("oldMove.index:"+_275.index);
        }
        var _27d=this.boardName+"-ms"+_275.index;
        var _27e=-1;
        if(_275.next){
            _27e=this.boardName+"-m"+_275.next.index;
        }
        if(clog){
            console.log("oldMoveId:"+_27d);
        }
        var _27f=YAHOO.util.Dom.get(_27d);
        var _280=YAHOO.util.Dom.get(_27e);
        if(_277){
            this.moveIndex++;
            _276.index=this.moveIndex;
            this.moveArray[this.moveIndex]=_276;
            if(clog){
                console.log("replace as variation old:"+_275.output()+" new:"+_276.output());
            }
            var _281=document.createElement("span");
            if(typeof _275.ravlevel=="undefined"||_275.ravlevel==0){
                YAHOO.util.Dom.addClass(_281,"ct-top-var-start");
            }
            var _282=this.movesDisplay.outputVariationStart(0,0,_276.moveNum,0);
            _276.ravLevel=_275.ravlevel+1;
            var _27a=Board.moveToLocale(san);
            if(_276.prev==null){
                this.movesDisplay.firstNonMove=false;
            }
            var _283=this.movesDisplay.outputMove(this.moveIndex,_276.ravLevel,_276.moveNum,_27a,_277,0,_276.moveNum,_276,_278,_279);
            var _284=document.createElement("span");
            _284.id=(this.boardName+"-ms"+_276.index);
            _284.innerHTML=_283+"&nbsp;";
            var _285=this.movesDisplay.outputVariationEnd(0,0,_276.moveNum,0);
            this.movesDisplay.firstNonMove=true;
            var _286=document.createElement("span");
            _286.innerHTML=_282;
            var _287=document.createElement("span");
            _287.innerHTML=_285;
            _281.appendChild(_286);
            var els=YAHOO.util.Dom.getElementsByClassName("ct-mainline-commentary","div",_281);
            var _289=_281;
            if(els.length>0){
                _289=els[0];
            }
            _289.appendChild(_284);
            _289.appendChild(_287);
            _27f.appendChild(_281);
            if(_280){
                var els=YAHOO.util.Dom.getElementsByClassName("ct-board-move-movenum","span",_280);
                if(els.length==0){
                    var _28a=_275.next.moveNum;
                    var _28b=""+Math.round(_28a/2)+". ";
                    var _28c=false;
                    if(_28a%2!=1){
                        if(clog){
                            console.log("firstRav:"+firstRav+" firstNonMove:"+this.firstNonMove);
                        }
                        if(true||firstRav||!this.firstNonMove){
                            _28b=Math.round(_28a/2)+"... ";
                            _28c=true;
                        }else{
                            _28b="";
                        }
                    }
                    var _284=document.createElement("span");
                    _284.className="ct-board-move-movenum";
                    _284.innerHTML=_28b;
                    insertBefore(_284,_280.firstChild);
                    _284=document.createElement("span");
                    if(_28c){
                        _284.className="ct-board-move-dottedempty";
                        _284.innerHTML="&nbsp;";
                        insertAfter(_284,_280.firstChild);
                    }
                }
            }
        }else{
            _276.index=_275.index;
            this.moveArray[_276.index]=_276;
            var _27a=Board.moveToLocale(san);
            if(_276.prev==null){
                this.movesDisplay.firstNonMove=false;
            }
            var _283=this.movesDisplay.outputMove(_276.index,_276.ravLevel,_276.moveNum,_27a,_277,0,_276.moveNum,_276,_278,_279);
            var _284=document.createElement("span");
            _284.innerHTML=_283+"&nbsp;";
            _284.id=(this.boardName+"-ms"+_276.index);
            var _28d=[];
            if(_27f&&_27f.childNodes){
                for(var i=1;i<_27f.childNodes.length;i++){
                    _28d[i-1]=_27f.childNodes[i];
                }
            }
            if(clog){
                console.log("replace as main line not variation old:"+_275.output()+" new:"+_276.output());
            }
            _27f.parentNode.replaceChild(_284,_27f);
            if(_28d){
                for(var i=0;i<_28d.length;i++){
                    _284.appendChild(_28d[i]);
                }
            }
        }
        YAHOO.util.Event.removeListener(this.boardName+"-m"+_276.index);
        YAHOO.util.Event.addListener((this.boardName+"-m"+_276.index),"click",this.movesDisplay.gotoMove,this.movesDisplay,true);
    }
};
Board.prototype.insertCommentIntoMoveDisplay=function(move,_290,_291){
    var _292=this.movesDisplay.getMovesDisplay();
    if(!_292){
        return;
    }
    var _293="b";
    if(_291){
        _293="a";
    }
    if(move){
        var _294=this.boardName+"-mc"+_293+move.index;
        var _295=YAHOO.util.Dom.get(_294);
        var _296=false;
        if(!_295){
            _295=document.createElement("span");
            _295.id=_294;
            _296=true;
        }
        var _297=move.moveNum%2!=1;
        var _298=!_297&&!_291;
        if(clog){
            console.log("dontResetFirstNoneMove:"+_298+" isBlackMoveNum:"+_297+" insertCommentAfter:"+_291+" move.moveNum:"+move.moveNum+" comment:"+_290);
        }
        _295.innerHTML=this.movesDisplay.outputComment(_290,0,false,_298);
        var _299=YAHOO.util.Dom.get((this.boardName+"-m"+move.index));
        if(_299){
            if(_291){
                move.afterComment=_290;
                if(_296){
                    insertAfter(_295,_299);
                }
            }else{
                move.beforeComment=_290;
                if(_296){
                    insertBefore(_295,_299);
                }
            }
        }
        if(_295&&_296&&this.handleCommentClicks){
            YAHOO.util.Event.addListener(_295,"click",this.movesDisplay.clickComment,this.movesDisplay,true);
        }
    }else{
    }
};
Board.prototype.insertIntoMoveDisplay=function(_29a,_29b,_29c,_29d,_29e){
    var _29f=this.movesDisplay.getMovesDisplay();
    if(!_29f){
        return;
    }
    if(clog){
        var _2a0="null";
        if(_29a){
            _2a0=_29a.output();
        }
        console.log("insert display newMove:"+_29b.output()+" after:"+_2a0);
    }
    var san=_29b.SAN;
    if(!san){
        san=this.makeShortAlgabraic(_29b.fromColumn,_29b.fromRow,_29b.toColumn,_29b.toRow,_29b);
        _29b.SAN=san;
    }
    this.moveIndex++;
    _29b.index=this.moveIndex;
    this.moveArray[this.moveIndex]=_29b;
    var _2a0=Board.moveToLocale(san);
    var _2a2=false;
    var _2a3=null;
    if(_29a){
        _2a3=YAHOO.util.Dom.get((this.boardName+"-ms"+_29a.index));
    }
    if(_2a3){
        var els=YAHOO.util.Dom.getElementsByClassName("ct-mainline-commentary","div",_2a3);
        if(els.length>0){
            _2a2=true;
        }
    }
    var _2a5=this.movesDisplay.outputMove(this.moveIndex,_29b.ravLevel,_29b.moveNum,_2a0,_2a2,0,_29b.moveNum,_29b,_29d,_29e);
    var _2a6=document.createElement("span");
    _2a6.innerHTML=_2a5+"&nbsp;";
    _2a6.id=(this.boardName+"-ms"+this.moveIndex);
    if(_29c){
        YAHOO.util.Dom.setStyle(_2a6,"visibility","hidden");
    }
    if(_29a){
        if(clog){
            console.log("prevMove.index:"+_29a.index+"prevMove:"+_29a.output());
        }
        if(_2a3){
            insertAfter(_2a6,_2a3);
        }else{
            _29f.appendChild(_2a6);
        }
    }else{
        if(_29b.next){
            var _2a7=YAHOO.util.Dom.get((this.boardName+"-ms"+_29b.next.index));
            insertBefore(_2a6,_2a7);
        }else{
            _29f.appendChild(_2a6);
        }
    }
    YAHOO.util.Event.removeListener(this.boardName+"-m"+this.moveIndex);
    YAHOO.util.Event.addListener((this.boardName+"-m"+this.moveIndex),"click",this.movesDisplay.gotoMove,this.movesDisplay,true);
};
Board.prototype.replaceMove=function(_2a8,_2a9,_2aa,_2ab,_2ac,_2ad,_2ae){
    var _2af="null";
    if(_2a8){
        _2af=_2a8.output();
    }
    if(clog){
        console.log("replace newMove:"+_2a9.output()+" after:"+_2af+" replace as var"+_2aa+" rep move display:"+_2ab+" hideScore:"+_2ac+" replaceAsVariationEvenIfSame:"+_2ae);
        if(_2a8&&_2a8.prev){
            console.log("replace oldMove.prev:"+_2a8.prev.output());
        }
        if(_2a8&&_2a8.next){
            console.log("replace oldMove.next:"+_2a8.next.output());
        }
    }
    var _2b0=false;
    var _2b1=null;
    var _2b2=0;
    if(_2a8.endNode){
        if(clog){
            console.log("asked to replace endNode,inserting before instead");
        }
        this.insertMoveAfter(_2a8.prev,_2a9,false,false,_2ac,_2ad);
        _2a9.fen=_2a8.fen;
        _2a9.nextFen=_2a8.nextFen;
        return;
    }
    if(!_2ae&&_2a9.equals(_2a8)){
        if(clog){
            console.log("new move is same as old move so not replacing as variation");
        }
        _2aa=false;
    }else{
        if(!_2ae&&_2a8&&_2a8.numVars>0){
            for(var i=0;i<_2a8.numVars;i++){
                var _2b4=_2a8.vars[i];
                if(_2a9.equals(_2b4)){
                    if(clog){
                        console.log("new move is same as an existing variation varNum:"+i);
                        console.log("variation:"+_2b4.output());
                        if(_2b4.next){
                            console.log("variation next:"+_2b4.next.output());
                        }
                    }
                    _2b0=true;
                    _2b1=_2a8;
                    _2a8=_2b4;
                    _2b2=i;
                    break;
                }
            }
        }
    }
    if(_2a8==null){
        if(clog){
            console.log("replaced new move with null oldmove");
        }
        this.currentMove=_2a9;
        _2a9.atEnd=1;
        _2a9.next=null;
        _2a9.prev=null;
        if(this.startPositionAfterOpponentMove){
            _2a9.fen=this.startPositionAfterOpponentMove;
            _2a9.nextFen=null;
        }
        if(this.toMove==ChessPiece.WHITE){
            this.currentMove.moveNum=1;
        }else{
            this.currentMove.moveNum=2;
        }
        this.firstMove=_2a9;
    }else{
        var _2b5=false;
        if(_2a8&&_2a8.prev&&_2a8.prev.next!=_2a8){
            _2b5=true;
        }
        if(this.currentMove==_2a8&&!_2aa){
            this.currentMove=_2a9;
        }else{
            if(clog){
                console.log("not setting current move in replacemove");
            }
        }
        _2a9.atEnd=_2a8.atEnd;
        _2a9.prev=_2a8.prev;
        _2a9.next=_2a8.next;
        _2a9.fen=_2a8.fen;
        _2a9.nextFen=_2a8.nextFen;
        _2a9.bestMoves=_2a8.bestMoves;
        _2a9.correctMove=_2a8.correctMove;
        _2a9.wrongMove=_2a8.wrongMove;
        _2a9.moveNum=_2a8.moveNum;
        _2a9.ravLevel=_2a8.ravLevel;
        _2a9.index=_2a8.index;
        if(clog){
            console.log("replacingVariation with var not null:"+_2b0);
        }
        if(_2b0){
            _2b1.vars[_2b2]=_2a9;
            _2a9.vars=_2a8.vars;
            _2a9.numVars=_2a8.numVars;
            this.reconnectNextNodeVariations(_2a9,_2a8.next);
            if(_2a8.next){
                _2a8.next.prev=_2a9;
            }
            this.moveArray[_2a9.index]=_2a9;
            if(clog){
                console.log("replacing existing sub variation of main line");
                if(_2a9.next){
                    console.log("next of replacement variation:"+_2a9.next.output());
                }
            }
            return;
        }
        if(!_2aa){
            if(clog){
                console.log("not replacing as variation");
            }
            if(!_2b5&&_2a8.prev){
                _2a8.prev.next=_2a9;
            }
            if(_2a8.next){
                _2a8.next.prev=_2a9;
            }
            _2a9.vars=_2a8.vars;
            _2a9.numVars=_2a8.numVars;
            this.reconnectNextNodeVariations(_2a9,_2a8.next);
            if(this.firstMove==_2a8){
                this.firstMove=_2a9;
            }
            this.moveArray[_2a9.index]=_2a9;
        }else{
            if(clog){
                console.log("replacing as variation");
            }
            if(_2a8.numVars==0){
                _2a8.vars=new Array();
            }
            _2a8.vars[_2a8.numVars++]=_2a9;
            _2a8.atEnd=0;
            _2a9.next=null;
            var _2b6=this.createMoveFromString("i1i2");
            _2a9.next=_2b6;
            _2b6.prev=_2a9;
            _2b6.next=null;
            _2b6.atEnd=1;
            _2b6.moveNum=_2a9.moveNum+1;
            _2b6.ravLevel=_2a9.ravLevel;
            _2b6.endNode=true;
        }
    }
    if(_2ab){
        this.replaceIntoMoveDisplay(_2a8,_2a9,_2aa,_2ac,_2ad);
    }
};
Board.prototype.setCurrentMove=function(move,_2b8,_2b9){
    if(!this.cloned&&this.currentMove!=null){
        if(this.currentMove.prev!=null){
            YAHOO.util.Dom.removeClass(this.boardName+"-m"+this.currentMove.prev.index,"ct-board-move-current");
        }
    }
    this.currentMove=move;
    if(!this.cloned&&this.currentMove!=null&&this.currentMove.prev!=null){
        var _2ba=this.boardName+"-m"+this.currentMove.prev.index;
        if(clog){
            console.log("setCurrentMove attempted highlight of id:"+_2ba+" for move:"+move.output());
        }
        var span=YAHOO.util.Dom.get(_2ba);
        if(span){
            var cls=span.className;
            YAHOO.util.Dom.addClass(span,"ct-board-move-current");
            if(this.autoScrollMoves){
                if(!_2b9&&(this.scrollVariations||cls.indexOf("ct-board-move-variation")==-1)){
                    var _2bd=this.movesDisplay.getMovesDisplay();
                    if(_2bd){
                        var off=0;
                        if(_2bd&&_2bd.offsetHeight){
                            off=_2bd.offsetHeight/2;
                        }
                        var y=YAHOO.util.Dom.getY(span)-(YAHOO.util.Dom.getY(_2bd)+off);
                        var _2c0=new YAHOO.util.Scroll(_2bd,{scroll:{by:[0,y-this.scrollOffsetCorrection]}},this.moveAnimationLength,YAHOO.util.Easing.easeOut);
                        _2c0.animate();
                    }
                }
            }
        }
    }else{
        if(move==null){
            if(clog){
                console.log("attempted to set current move on null node");
            }
        }
    }
    if(!_2b8){
        this.setForwardBack();
    }
};
Board.prototype.distanceFromInitial=function(){
    var _2c1=this.cloneBoard();
    _2c1.setupFromFen(Board.INITIAL_FEN,false,false,true,false,false);
    var _2c2=0;
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var p1=this.boardPieces[i][j];
            var p2=_2c1.boardPieces[i][j];
            if(p1==p2){
                continue;
            }
            if(!p2){
                continue;
            }
            if(!p1){
                _2c2++;
                continue;
            }
            if(p1.piece==p2.piece&&p1.colour==p2.colour){
                continue;
            }
            _2c2++;
        }
    }
    return _2c2;
};
Board.INITIAL_FEN="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
Board.isFenLegal=function(fen){
    function isLegalCastling(c){
        if(!c){
            return false;
        }
        c=c.toLowerCase();
        for(var i=0;i<c.length;i++){
            if(!(c.charAt(i)=="q"||c.charAt(i)=="k"||c.charAt(i)=="-")){
                return false;
            }
        }
        return true;
    }
    function isLegalEnpassant(m){
        if(!m){
            return false;
        }
        if(m=="-"){
            return true;
        }
        if(m.length!=2){
            return false;
        }
        if(m.charAt(0)<"a"||m.charAt(0)>"h"){
            return false;
        }
        var n=parseInt(m.charAt(1));
        if(isNaN(n)||n<1||n>8){
            return false;
        }
        return true;
    }
    function isRowLegal(r){
        if(!r){
            return false;
        }
        r=r.toLowerCase();
        for(var i=0;i<r.length;i++){
            if(!isSquareCharLegal(r.charAt(i))){
                return false;
            }
        }
        return true;
    }
    function isSquareCharLegal(c){
        switch(c){
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "p":
            case "k":
            case "b":
            case "q":
            case "r":
            case "k":
                return true;
            default:
                false;
        }
    }
    if(!fen){
        return false;
    }
    var ss=fen.split(" ");
    if(ss.length!=6){
        return false;
    }
    var pos=ss[0].split("/");
    if(pos.length!=8){
        return false;
    }
    if(ss[1]!="w"&&ss[1]!="b"){
        return false;
    }
    if(isNaN(parseInt(ss[4]))){
        return false;
    }
    if(isNaN(parseInt(ss[5]))){
        return false;
    }
    if(!isLegalCastling(ss[2])){
        return false;
    }
    if(!isLegalEnpassant(ss[3])){
        return false;
    }
    return true;
};
Board.prototype.boardToUniqueFen=function(_2d1){
    var fen=this.boardToFen();
    var ss=fen.split(" ");
    var _2d4="w";
    if(_2d1==ChessPiece.BLACK){
        _2d4="b";
    }
    var _2d5=ss[0]+" "+_2d4+" "+ss[2]+" "+ss[3];
    return _2d5;
};
Board.prototype.boardToFen=function(_2d6){
    var _2d7="";
    for(var row=7;row>=0;row--){
        var _2d9=0;
        var line="";
        if(row<7){
            line="/";
        }
        for(var col=0;col<8;col++){
            var _2dc=this.boardPieces[col][row];
            if(_2dc){
                var _2dd="";
                if(_2d9>0){
                    _2dd=_2d9+"";
                }
                line+=_2dd+_2dc.getFenLetter();
                _2d9=0;
            }else{
                _2d9++;
            }
        }
        if(_2d9>0){
            line+=_2d9+"";
        }
        _2d7+=line;
    }
    var fen=_2d7;
    var _2df=" w ";
    if(_2d6){
        if(this.toMove==ChessPiece.WHITE){
            _2df=" b ";
        }
    }else{
        if(this.toMove==ChessPiece.BLACK){
            _2df=" b ";
        }
    }
    fen+=_2df;
    var _2e0="";
    _2e0+=Board.getFenCastleChar(this.canCastleKingSide,"K",ChessPiece.WHITE);
    _2e0+=Board.getFenCastleChar(this.canCastleQueenSide,"Q",ChessPiece.WHITE);
    _2e0+=Board.getFenCastleChar(this.canCastleKingSide,"K",ChessPiece.BLACK);
    _2e0+=Board.getFenCastleChar(this.canCastleQueenSide,"Q",ChessPiece.BLACK);
    if(_2e0==""){
        fen+="- ";
    }else{
        fen+=_2e0+" ";
    }
    var _2e1=null;
    if(this.currentMove){
        if(this.currentMove.prev){
            _2e1=this.currentMove.prev;
        }else{
            _2e1=this.prev_move;
        }
    }else{
        _2e1=this.prev_move;
    }
    var _2e2="- ";
    if(_2e1){
        var _2e3=this.boardPieces[_2e1.toColumn][_2e1.toRow];
        if(_2e3){
            if(_2e3.piece==ChessPiece.PAWN){
                if(_2e3.colour==ChessPiece.WHITE){
                    if(_2e1.fromRow==1&&_2e1.toRow==3){
                        _2e2=Move.columnToChar(_2e1.fromColumn)+"3 ";
                    }
                }else{
                    if(_2e1.fromRow==6&&_2e1.toRow==4){
                        _2e2=Move.columnToChar(_2e1.fromColumn)+"6 ";
                    }
                }
            }
        }
    }
    fen+=_2e2;
    fen+=this.halfMoveNumber+" "+parseInt((this.moveNumber+1)/2);
    if(clog){
        console.log("moveNumber:"+this.moveNumber+" fen:"+fen);
    }
    return fen;
};
Board.getFenCastleChar=function(_2e4,_2e5,_2e6){
    if(_2e4[_2e6]){
        if(_2e6==ChessPiece.WHITE){
            return _2e5.toUpperCase();
        }else{
            return _2e5.toLowerCase();
        }
    }
    return "";
};
Board.prototype.getCastlingString=function(_2e7){
    var _2e8=_js("None");
    if(this.canCastleKingSide[_2e7]){
        _2e8="O-O";
    }
    if(this.canCastleQueenSide[_2e7]){
        if(_2e8==_js("None")){
            _2e8="O-O-O";
        }else{
            _2e8+=",O-O-O";
        }
    }
    return _2e8;
};
Board.prototype.updateToPlay=function(){
    if(this.disableUpdateToPlay){
        return;
    }
    if(this.showToMoveIndicators){
        if(this.isFlipped){
            YAHOO.util.Dom.setStyle(this.boardName+"-top-to-move-inner","background-color","white");
            YAHOO.util.Dom.setStyle(this.boardName+"-top-to-move-inner","border","1px solid black");
            YAHOO.util.Dom.setStyle(this.boardName+"-bottom-to-move-inner","background-color","black");
            YAHOO.util.Dom.setStyle(this.boardName+"-bottom-to-move-inner","border","1px solid white");
        }else{
            YAHOO.util.Dom.setStyle(this.boardName+"-bottom-to-move-inner","background-color","white");
            YAHOO.util.Dom.setStyle(this.boardName+"-bottom-to-move-inner","border","1px solid black");
            YAHOO.util.Dom.setStyle(this.boardName+"-top-to-move-inner","background-color","black");
            YAHOO.util.Dom.setStyle(this.boardName+"-top-to-move-inner","border","1px solid white");
        }
        if(this.toMove==ChessPiece.WHITE){
            if(this.isFlipped){
                YAHOO.util.Dom.addClass(this.boardName+"-top-to-move-outer","ct-to-move-active");
                YAHOO.util.Dom.removeClass(this.boardName+"-bottom-to-move-outer","ct-to-move-active");
            }else{
                YAHOO.util.Dom.addClass(this.boardName+"-bottom-to-move-outer","ct-to-move-active");
                YAHOO.util.Dom.removeClass(this.boardName+"-top-to-move-outer","ct-to-move-active");
            }
        }else{
            if(this.isFlipped){
                YAHOO.util.Dom.addClass(this.boardName+"-bottom-to-move-outer","ct-to-move-active");
                YAHOO.util.Dom.removeClass(this.boardName+"-top-to-move-outer","ct-to-move-active");
            }else{
                YAHOO.util.Dom.addClass(this.boardName+"-top-to-move-outer","ct-to-move-active");
                YAHOO.util.Dom.removeClass(this.boardName+"-bottom-to-move-outer","ct-to-move-active");
            }
        }
    }
    var _2e9=YAHOO.util.Dom.get("toPlay");
    if(_2e9==null){
        return;
    }
    if(this.toMove==ChessPiece.WHITE){
        _2e9.src="/static/images/whiteknight"+this.getVersString()+".gif";
        _2e9.alt=_js("White to play");
    }else{
        _2e9.src="/static/images/blackknight"+this.getVersString()+".gif";
        _2e9.alt=_js("Black to play");
    }
    var _2ea=YAHOO.util.Dom.get("fenStatus");
    if(_2ea){
        var _2eb=this.getCastlingString(ChessPiece.BLACK);
        var _2ec=this.getCastlingString(ChessPiece.WHITE);
        var s="<div><span>"+_js("White Castling: ")+"</span><span>"+_2ec+"</span></div>"+"<div><span>"+_js("Black Castling: ")+"</span><span>"+_2eb+"</span></div>";
        _2ea.innerHTML=s;
    }
};
Board.prototype.getBoardDivFromId=function(id){
    if(!this[id]){
        this[id]=YAHOO.util.Dom.get(id);
    }
    return this[id];
};
Board.prototype.getBoardDiv=function(){
    if(!this.boardDiv){
        this.boardDiv=YAHOO.util.Dom.get("ctb-"+this.boardName);
    }
    return this.boardDiv;
};
Board.prototype.getDocBody=function(){
    if(!this.docBody){
        var _2ef=document.getElementsByTagName("body");
        if(_2ef==null||_2ef.length==0){
            alert("Could not find body tag");
        }else{
            this.docBody=_2ef[0];
        }
    }
    return this.docBody;
};
Board.prototype.getPieceDragDiv=function(){
    if(!this.pieceDragDiv){
        this.pieceDragDiv=YAHOO.util.Dom.get("pieceDragDiv");
    }
    return this.pieceDragDiv;
};
Board.prototype.createBoardCoords=function(){
    this.coordinatesShown=false;
    var _2f0=YAHOO.util.Dom.get(this.boardName+"-fileLabels");
    var _2f1=YAHOO.util.Dom.get(this.boardName+"-rankLabels");
    if(!_2f0||!_2f1){
        return;
    }
    YAHOO.util.Event.purgeElement(_2f0,true);
    _2f1.innerHTML="";
    _2f0.innerHTML="";
    var _2f2=YAHOO.util.Dom.get(this.boardName+"-boardBorder");
    if(!this.showCoordinates){
        YAHOO.util.Dom.setStyle(_2f0,"display","none");
        YAHOO.util.Dom.setStyle(_2f1,"display","none");
        var _2f3=0;
        YAHOO.util.Dom.setStyle(_2f2,"width",(this.pieceSize*8+_2f3)+"px");
        YAHOO.util.Dom.setStyle(_2f2,"height",(this.pieceSize*8+_2f3)+"px");
        return;
    }
    YAHOO.util.Dom.setStyle(_2f0,"display","block");
    YAHOO.util.Dom.setStyle(_2f1,"display","block");
    var _2f3=15;
    var _2f4=0;
    if(check_bad_msie()){
        _2f4=this.ie6FixCoordsOffsetSize;
    }
    if(YAHOO.util.Event.isIE){
        _2f4+=this.allIeFixCoordsOffsetSize;
        if(document.compatMode!="CSS1Compat"){
            _2f4=8;
        }
    }
    YAHOO.util.Dom.setStyle(_2f2,"width",(this.pieceSize*8+_2f3+_2f4)+"px");
    YAHOO.util.Dom.setStyle(_2f2,"height",(this.pieceSize*8+_2f3)+"px");
    this.coordinatesShown=true;
    for(var i=0;i<8;i++){
        var _2f6=document.createElement("div");
        YAHOO.util.Dom.setStyle(_2f6,"height",this.pieceSize+"px");
        YAHOO.util.Dom.setStyle(_2f6,"width","15px");
        YAHOO.util.Dom.setStyle(_2f6,"text-align","center");
        YAHOO.util.Dom.setStyle(_2f6,"line-height",this.pieceSize+"px");
        if(this.isFlipped){
            _2f6.innerHTML=""+(i+1);
        }else{
            _2f6.innerHTML=""+9-(i+1);
        }
        _2f1.appendChild(_2f6);
    }
    for(var i=0;i<9;i++){
        var _2f7=document.createElement("span");
        YAHOO.util.Dom.setStyle(_2f7,"float","left");
        YAHOO.util.Dom.setStyle(_2f7,"height","15px");
        if(i==0){
            YAHOO.util.Dom.setStyle(_2f7,"width","15px");
            YAHOO.util.Dom.setStyle(_2f7,"clear","both");
            YAHOO.util.Dom.setStyle(_2f7,"margin-top","-5px");
            if(_2f4){
                YAHOO.util.Dom.setStyle(_2f7,"margin-left","-3px");
            }else{
                YAHOO.util.Dom.setStyle(_2f7,"margin-left","-2px");
            }
            var _2f8="";
            if(this.isFlipped){
                _2f8="whiteblack-flipper"+this.getVersString()+".png";
            }else{
                _2f8="blackwhite-flipper"+this.getVersString()+".png";
            }
            _2f7.innerHTML="<span><img id=\""+this.boardName+"-flipper\" title=\""+_js("Flip Board")+"\" src=\""+this.boardImagePath+"/static/images/"+_2f8+"\"/></span>";
            if(!this.disableFlipper){
                YAHOO.util.Event.addListener(this.boardName+"-flipper","click",this.flipBoard,this,true);
            }
        }else{
            YAHOO.util.Dom.setStyle(_2f7,"width",this.pieceSize+"px");
            YAHOO.util.Dom.setStyle(_2f7,"text-align","center");
            if(this.isFlipped){
                _2f7.innerHTML=_js(Move.columnToChar(8-(i)));
            }else{
                _2f7.innerHTML=_js(Move.columnToChar((i-1)));
            }
        }
        _2f0.appendChild(_2f7);
    }
    var _2f9=YAHOO.util.Dom.get(this.boardName+"-flipper");
    if(_2f9){
        fix_ie_png(_2f9);
    }
};
Board.prototype.showNavigation=function(){
    this.disableNavigation=false;
    YAHOO.util.Dom.setStyle(this.boardName+"-ct-nav-container","display","block");
};
Board.prototype.hideNavigation=function(){
    this.disableNavigation=true;
    YAHOO.util.Dom.setStyle(this.boardName+"-ct-nav-container","display","none");
};
Board.prototype.createBoardUI=function(){
    var _2fa=this.boardName+"-container";
    var _2fb=YAHOO.util.Dom.get(_2fa);
    if(_2fb==null){
        alert("Could not find board container:"+_2fa);
        return;
    }
    YAHOO.util.Dom.addClass(_2fb,"ct-board-container");
    this.boardDiv=null;
    var _2fc=document.createElement("div");
    _2fc.id=this.boardName+"-boardBorder";
    YAHOO.util.Dom.addClass(_2fc,"ct-board-border"+this.squareColorClass);
    var _2fd=0;
    if(this.showCoordinates){
        _2fd=15;
    }
    YAHOO.util.Dom.setStyle(_2fc,"width",(this.pieceSize*8+_2fd)+"px");
    YAHOO.util.Dom.setStyle(_2fc,"height",(this.pieceSize*8+_2fd)+"px");
    var _2fe=document.createElement("div");
    YAHOO.util.Dom.setStyle(_2fe,"float","left");
    _2fe.id=this.boardName+"-rankLabels";
    _2fc.appendChild(_2fe);
    var _2ff=document.createElement("div");
    YAHOO.util.Dom.addClass(_2ff,"ct-board");
    YAHOO.util.Dom.setStyle(_2ff,"width",(this.pieceSize*8)+"px");
    YAHOO.util.Dom.setStyle(_2ff,"height",(this.pieceSize*8)+"px");
    _2ff.id="ctb-"+this.boardName;
    var _300="ct-white-square"+this.squareColorClass;
    var _301="";
    var _302=[];
    for(var i=7;i>=0;i--){
        var s="<div>";
        for(var j=0;j<8;j++){
            var _306=document.createElement("div");
            var _307=this.boardName+"-s"+j+""+i;
            var _308=(((j+1)*(i+1))%19/19*100);
            var _309=((65-((j+1)*(i+1)))%19/19*100);
            s+="<div id=\""+_307+"\" class=\""+_300+"\" style=\"width:"+this.pieceSize+"px;height:"+this.pieceSize+"px;background-position:"+_308+"% "+_309+"%\"></div>";
            _302.push(_307);
            _300=(_300=="ct-black-square"+this.squareColorClass)?"ct-white-square"+this.squareColorClass:"ct-black-square"+this.squareColorClass;
        }
        _300=(_300=="ct-black-square"+this.squareColorClass)?"ct-white-square"+this.squareColorClass:"ct-black-square"+this.squareColorClass;
        s+="</div>";
        _301+=s;
    }
    _2ff.innerHTML=_301;
    var _30a=document.createElement("div");
    _30a.id=this.boardName+"-fileLabels";
    _2fc.appendChild(_2ff);
    _2fc.appendChild(_30a);
    _2fb.appendChild(_2fc);
    if(this.showToMoveIndicators){
        var _30b=document.createElement("div");
        _30b.id=this.boardName+"-moveIndicators";
        YAHOO.util.Dom.addClass(_30b,"ct-move-indicators");
        _30b.innerHTML="<div class=\"ct-top-to-move-outer\" id=\""+this.boardName+"-top-to-move-outer\"><div  class=\"ct-top-to-move-inner\" id=\""+this.boardName+"-top-to-move-inner\"></div></div><div class=\"ct-bottom-to-move-outer\"  id=\""+this.boardName+"-bottom-to-move-outer\"><div class=\"ct-bottom-to-move-inner\" id=\""+this.boardName+"-bottom-to-move-inner\" ></div>";
        _2fb.appendChild(_30b);
        YAHOO.util.Dom.setStyle(_2fc,"float","left");
        YAHOO.util.Dom.setStyle(_30b,"float","left");
        YAHOO.util.Dom.setStyle(_30b,"margin-left","2px");
        YAHOO.util.Dom.setStyle(_30b,"height",((this.pieceSize*8)+2)+"px");
        YAHOO.util.Dom.setStyle(_30b,"position","relative");
        var _30c=document.createElement("div");
        YAHOO.util.Dom.setStyle(_30c,"clear","both");
        _2fb.appendChild(_30c);
    }
    this.createBoardCoords();
    var _30d=false;
    var _30e=YAHOO.util.Dom.get(this.boardName+"-ct-nav-container");
    if(!_30e){
        _30e=document.createElement("div");
    }else{
        _30d=true;
        _30e.innerHTML="";
    }
    _30e.id=this.boardName+"-ct-nav-container";
    if(!this.dontOutputNavButtons||this.r){
        var _30f="";
        if(!this.dontOutputNavButtons){
            if(!this.problem||!this.problem.isEndgame){
                _30f="<span id=\"playStopSpan\"><img class=\"ct-end\" id=\""+this.boardName+"-end\" src=\""+this.boardImagePath+"/static/images/resultset_last"+this.getVersString()+".gif\" alt=\""+_js("End position")+"\" title=\""+_js("Go to final position")+"\"/>"+"<img class=\"ct-play\" id=\""+this.boardName+"-play\" src=\""+this.boardImagePath+"/static/images/control_play_blue"+this.getVersString()+".gif\" alt=\""+_js("Play moves")+"\" title=\""+_js("Play sequence of moves")+"\"/>"+"<img class=\"ct-stop\" id=\""+this.boardName+"-stop\" src=\""+this.boardImagePath+"/static/images/control_stop_blue"+this.getVersString()+".gif\" alt=\""+_js("Stop playing")+"\" title=\""+_js("Stop playing move sequence")+"\"/></span>";
            }
        }
        var _310="<div class=\"ct-nav-buttons\" id=\""+this.boardName+"-navButtons\"><span id=\""+this.boardName+"-nav-buttons-only\">";
        if(!this.dontOutputNavButtons){
            var size="";
            if(isIphone||isIpad){
                size=" width=\"50px\" height=\"34px\" ";
                _30f="";
            }
            if(!(isIphone||isIpad)){
                _310+="<img class=\"ct-start\" id=\""+this.boardName+"-start\" src=\""+this.boardImagePath+"/static/images/resultset_first"+this.getVersString()+".gif\" alt=\""+_js("Start position")+"\" title=\""+_js("Go to starting position")+"\"/>";
            }
            _310+="<img class=\"ct-back\" id=\""+this.boardName+"-back\" "+size+" src=\""+this.boardImagePath+"/static/images/resultset_previous"+this.getVersString()+".gif\" alt=\""+_js("Previous Move")+"\" title=\""+_js("Go back a move")+"\"/>"+"<img class=\"ct-forward\" id=\""+this.boardName+"-forward\" "+size+" src=\""+this.boardImagePath+"/static/images/resultset_next"+this.getVersString()+".gif\" alt=\""+_js("Next Move")+"\" title=\""+_js("Go forward a move")+"\"/>"+_30f;
        }
        if(this.r){
            _310+="<img class=\"ct-forward\" id=\""+this.boardName+"-analyse\" src=\""+this.boardImagePath+"/static/images/anboard"+this.getVersString()+".gif\" alt=\""+_js("Analyse")+"\" title=\""+_js("Launch analysis board to explore different lines in this position")+"\"/>";
            if(!this.g){
                _310+="<img class=\"ct-forward\" id=\""+this.boardName+"-showfen\" src=\""+this.boardImagePath+"/static/images/copy_fen"+this.getVersString()+".gif\" alt=\""+_js("Copy FEN")+"\" title=\""+_js("Show FEN for current position")+"\"/>";
            }
        }
        if(this.canPasteFen){
            _310+="<img class=\"ct-forward\" id=\""+this.boardName+"-pastefen\" src=\""+this.boardImagePath+"/static/images/paste_fen"+this.getVersString()+".gif\" alt=\""+_js("Input FEN")+"\" title=\""+_js("Setup position from user supplied FEN or move list")+"\"/>";
        }
        if(this.g2){
            _310+="<img class=\"ct-forward\" id=\""+this.boardName+"-playcomp\" src=\""+this.boardImagePath+"/static/images/computer"+this.getVersString()+".gif\" alt=\""+_js("Play Current Position vs Computer")+"\" title=\""+_js("Play current position against computer")+"\"/>";
        }
        _310+="</span>";
        _310+="</div>";
        if(this.puzzle){
            var _312="";
            var _313="";
            var _314="";
            var _315="";
            if(this.pieceSize>=29){
                _312=_js("Easy");
                _313=_js("Medium");
                _314=_js("Hard");
                _315=_js("Help");
            }else{
                _312=_js("D1");
                _313=_js("D2");
                _314=_js("D3");
                _315=_js("?");
            }
            _310+="<div><form action=\"\"><button type=\"button\" id=\""+this.boardName+"-puzzleSolution\" class=\"asolution-button\">"+_js("Show")+"</button><button id=\""+this.boardName+"-easyPuzzle\" type=\"button\" class=\"puzzle-difficulty\">"+_312+"</button>"+"<button id=\""+this.boardName+"-mediumPuzzle\" type=\"button\" class=\"puzzle-difficulty\">"+_313+"</button>"+"<button id=\""+this.boardName+"-hardPuzzle\" type=\"button\" class=\"puzzle-difficulty\">"+_314+"</button>"+"<button id=\""+this.boardName+"-puzzleHelp\" type=\"button\" class=\"puzzle-difficulty\">"+_315+"</button>"+"<img alt=\"\" class=\"ct-forward\" id=\""+this.boardName+"-problemState\"></img><span id=\""+this.boardName+"-puzzleResult\"></span></form></div>";
            _310+="<div class=\"initially_hidden initially_invisible\" id=\""+this.boardName+"-moves\"></div>";
            _310+="<div class=\"initially_hidden initially_invisible\" id=\""+this.boardName+"-moves\"></div>";
        }
        _30e.innerHTML=_310;
    }
    if(!_30d){
        _2fb.appendChild(_30e);
    }
    if(this.problem){
        var body=YAHOO.util.Dom.get("body");
        if(body){
            YAHOO.util.Dom.setStyle(body,"min-width",((this.pieceSize*8+_2fd)+300+200+120)+"px");
        }
    }
};
Board.prototype.getPieceDiv=function(){
    var _317=this.getBoardDiv();
    var _318=document.createElement("div");
    this.availPieceDivs[this.uptoId]=_318;
    this.availIds[this.uptoId]=YAHOO.util.Dom.generateId(_318);
    YAHOO.util.Dom.setStyle(_318,"visibility","hidden");
    YAHOO.util.Dom.addClass(_318,"board-piece-start-style");
    _317.appendChild(_318);
    this.uptoId++;
    return _318;
};
Board.prototype.flipToMove=function(_319){
    return (_319=="w")?"b":"w";
};
Board.prototype.pieceCharToPieceNum=function(_31a){
    var _31b;
    switch(_31a){
        case "K":
            _31b=ChessPiece.KING;
            break;
        case "Q":
            _31b=ChessPiece.QUEEN;
            break;
        case "R":
            _31b=ChessPiece.ROOK;
            break;
        case "B":
            _31b=ChessPiece.BISHOP;
            break;
        case "N":
            _31b=ChessPiece.KNIGHT;
            break;
        case "P":
            _31b=ChessPiece.PAWN;
            break;
    }
    return _31b;
};
Board.prototype.pieceTypeToChar=function(_31c){
    switch(_31c){
        case ChessPiece.KING:
            return "K";
        case ChessPiece.QUEEN:
            return "Q";
        case ChessPiece.ROOK:
            return "R";
        case ChessPiece.BISHOP:
            return "B";
        case ChessPiece.KNIGHT:
            return "N";
        case ChessPiece.PAWN:
            return "P";
    }
    return "?";
};
Board.prototype.canMoveKnight=function(_31d,_31e,_31f,_320){
    if(_31d+2==_31f&&_31e+1==_320){
        return true;
    }
    if(_31d+2==_31f&&_31e-1==_320){
        return true;
    }
    if(_31d-2==_31f&&_31e+1==_320){
        return true;
    }
    if(_31d-2==_31f&&_31e-1==_320){
        return true;
    }
    if(_31d+1==_31f&&_31e+2==_320){
        return true;
    }
    if(_31d-1==_31f&&_31e+2==_320){
        return true;
    }
    if(_31d+1==_31f&&_31e-2==_320){
        return true;
    }
    if(_31d-1==_31f&&_31e-2==_320){
        return true;
    }
    return false;
};
Board.prototype.canMovePawn=function(_321,_322,_323,_324,_325){
    var _326=this.boardPieces[_323][_324];
    var _327=this.boardPieces[_321][_322];
    if(_325){
        var _328=this.boardPieces[_325.toColumn][_325.toRow];
        if(_328&&_328.piece==ChessPiece.PAWN){
            if(_328.colour==ChessPiece.WHITE){
                if(_325.fromRow==1&&_325.toRow==3){
                    if(_323==_325.fromColumn&&_322==3&&_324==2&&(_321==_323+1||_321==_323-1)){
                        return true;
                    }
                }
            }else{
                if(_325.fromRow==6&&_325.toRow==4){
                    if(_323==_325.fromColumn&&_322==4&&_324==5&&(_321==_323+1||_321==_323-1)){
                        return true;
                    }
                }
            }
        }
    }
    if(_326){
        if(_327.colour==ChessPiece.WHITE){
            if((_321==_323+1||_321==_323-1)&&(_322==_324-1)){
                return true;
            }
        }else{
            if((_321==_323+1||_321==_323-1)&&(_322==_324+1)){
                return true;
            }
        }
    }else{
        if(_321==_323){
            if(_327.colour==ChessPiece.WHITE){
                if(_322==1){
                    if(_324==2){
                        return true;
                    }else{
                        if(_324==3&&this.boardPieces[_323][2]==null){
                            return true;
                        }
                    }
                }else{
                    if(_322+1==_324){
                        return true;
                    }
                }
            }else{
                if(_322==6){
                    if(_324==5){
                        return true;
                    }else{
                        if(_324==4&&this.boardPieces[_323][5]==null){
                            return true;
                        }
                    }
                }else{
                    if(_322-1==_324){
                        return true;
                    }
                }
            }
        }
    }
    return false;
};
Board.prototype.canMoveStraight=function(_329,_32a,_32b,_32c,_32d,_32e){
    var _32f=_329;
    var _330=_32a;
    var _331=0;
    var _332=0;
    if(_32b>_329){
        _331=1;
    }else{
        if(_32b<_329){
            _331=-1;
        }
    }
    if(_32c>_32a){
        _332=1;
    }else{
        if(_32c<_32a){
            _332=-1;
        }
    }
    if(clog){
        console.log("deltaRow:"+_332+" deltaCol:"+_331+" fromCol:"+_329+" fromRow:"+_32a+" toCol:"+_32b+" toRow:"+_32c);
    }
    if(_32d==ChessPiece.ROOK&&(_331!=0&&_332!=0)){
        return false;
    }
    if(_32d==ChessPiece.BISHOP&&(_331==0||_332==0)){
        return false;
    }
    var _333=0;
    while(true){
        _333++;
        _329+=_331;
        _32a+=_332;
        if(_32d==ChessPiece.KING&&_333>1){
            if(clog){
                console.log("king count:"+_333+" toCol:"+_32b+" toRow:"+_32c);
            }
            if(_333!=2){
                return false;
            }
            if(_332!=0){
                return false;
            }
            if(!(_32b==6||_32b==2)){
                return false;
            }
            if(_32b==2){
                if(this.boardPieces[1][_32a]||this.boardPieces[2][_32a]||this.boardPieces[3][_32a]){
                    return false;
                }
                if(!this.canCastleQueenSide[_32e.colour]){
                    return false;
                }
            }else{
                if(_32b==6){
                    if(this.boardPieces[5][_32a]||this.boardPieces[6][_32a]){
                        if(clog){
                            console.log("king can't castle intervening piece");
                        }
                        return false;
                    }
                    if(!this.canCastleKingSide[_32e.colour]){
                        if(clog){
                            console.log("king can't castle king side (made previously invalid) colour:"+_32e.colour);
                        }
                        return false;
                    }
                }else{
                    if(clog){
                        console.log("king not in col 2 or 6");
                    }
                    return false;
                }
            }
            var _334="";
            _334+=Move.columnToChar(_32f);
            _334+=String.fromCharCode("1".charCodeAt(0)+_330);
            _334+=Move.columnToChar((_32f+_331));
            _334+=String.fromCharCode("1".charCodeAt(0)+(_330+_332));
            var move=this.createMoveFromString(_334);
            var _336=this.cloneBoard();
            _336.makeMove(move,_336.boardPieces[_32f][_330],false,this.moveAnimationLength,false,false);
            kingSafe=_336.isKingSafe(_32e.colour,move);
            if(clog){
                console.log("kingSafe1:"+kingSafe);
            }
            if(!kingSafe){
                return false;
            }
            var _334="";
            _334+=Move.columnToChar(_32f);
            _334+=String.fromCharCode("1".charCodeAt(0)+_330);
            _334+=Move.columnToChar(_32f);
            _334+=String.fromCharCode("1".charCodeAt(0)+_330);
            var move=this.createMoveFromString(_334);
            var _336=this.cloneBoard();
            _336.makeMove(move,_336.boardPieces[_32f][_330],false,this.moveAnimationLength,false,false);
            kingSafe=_336.isKingSafe(_32e.colour,move);
            var _336=this.cloneBoard();
            _336.makeMove(move,_336.boardPieces[_32f][_330],false,this.moveAnimationLength,false,false);
            kingSafe=this.isKingSafe(_32e.colour,move);
            if(clog){
                console.log("kingSafe2:"+kingSafe);
            }
            if(!kingSafe){
                return false;
            }
        }
        if(_329==_32b&&_32a==_32c){
            return true;
        }
        if(_329<0||_329>7||_32a<0||_32a>7){
            return false;
        }
        if(this.boardPieces[_329][_32a]!=null){
            return false;
        }
    }
};
Board.prototype.canMove=function(_337,_338,_339,_33a,_33b){
    var _33c=_337.column;
    var _33d=_337.row;
    if(_338>7||_338<0||_339>7||_339<0){
        if(clog){
            console.log("can't move coz out of bounds");
        }
        return false;
    }
    var _33e=this.boardPieces[_338][_339];
    var _33f=this.boardPieces[_33c][_33d];
    if(_33f==null){
        return false;
    }
    if(_33e&&_33e.colour==_33f.colour){
        return false;
    }
    var _340=false;
    if(_337.piece==ChessPiece.PAWN){
        _340=this.canMovePawn(_33c,_33d,_338,_339,_33a);
    }else{
        if(_337.piece==ChessPiece.KNIGHT){
            _340=this.canMoveKnight(_33c,_33d,_338,_339);
        }else{
            _340=this.canMoveStraight(_33c,_33d,_338,_339,_337.piece,_337);
        }
    }
    if(clog){
        console.log("moveOk:"+_340);
    }
    var _341=true;
    if(_340&&_33b){
        var _342="";
        _342+=Move.columnToChar(_33c);
        _342+=String.fromCharCode("1".charCodeAt(0)+_33d);
        _342+=Move.columnToChar(_338);
        _342+=String.fromCharCode("1".charCodeAt(0)+_339);
        var move=this.createMoveFromString(_342);
        var _344=this.cloneBoard();
        _344.makeMove(move,_344.boardPieces[_33c][_33d],false,this.moveAnimationLength,false,false);
        _341=_344.isKingSafe(_337.colour,move);
    }
    return _340&&_341;
};
Board.prototype.is50MoveRule=function(){
    return (this.halfMoveNumber>=100);
};
Board.prototype.isCheckmate=function(_345){
    return (!this.isKingSafe(this.toMove,_345)&&this.isKingMated(this.toMove,_345));
};
Board.prototype.isStalemate=function(_346){
    return (this.isKingSafe(this.toMove,_346)&&(this.getCandidateMoves(this.toMove,_346,true)).length==0);
};
Board.prototype.isKingMated=function(_347,_348){
    var _349=null;
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var bp=this.boardPieces[i][j];
            if(bp!=null&&bp.piece==ChessPiece.KING&&bp.colour==_347){
                _349=bp;
                break;
            }
        }
    }
    var _34d=[[1,0],[1,1],[1,-1],[-1,0],[-1,1],[-1,-1],[0,1],[0,-1],[2,0],[-2,0]];
    var bp=_349;
    for(var k=0;k<_34d.length;k++){
        if(this.canMove(bp,bp.column+_34d[k][0],bp.row+_34d[k][1],_348,true)){
            return false;
        }
    }
    var _34f=this.getCandidateMoves(_347,_348,true,true);
    if(_34f.length>0){
        return false;
    }
    return true;
};
Board.prototype.getCandidateMoves=function(_350,_351,_352,_353){
    var _354=new Array();
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var bp=this.boardPieces[i][j];
            var _358=[];
            if(!bp||bp.colour!=_350){
                continue;
            }
            switch(bp.piece){
                case ChessPiece.KING:
                    if(_353){
                        continue;
                    }
                    _358=[[1,0],[1,1],[1,-1],[-1,0],[-1,1],[-1,-1],[0,1],[0,-1],[2,0],[-2,0]];
                    break;
                case ChessPiece.KNIGHT:
                    _358=[[2,1],[2,-1],[-2,1],[-2,-1],[1,2],[1,-2],[-1,2],[-1,-2]];
                    break;
                case ChessPiece.BISHOP:
                    for(var k=0;k<8;k++){
                        _358.push([1+k,1+k]);
                        _358.push([1+k,-1-k]);
                        _358.push([-1-k,1+k]);
                        _358.push([-1-k,-1-k]);
                    }
                    break;
                case ChessPiece.QUEEN:
                    for(var k=0;k<8;k++){
                        _358.push([1+k,0]);
                        _358.push([1+k,1+k]);
                        _358.push([1+k,-1-k]);
                        _358.push([-1-k,0]);
                        _358.push([-1-k,1+k]);
                        _358.push([-1-k,-1-k]);
                        _358.push([0,-1-k]);
                        _358.push([0,1+k]);
                    }
                    break;
                case ChessPiece.ROOK:
                    for(var k=0;k<8;k++){
                        _358.push([1+k,0]);
                        _358.push([-1-k,0]);
                        _358.push([0,-1-k]);
                        _358.push([0,1+k]);
                    }
                    break;
                case ChessPiece.PAWN:
                    if(_350==ChessPiece.BLACK){
                        _358=[[0,-1],[1,-1],[-1,-1]];
                        if(j==6){
                            _358.push([0,-2]);
                        }
                    }else{
                        _358=[[0,1],[1,1],[-1,1]];
                        if(j==1){
                            _358.push([0,2]);
                        }
                    }
                    break;
            }
            for(var k=0;k<_358.length;k++){
                if(this.canMove(bp,bp.column+_358[k][0],bp.row+_358[k][1],_351,true)){
                    _354.push(new Move(bp.column,bp.row,bp.column+_358[k][0],bp.row+_358[k][1]));
                    if(_352){
                        return _354;
                    }
                }
            }
        }
    }
    return _354;
};
Board.prototype.isKingSafe=function(_35a,_35b){
    var _35c=null;
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var bp=this.boardPieces[i][j];
            if(bp!=null&&bp.piece==ChessPiece.KING&&bp.colour==_35a){
                _35c=bp;
                break;
            }
        }
    }
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var bp=this.boardPieces[i][j];
            if(bp!=null&&bp.colour!=_35a){
                if(this.canMove(bp,_35c.column,_35c.row,_35b,false)){
                    return false;
                }
            }
        }
    }
    return true;
};
Board.prototype.freeBoardPieces=function(_360){
    if(this.boardPieces){
        for(var i=0;i<8;i++){
            for(var j=0;j<8;j++){
                if(this.boardPieces[i][j]!=null){
                    this.boardPieces[i][j].free();
                    this.boardPieces[i][j]=null;
                }
            }
            if(_360){
                this.boardPieces[i]=null;
            }
        }
    }
    if(_360){
        this.boardPieces=null;
    }
};
Board.prototype.freeBoard=function(){
    this.freeBoardPieces(true);
    this.freeMoveArray();
};
Board.prototype.freeMoveArray=function(){
    if(this.moveArray){
        for(var i=0;i<this.moveArray.length;i++){
            var m=this.moveArray[i];
            if(m){
                m.freeMove();
                this.moveArray[i]=null;
            }
        }
    }
};
Board.prototype.cloneBoard=function(){
    var _365=new Board();
    _365.boardName=this.boardName;
    _365.cloned=true;
    _365.boardPieces=this.copyBoardPieces(true);
    _365.moveArray=this.copyMoveArray(false);
    _365.canCastleQueenSide=this.copyCastleQueenSide();
    _365.canCastleKingSide=this.copyCastleKingSide();
    _365.toMove=this.toMove;
    _365.restrictedColourMovement=-1;
    _365.opponentColour=this.opponentColour;
    _365.outputWithoutDisplay=this.outputWithoutDisplay;
    _365.isFlipped=this.isFlipped;
    _365.isUserFlipped=this.isUserFlipped;
    _365.ignoreFlipping=this.ignoreFlipping;
    _365.reverseFlip=this.reverseFlip;
    _365.moveAnimationLength=this.moveAnimationLength;
    _365.moveNumber=this.moveNumber;
    _365.halfMoveNumber=this.halfMoveNumber;
    _365.startFen=this.startFen;
    _365.boardImagePath=this.boardImagePath;
    _365.dontUpdatePositionReachedTable=this.dontUpdatePositionReachedTable;
    if(this.prev_move){
        _365.prev_move=this.prev_move.clone();
    }else{
        _365.prev_move=null;
    }
    return _365;
};
Board.prototype.copyCastleQueenSide=function(){
    return [this.canCastleQueenSide[0],this.canCastleQueenSide[1]];
};
Board.prototype.copyCastleKingSide=function(){
    return [this.canCastleKingSide[0],this.canCastleKingSide[1]];
};
Board.copyMoves=function(_366,_367,_368){
    var _369=new Array();
    if(!_367){
        if(_366&&_366.length>0){
            _369=_366.slice(0);
        }
    }else{
        if(_366){
            for(var i=0;i<_366.length;i++){
                var m=_366[i];
                var newM=null;
                if(m){
                    newM=m.clone(true);
                }
                _369[i]=newM;
            }
        }
    }
    if(_368){
        for(var i=0;i<_366.length;i++){
            if(_366[i].prev){
                if(typeof _366[i].prev.index!="undefined"){
                    _369[i].prev=_369[_366[i].prev.index];
                }
            }
            if(_366[i].next){
                if(typeof _366[i].next.index!="undefined"){
                    _369[i].next=_369[_366[i].next.index];
                }
            }
        }
    }
    return _369;
};
Board.prototype.copyMoveArray=function(_36d){
    return Board.copyMoves(this.moveArray,_36d);
    var _36e=new Array();
    if(!_36d){
        if(this.moveArray&&this.moveArray.length>0){
            _36e=this.moveArray.slice(0);
        }
        return _36e;
    }else{
        if(this.moveArray){
            for(var i=0;i<this.moveArray.length;i++){
                var m=this.moveArray[i];
                if(m){
                    var newM=m.clone(true);
                    _36e[i]=newM;
                }
            }
        }
        return _36e;
    }
};
Board.prototype.copyBoardPieces=function(_372){
    var _373=Board.createBoardArray();
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            if(this.boardPieces[i][j]!=null){
                if(_372){
                    _373[i][j]=this.boardPieces[i][j].makeLightWeight();
                }else{
                    _373[i][j]=this.boardPieces[i][j].copyPiece();
                }
            }else{
                _373[i][j]=null;
            }
        }
    }
    return _373;
};
Board.prototype.createPiece=function(_376,_377,_378){
    if(_378){
        return new LightweightChessPiece(null,_376,_377,this);
    }else{
        return new ChessPiece(this.getPieceDiv(),_376,_377,this);
    }
};
Board.prototype.restoreCastling=function(_379){
    this.canCastleKingSide=_379.kingSide;
    this.canCastleQueenSide=_379.queenSide;
};
Board.prototype.saveCastling=function(){
    var _37a=[this.canCastleQueenSide[0],this.canCastleQueenSide[1]];
    var _37b=[this.canCastleKingSide[0],this.canCastleKingSide[1]];
    return {queenSide:_37a,kingSide:_37b};
};
var firstLightProf=true;
var firstHeavyProf=true;
Board.prototype.setupFromFenLightweight=function(fen,_37d,flip,_37f,_380){
    var _381=false&&firstLightProf;
    if(_381){
        console.profile("setupFromFenLight");
    }
    this.setupFromFenGeneric(fen,_37d,flip,true,_37f,_380);
    if(_381){
        console.profileEnd();
    }
};
Board.prototype.setupFromFenHeavyWeight=function(fen,_383,flip,_385,_386){
    var _387=false&&firstHeavyProf;
    if(_387){
        console.profile("setupFromFenHeavy");
    }
    if(this.lastFromSquare){
        YAHOO.util.Dom.removeClass(this.lastFromSquare,"ct-from-square");
    }
    if(this.lastToSquare){
        YAHOO.util.Dom.removeClass(this.lastToSquare,"ct-to-square");
    }
    this.setupFromFenGeneric(fen,_383,flip,false,_385,_386);
    if(_387){
        console.profileEnd();
    }
};
Board.prototype.setupFromFen=function(fen,_389,flip,_38b,_38c,_38d){
    this.positionsSeen=[];
    if(_38b){
        this.setupFromFenLightweight(fen,_389,flip,_38c,_38d);
    }else{
        this.setupFromFenHeavyWeight(fen,_389,flip,_38c,_38d);
    }
};
Board.prototype.setupFromFenGeneric=function(fen,_38f,flip,_391,_392,_393){
    if(ctime){
        console.time("setupFromFen"+_391);
    }
    if(this.oldSelectedSquare){
        YAHOO.util.Dom.removeClass(this.oldSelectedSquare,"ct-source-square");
    }
    this.oldSelectedSquare=null;
    this.oldSelectedPiece=null;
    this.settingUpPosition=true;
    var _394=fen.split(" ");
    var _395=_394[0].split("/");
    this.halfMoveNumber=parseInt(_394[4]);
    this.moveNumber=parseInt(_394[5])*2;
    var _396=0;
    var row=8;
    this.uptoId=0;
    this.board_xy=null;
    var _398=_394[2];
    var _399=null;
    this.canCastleQueenSide=[false,false];
    this.canCastleKingSide=[false,false];
    if(_398!="-"){
        if(_398.indexOf("K")>=0){
            this.canCastleKingSide[ChessPiece.WHITE]=true;
        }
        if(_398.indexOf("Q")>=0){
            this.canCastleQueenSide[ChessPiece.WHITE]=true;
        }
        if(_398.indexOf("k")>=0){
            this.canCastleKingSide[ChessPiece.BLACK]=true;
        }
        if(_398.indexOf("q")>=0){
            this.canCastleQueenSide[ChessPiece.BLACK]=true;
        }
    }
    if(_393){
        this.startMoveNum=this.moveNumber;
    }
    if(_394[1]=="w"){
        if(_393){
            this.startMoveNum--;
        }
        this.toMove=ChessPiece.WHITE;
        this.opponentColour=ChessPiece.WHITE;
        this.isFlipped=false;
        this.moveNumber--;
    }else{
        this.toMove=ChessPiece.BLACK;
        this.opponentColour=ChessPiece.BLACK;
        this.isFlipped=true;
    }
    if(_392){
        var _39a=_394[3];
        if(_39a!="-"&&_39a.length==2){
            var _39b=_39a[0];
            var _39c=parseInt(_39a[1]);
            if(_39c==3){
                _399=this.createMoveFromString(_39b+"2"+_39b+"4");
            }else{
                _399=this.createMoveFromString(_39b+"7"+_39b+"5");
            }
            _399.prevMoveEnpassant=true;
            this.prev_move=_399;
        }
    }
    if(_38f){
        this.toMove=(ChessPiece.BLACK==this.toMove)?ChessPiece.WHITE:ChessPiece.BLACK;
        this.isFlipped=!this.isFlipped;
    }
    if(flip){
        this.isFlipped=true;
    }
    if(this.reverseFlip){
        this.isFlipped=!this.isFlipped;
    }
    if(this.ignoreFlipping){
        this.isFlipped=false;
    }
    if(this.isUserFlipped){
        this.isFlipped=!this.isFlipped;
    }
    this.updateToPlay();
    this.setupPieceDivs();
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            this.boardPieces[i][j]=null;
        }
    }
    for(var i=0;i<8;i++){
        var line=_395[i];
        row--;
        _396=0;
        for(var j=0;j<line.length;j++){
            var c=line.charAt(j);
            var code=line.charCodeAt(j);
            var num=code-"0".charCodeAt(0);
            if(num>0&&num<9){
                while(num--){
                    var _3a3=this.boardPieces[_396][row];
                    this.boardPieces[_396][row]=null;
                    _396++;
                }
            }else{
                var _3a4=(c+"").toLowerCase().charAt(0);
                var _3a5=ChessPiece.WHITE;
                if(_3a4==c){
                    _3a5=ChessPiece.BLACK;
                }
                var cp;
                switch(_3a4){
                    case "k":
                        cp=this.createPiece(_3a5,ChessPiece.KING,_391);
                        break;
                    case "q":
                        cp=this.createPiece(_3a5,ChessPiece.QUEEN,_391);
                        break;
                    case "r":
                        cp=this.createPiece(_3a5,ChessPiece.ROOK,_391);
                        break;
                    case "b":
                        cp=this.createPiece(_3a5,ChessPiece.BISHOP,_391);
                        break;
                    case "n":
                        cp=this.createPiece(_3a5,ChessPiece.KNIGHT,_391);
                        break;
                    case "p":
                        cp=this.createPiece(_3a5,ChessPiece.PAWN,_391);
                        break;
                    default:
                        alert("unknown piece letter:"+_3a4+" for fen:"+fen);
                }
                if(isGecko||isOpera){
                    cp.setPosition(_396,row,false,null,this.moveAnimationLength);
                    cp.setVisible(true);
                }
                this.boardPieces[_396][row]=cp;
                this.pieces[this.uptoPiece]=cp;
                this.pieces[this.uptoPiece].column=_396;
                this.pieces[this.uptoPiece].row=row;
                this.uptoPiece++;
                _396++;
            }
        }
    }
    if(!isGecko){
        for(var i=0;i<this.uptoPiece;i++){
            this.pieces[i].setPosition(this.pieces[i].column,this.pieces[i].row,false,null,0);
        }
    }
    if(!_391){
        for(var i=0;i<this.uptoPiece;i++){
            this.pieces[i].setVisible(true);
        }
    }
    if(!_391){
        this.createBoardCoords();
    }
    this.settingUpPosition=false;
    if(ctime){
        console.timeEnd("setupFromFen"+_391);
    }
};
Board.prototype.resetMoveListScrollPosition=function(){
    var _3a7=this.movesDisplay.getMovesDisplay();
    if(_3a7){
        var _3a8=new YAHOO.util.Scroll(_3a7,{scroll:{to:[0,0]}},0);
        _3a8.animate();
    }
};
Board.prototype.changePieceSet=function(_3a9,_3aa){
    if(!this.showedIE6Warning){
        var str=_js("Depending on your browser you may need to reload the<br/> page for piece size changes to properly take effect.");
        alert(str.replace("<br/>","\n"));
    }
    this.showedIE6Warning=true;
    if(check_bad_msie()){
        if(!this.showedIE6Warning){
            var str=_js("Internet Explorer version 6 does not support dynamic piece size changes.<br/> Please reload page to view new settings.");
            alert(str.replace("<br/>","\n"));
        }
        this.showedIE6Warning=true;
        return;
    }
    var _3ac=this.pieceSize;
    this.pieceSet=_3a9;
    this.pieceSize=_3aa;
    var _3ad=YAHOO.util.Dom.get(this.boardName+"-boardBorder");
    var _3ae=0;
    if(this.showCoordinates){
        _3ae=15;
    }
    _3ad.className="";
    YAHOO.util.Dom.addClass(_3ad,"ct-board-border"+this.squareColorClass);
    YAHOO.util.Dom.setStyle(_3ad,"width",(this.pieceSize*8+_3ae)+"px");
    YAHOO.util.Dom.setStyle(_3ad,"height",(this.pieceSize*8+_3ae)+"px");
    var _3af=YAHOO.util.Dom.get("ctb-"+this.boardName);
    YAHOO.util.Dom.setStyle(_3af,"width",(this.pieceSize*8)+"px");
    YAHOO.util.Dom.setStyle(_3af,"height",(this.pieceSize*8)+"px");
    var _3b0="ct-white-square"+this.squareColorClass;
    for(var i=7;i>=0;i--){
        for(var j=0;j<8;j++){
            var _3b3=this.getBoardDivFromId(this.boardName+"-s"+j+""+i);
            _3b3.className="";
            YAHOO.util.Dom.addClass(_3b3,_3b0);
            YAHOO.util.Dom.setStyle(_3b3,"width",this.pieceSize+"px");
            YAHOO.util.Dom.setStyle(_3b3,"height",this.pieceSize+"px");
            var _3b4=(((j+1)*(i+1))%19/19*100);
            var _3b5=((65-((j+1)*(i+1)))%19/19*100);
            YAHOO.util.Dom.setStyle(_3b3,"background-position",_3b4+"% "+_3b5+"%");
            _3b0=(_3b0=="ct-black-square"+this.squareColorClass)?"ct-white-square"+this.squareColorClass:"ct-black-square"+this.squareColorClass;
        }
        _3b0=(_3b0=="ct-black-square"+this.squareColorClass)?"ct-white-square"+this.squareColorClass:"ct-black-square"+this.squareColorClass;
    }
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var cp=this.boardPieces[i][j];
            if(cp){
                cp.icon=get_image_str(ChessPiece.pieceIconNames[cp.colour][cp.piece],cp.board.boardImagePath,cp.board.pieceSet,cp.board.pieceSize,cp.board.addVersion);
                if(YAHOO.util.Event.isIE||isOpera){
                    var _3b7=cp.div;
                    _3b7.innerHTML="<img src=\""+cp.icon+"\"/>";
                    var img=_3b7.firstChild;
                    if(!isOpera){
                        fix_ie_png(img);
                    }
                }else{
                    YAHOO.util.Dom.setStyle([cp.div],"backgroundImage","url("+cp.icon+")");
                    YAHOO.util.Dom.setStyle([cp.div],"background-repeat","no-repeat");
                }
                YAHOO.util.Dom.setStyle([cp.div],"height",this.pieceSize+"px");
                YAHOO.util.Dom.setStyle([cp.div],"width",this.pieceSize+"px");
                YAHOO.util.Dom.setStyle([cp.div],"left","");
                YAHOO.util.Dom.setStyle([cp.div],"top","");
                var xy=cp.getNewXYPosition(cp.column,cp.row);
                YAHOO.util.Dom.setXY(cp.div,xy,false);
            }
        }
    }
    if(this.moveArray){
        var move=this.moveArray[0];
        while(move!=null){
            if(move.taken){
                var cp=move.taken;
                if(cp.getNewXYPosition){
                    cp.icon=get_image_str(ChessPiece.pieceIconNames[cp.colour][cp.piece],cp.board.boardImagePath,cp.board.pieceSet,cp.board.pieceSize,cp.board.addVersion);
                    if(YAHOO.util.Event.isIE||isOpera){
                        var _3b7=cp.div;
                        _3b7.innerHTML="<img src=\""+cp.icon+"\"/>";
                        YAHOO.util.Dom.setStyle([cp.div],"position","relative");
                        var img=_3b7.firstChild;
                        if(!isOpera){
                            fix_ie_png(img);
                        }
                    }else{
                        YAHOO.util.Dom.setStyle([cp.div],"backgroundImage","url("+cp.icon+")");
                        YAHOO.util.Dom.setStyle([cp.div],"background-repeat","no-repeat");
                    }
                    YAHOO.util.Dom.setStyle([cp.div],"height",this.pieceSize+"px");
                    YAHOO.util.Dom.setStyle([cp.div],"width",this.pieceSize+"px");
                    YAHOO.util.Dom.setStyle([cp.div],"left","");
                    YAHOO.util.Dom.setStyle([cp.div],"top","");
                    var xy=cp.getNewXYPosition(cp.column,cp.row);
                    YAHOO.util.Dom.setXY(cp.div,xy,false);
                }
            }
            move=move.next;
        }
    }
    if(this.problem){
        var body=YAHOO.util.Dom.get("body");
        if(body){
            YAHOO.util.Dom.setStyle(body,"min-width",((this.pieceSize*8+_3ae)+300+200+120)+"px");
        }
    }
    this.createBoardCoords();
};
Board.prototype.forwardMove=function(e){
    if(this.disableNavigation){
        return;
    }
    if(this.blockFowardBack||this.deferredBlockForwardBack){
        if(clog){
            console.log("returning early from forward due to block forward on");
        }
        return;
    }
    var _3bd=false;
    if(this.tactics&&this.tactics.problemActive){
        if(clog){
            console.log("not forwarding, tactic is active");
        }
        return;
    }
    this.blockForwardBack=true;
    if(this.currentMove&&!this.currentMove.atEnd){
        move=this.currentMove;
        if(move){
            if(clog){
                console.log("forward move:"+move.output());
            }
        }else{
            if(clog){
                console.log("forward move with currentmove null");
            }
        }
        if(move.endNode){
            if(clog){
                console.log("calling processendgame from forward move");
            }
            if(!_3bd){
                this.problem.processEndgame("",true);
            }
            this.toggleToMove();
            this.updateToPlay();
        }else{
            if(clog){
                console.log("forwarding move:"+move.output());
            }
            var _3be=null;
            piece=this.boardPieces[move.fromColumn][move.fromRow];
            if(move.promotion){
                _3be=move.promotion;
                piece.prePromotionColumn=null;
                piece.prePromotionRow=null;
            }
            this.updatePiece(piece,move.toColumn,move.toRow,true,true,false,_3be,true);
            this.toggleToMove();
            this.updateToPlay();
            var _3bf=this.currentMove;
            if(clog){
                if(_3bf){
                    console.log("after forward curmove:"+_3bf.output());
                }else{
                    console.log("after forward cur move null");
                }
            }
            for(var i=0;i<this.registeredForwardMovePostUpdateListeners.length;i++){
                var _3c1=this.registeredForwardMovePostUpdateListeners[i].forwardMovePostUpdateCallback(move);
            }
        }
    }else{
        if(clog){
            console.log("already at end");
        }
        for(var i=0;i<this.registeredForwardAtEndListeners.length;i++){
            var _3c1=this.registeredForwardAtEndListeners[i].forwardAtEndCallback();
        }
    }
    this.blockForwardBack=false;
};
Board.prototype.setupEventHandlers=function(){
    this.tlf=0;
    YAHOO.util.Event.addListener(document,"blur",this.lostFocus,this,true);
    if(!this.avoidMouseoverActive){
        YAHOO.util.Event.addListener(this.boardName+"-container","mouseover",function(e){
            activeBoard=this;
        },this,true);
    }
    if(true||this.clickAndClick){
        YAHOO.util.Event.addListener(this.boardName+"-container","click",this.selectDestSquare,this,true);
    }
    var _3c3="keydown";
    if(isGecko){
        _3c3="keypress";
    }
    YAHOO.util.Event.addListener(document,_3c3,function(e){
        var _3c5=(e.target)?e.target:e.srcElement;
        if(_3c5.form){
            return true;
        }
        var _3c6=_3c5.tagName.toLowerCase();
        switch(_3c6){
            case "input":
            case "textarea":
            case "select":
                return true;
        }
        if(activeBoard!=this){
            return true;
        }
        switch(YAHOO.util.Event.getCharCode(e)){
            case 37:
                this.backMove();
                break;
            case 39:
                this.forwardMove();
                break;
            case 32:
                var ret=this.spaceBar();
                if(!ret){
                    YAHOO.util.Event.preventDefault(e);
                }
                return ret;
                break;
            default:
        }
        return true;
    },this,true);
    YAHOO.util.Event.addListener(this.boardName+"-forward","click",this.forwardMove,this,true);
    YAHOO.util.Event.addListener(this.boardName+"-back","click",this.backMove,this,true);
    YAHOO.util.Event.addListener(this.boardName+"-start","click",this.gotoStart,this,true);
    YAHOO.util.Event.addListener(this.boardName+"-end","click",this.gotoEnd,this,true);
    YAHOO.util.Event.addListener(this.boardName+"-play","click",this.playMoves,this,true);
    YAHOO.util.Event.addListener(this.boardName+"-stop","click",this.stopPlayingMoves,this,true);
    if(this.r){
        YAHOO.util.Event.addListener(this.boardName+"-analyse","click",this.analysePosition,this,true);
        YAHOO.util.Event.addListener(this.boardName+"-showfen","click",this.showBoardFen,this,true);
    }
    if(this.canPasteFen){
        YAHOO.util.Event.addListener(this.boardName+"-pastefen","click",this.pasteFen,this,true);
    }
    if(this.g2){
        YAHOO.util.Event.addListener(this.boardName+"-playcomp","click",this.playComp,this,true);
    }
};
Board.prototype.addFlipListener=function(_3c8){
    this.registeredFlipListeners.push(_3c8);
};
Board.prototype.addSpaceListener=function(_3c9){
    this.registeredSpaceListeners.push(_3c9);
};
Board.prototype.flipBoard=function(){
    this.isUserFlipped=!this.isUserFlipped;
    this.isFlipped=!this.isFlipped;
    this.redrawBoard();
    this.updateToPlay();
    for(var i=0;i<this.registeredFlipListeners.length;i++){
        this.registeredFlipListeners[i].boardFlipped(this);
    }
};
Board.prototype.spaceBar=function(){
    var ret=true;
    for(var i=0;i<this.registeredSpaceListeners.length;i++){
        ret=this.registeredSpaceListeners[i].spacePressed(this);
    }
    return ret;
};
Board.prototype.lostFocus=function(){
    this.tlf++;
};
Board.prototype.redrawBoard=function(){
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var cp=this.boardPieces[i][j];
            if(cp){
                var xy=cp.getNewXYPosition(cp.column,cp.row);
                YAHOO.util.Dom.setXY(cp.div,xy,false);
            }
        }
    }
    if(this.moveArray){
        var move=this.moveArray[0];
        while(move!=null){
            if(move.taken){
                var cp=move.taken;
                if(cp.getNewXYPosition){
                    var xy=cp.getNewXYPosition(cp.column,cp.row);
                    YAHOO.util.Dom.setXY(cp.div,xy,false);
                }
            }
            move=move.next;
        }
    }
    this.createBoardCoords();
    if(this.oldSelectedSquare){
        YAHOO.util.Dom.removeClass(this.oldSelectedSquare,"ct-source-square");
    }
    this.oldSelectedSquare=null;
    this.oldSelectedPiece=null;
    if(this.highlightFromTo){
        if(!this.isFlipped){
            var _3d2=YAHOO.util.Dom.get(this.boardName+"-s"+this.lastFromColumn+""+this.lastFromRow);
            var _3d3=YAHOO.util.Dom.get(this.boardName+"-s"+this.lastToColumn+""+this.lastToRow);
        }else{
            var _3d2=YAHOO.util.Dom.get(this.boardName+"-s"+(7-this.lastFromColumn)+""+(7-this.lastFromRow));
            var _3d3=YAHOO.util.Dom.get(this.boardName+"-s"+(7-this.lastToColumn)+""+(7-this.lastToRow));
        }
        this.updateFromTo(_3d2,_3d3,this.lastFromRow,this.lastFromColumn,this.lastToRow,this.lastToColumn);
    }
};
Board.prototype.getMaxMoveNumber=function(_3d4){
    var _3d5=this.getMaxPly(_3d4);
    if(_3d5>0){
        return parseInt((_3d5+1)/2);
    }else{
        return 0;
    }
};
Board.prototype.getMaxPly=function(_3d6){
    var mv=null;
    if(_3d6){
        if(this.currentMove){
            mv=this.currentMove;
            if(mv.atEnd){
                if(mv.prev){
                    return mv.prev.moveNum;
                }else{
                    return 0;
                }
            }
        }else{
            return 0;
        }
    }else{
        if(this.moveArray){
            mv=this.moveArray[0];
        }
    }
    if(!mv){
        return 0;
    }
    while(mv!=null){
        if(mv.atEnd){
            if(mv.prev){
                return mv.prev.moveNum;
            }else{
                return 0;
            }
        }
        mv=mv.next;
    }
    return 0;
};
Board.fenPositionOnly=function(fen){
    var _3d9=fen.split(" ");
    return _3d9[0]+" "+_3d9[1];
};
Board.fenStripMoveClock=function(fen){
    var _3db=fen.split(" ");
    return _3db[0]+" "+_3db[1]+" "+_3db[2]+" "+_3db[3];
};
Board.fenSamePosition=function(fen1,fen2,_3de){
    if(!fen1||!fen2){
        return false;
    }
    var f1=null;
    var f2=null;
    if(_3de){
        f1=Board.fenPositionOnly(fen1);
        f2=Board.fenPositionOnly(fen2);
    }else{
        f1=Board.fenStripMoveClock(fen1);
        f2=Board.fenStripMoveClock(fen2);
    }
    return (f1==f2);
};
Board.prototype.findFen=function(mv,brd,fen,_3e4){
    var res=this.findFen2(mv,brd,fen,true);
    if(res.move){
        return res.move;
    }else{
        if(_3e4){
            if(res.clockStrip){
                return res.clockStrip;
            }else{
                if(res.fullStrip){
                    return res.fullStrip;
                }
            }
        }
    }
    return null;
};
Board.prototype.findFen2=function(mv,brd,fen,_3e9){
    var _3ea=brd.cloneBoard();
    var res=Object();
    var _3ec=null;
    var _3ed=null;
    res.move=null;
    if(_3e9){
        _3ea.gotoMoveIndex(-1,true,true,true,true);
    }
    var _3ee=null;
    while(mv){
        var _3ef=_3ea.boardToFen();
        if(_3ef==fen){
            res.move=_3ee;
            res.clockStrip=null;
            res.fullStrip=null;
            return res;
        }else{
            if(Board.fenSamePosition(fen,_3ef)){
                _3ec=_3ee;
            }else{
                if(Board.fenSamePosition(fen,_3ef,true)){
                    _3ed=_3ee;
                }
            }
        }
        if(mv.atEnd){
            break;
        }
        if(mv.vars&&mv.vars.length>0){
            for(var i=0;i<mv.vars.length;i++){
                var _3f1=this.findFen2(mv.vars[i],_3ea,fen,false);
                if(_3f1){
                    if(_3f1.move){
                        return _3f1;
                    }else{
                        if(_3f1.clockStrip){
                            _3ec=_3f1.clockStrip;
                        }else{
                            if(_3f1.fullStrip){
                                _3ed=_3f1.fullStrip;
                            }
                        }
                    }
                }
            }
        }
        if(clog){
            console.log("about to make mv:"+mv.output()+" fen:"+_3ea.boardToFen());
        }
        _3ea.makeMove(mv,_3ea.boardPieces[mv.fromColumn][mv.fromRow],false,this.moveAnimationLength,false,false);
        if(clog){
            console.log("finished making mv");
        }
        _3ee=mv;
        mv=mv.next;
        if(clog){
            console.log("toMove:"+_3ea.toMove);
        }
        _3ea.setCurrentMove(mv);
        _3ea.toggleToMove();
    }
    if(_3ec){
        res.clockStrip=_3ec;
    }
    if(_3ed){
        res.fullStrip=_3ed;
    }
    return res;
};
Board.prototype.gotoFen=function(fen,_3f3){
    if(clog){
        console.log("about to find fen for:"+fen);
    }
    var _3f4=this.findFen(this.moveArray[0],this,fen,_3f3);
    if(_3f4){
        if(clog){
            console.log("found move:"+_3f4.output()+" for fen:"+fen);
        }
        this.gotoMoveIndex(_3f4.index);
    }else{
        if(clog){
            console.log("didn't find move for fen:"+fen);
        }
    }
};
Board.prototype.getMaxMoveIndex=function(){
    return this.moveArray.length-1;
};
Board.prototype.gotoMoveIndex=function(_3f5,_3f6,_3f7,_3f8,_3f9){
    if(clog){
        console.log("going to move index:"+_3f5);
    }
    var _3fa=!_3f7;
    if(!this.moveArray||this.moveArray.length<=_3f5||(_3f5==-1&&this.moveArray.length==0)){
        return;
    }
    var _3fb=this.boardName+"-piecestaken";
    var _3fc=YAHOO.util.Dom.get(_3fb);
    if(_3fc){
        _3fc.innerHTML="";
    }
    if(_3f5==-1){
        var flip=false;
        if(this.prev_move&&!this.prev_move.prevMoveEnpassant){
            flip=true;
        }
        this.setupFromFen(this.startFen,flip,false,_3f9);
        if(this.prev_move&&!this.prev_move.prevMoveEnpassant){
            this.makeMove(this.prev_move,this.boardPieces[this.prev_move.fromColumn][this.prev_move.fromRow],!_3f7,this.moveAnimationLength,true,true);
            this.updateToPlay();
        }
        if(this.moveArray&&this.moveArray.length>0){
            this.setCurrentMove(this.moveArray[0],_3f6);
        }else{
            this.setCurrentMove(this.firstMove,_3f6);
        }
        if(!_3f6){
            this.setForwardBack();
        }
        if(!_3f8){
            for(var i=0;i<this.registeredGotoMoveIndexListeners.length;i++){
                var _3ff=this.registeredGotoMoveIndexListeners[i].gotoMoveIndexCallback(_3f5);
            }
        }
        return;
    }
    var _400=new Array();
    var move=this.moveArray[_3f5];
    if(clog&&move){
        console.log("gotomoveindex move:"+move.output());
        if(move.next){
            console.log("gotomoveindex move.next:"+move.next.output());
        }
        if(move.prev){
            console.log("gotomoveindex move.prev:"+move.prev.output());
        }
    }
    var _402=0;
    if(move.next!=null){
        this.setCurrentMove(move.next,_3f6);
    }else{
        if(clog){
            console.log("move next null with move:"+move.output());
        }
    }
    while(move!=null&&!move.dummy){
        _400[_402++]=move;
        move=move.prev;
    }
    var flip=false;
    if(this.prev_move&&!this.prev_move.prevMoveEnpassant){
        flip=true;
    }
    this.setupFromFen(this.startFen,flip,false,true);
    if(this.prev_move&&!this.prev_move.prevMoveEnpassant){
        if(clog){
            console.log("gotomoveindex prev_move:"+this.prev_move.output());
        }
        this.makeMove(this.prev_move,this.boardPieces[this.prev_move.fromColumn][this.prev_move.fromRow],false,this.moveAnimationLength,true,true);
        this.updateToPlay();
    }
    for(var i=_402-1;i>=1;i--){
        var move=_400[i];
        this.makeMove(move,this.boardPieces[move.fromColumn][move.fromRow],false,this.moveAnimationLength,true,false);
        this.toggleToMove();
    }
    if(!_3f6){
        this.convertPiecesFromLightWeight(_3f5);
    }
    var move=_400[0];
    this.makeMove(move,this.boardPieces[move.fromColumn][move.fromRow],_3fa,this.moveAnimationLength,true,true);
    this.toggleToMove();
    this.updateToPlay();
    if(!_3f6){
        this.setForwardBack();
    }
    if(!_3f8){
        for(var i=0;i<this.registeredGotoMoveIndexListeners.length;i++){
            var _3ff=this.registeredGotoMoveIndexListeners[i].gotoMoveIndexCallback(_3f5);
        }
    }
};
Board.prototype.gotoStart=function(e){
    if(this.disableNavigation){
        return;
    }
    if(this.lastFromSquare){
        YAHOO.util.Dom.removeClass(this.lastFromSquare,"ct-from-square");
    }
    if(this.lastToSquare){
        YAHOO.util.Dom.removeClass(this.lastToSquare,"ct-to-square");
    }
    this.gotoMoveIndex(-1);
    if(this.problem){
        if(this.currentMove&&this.currentMove.bestMoves){
            this.problem.showBestMoves(this.currentMove,this.currentMove.bestMoves,this.currentMove.correctMove,this.currentMove.wrongMove);
        }else{
            this.problem.clearBestMoves();
        }
    }
};
Board.prototype.gotoEnd=function(e){
    if(this.disableNavigation){
        return;
    }
    if(clog){
        console.log("goto end called");
    }
    if(this.tactics&&this.tactics.problemActive){
        this.tactics.autoForward=false;
        this.tactics.markProblem(false,false,"NULL","NULL");
    }
    if(clog){
        console.log("jumping to start");
    }
    this.gotoMoveIndex(-1,true,true,true);
    var _405=0;
    while(this.currentMove&&this.currentMove.next!=null){
        var move=this.currentMove;
        if(clog){
            console.log("going to end move:"+move.output());
        }
        this.makeMove(move,this.boardPieces[move.fromColumn][move.fromRow],false,this.moveAnimationLength,true,true);
        _405=move.index;
        this.toggleToMove();
        this.setCurrentMove(move.next);
    }
    for(var i=0;i<this.registeredGotoMoveIndexListeners.length;i++){
        var _408=this.registeredGotoMoveIndexListeners[i].gotoMoveIndexCallback(_405);
    }
};
Board.prototype.gotoPly=function(_409,_40a){
    if(clog){
        console.log("goto ply called");
    }
    this.gotoMoveIndex(-1,true,true,true);
    var cnt=1;
    var _40c=0;
    while(cnt<=_409&&this.currentMove&&this.currentMove.next!=null){
        var move=this.currentMove;
        if(clog){
            console.log("going to end move:"+move.output());
        }
        this.makeMove(move,this.boardPieces[move.fromColumn][move.fromRow],false,this.moveAnimationLength,true,true);
        _40c=move.index;
        this.toggleToMove();
        this.setCurrentMove(move.next);
        cnt++;
    }
    if(_40a){
        for(var i=0;i<this.registeredGotoMoveIndexListeners.length;i++){
            var _40f=this.registeredGotoMoveIndexListeners[i].gotoMoveIndexCallback(_40c);
        }
    }
};
Board.prototype.playMove=function(self){
    if(!self.keepPlayingMoves||!self.currentMove||!self.currentMove.next){
        var play=YAHOO.util.Dom.get(this.boardName+"-play");
        play.src=this.boardImagePath+"/static/images/control_play_blue"+this.getVersString()+".gif";
        self.keepPlayingMoves=false;
        return;
    }
    self.forwardMove();
    setTimeout(function(){
        self.playMove(self);
    },self.pauseBetweenMoves);
};
Board.prototype.insertLineToMoveIndexPosition=function(_412,_413,_414,_415,_416){
    var _415=Board.copyMoves(_415,true,true);
    var mv=null;
    if(!this.moveArray||this.moveArray.length==0||this.moveArray[0]==null||this.moveArray[0].atEnd||_413==this.startFen){
        mv=null;
        if(clog){
            console.log("no moves or initial position, using first move");
        }
    }else{
        if(clog){
            console.log("calling find fen....");
        }
        if(_412>=0){
            mv=this.moveArray[_412];
        }
        if(!mv){
            mv=this.findFen(this.moveArray[0],this,_413,false);
        }
        if(clog){
            console.log("finished calling find fen");
        }
        if(!mv){
            return;
        }
    }
    var _418=-1;
    if(this.currentMove){
        if(this.currentMove.prev){
            _418=this.currentMove.prev.index;
        }
    }
    if(_414){
        _415[0].beforeComment=_414;
    }
    if(clog){
        if(mv){
            console.log("mv:"+mv.output()+" mv next:"+mv.next+" oldCurrentMoveIndex:"+_418);
        }else{
            console.log("mv: null oldCurrentMoveIndex:"+_418);
        }
    }
    var _419=null;
    var _41a=null;
    if(mv&&mv.next&&!mv.next.atEnd){
        _41a=mv.next;
    }else{
        _419=mv;
    }
    if(mv){
        this.gotoMoveIndex(mv.index);
    }else{
        if(this.moveArray&&this.moveArray.length>0){
            _41a=this.moveArray[0];
            if(_41a){
                if(clog){
                    console.log("variation parent from first move:"+_41a.output());
                }
                this.gotoMoveIndex(-1);
            }
        }else{
            this.currentMove=null;
        }
    }
    if(clog){
        if(this.currentMove){
            console.log("current move before insertline:"+this.currentMove.output());
        }else{
            console.log("no current move before insertline");
        }
    }
    if(clog){
        if(_41a){
            console.log("var parent:"+_41a.output());
        }else{
            console.log("var null");
        }
        if(_419){
            console.log("move ins after:"+_419.output());
        }else{
            console.log("moveinsafter null");
        }
    }
    this.insertMovesFromMoveList(_415[0],true,_41a,_419,_416);
    if(clog){
        if(this.currentMove){
            console.log("current move after insertline:"+this.currentMove.output());
        }else{
            console.log("no current move after insertline");
        }
    }
    this.gotoMoveIndex(_418);
};
Board.prototype.getVersString=function(){
    var _41b=".vers"+SITE_VERSION;
    if(!this.addVersion){
        _41b="";
    }
    return _41b;
};
Board.prototype.playMoves=function(e){
    if(this.disableNavigation){
        return;
    }
    this.keepPlayingMoves=true;
    var play=YAHOO.util.Dom.get(this.boardName+"-play");
    play.src=this.boardImagePath+"/static/images/disabled_control_play_blue"+this.getVersString()+".gif";
    this.playMove(this);
};
Board.prototype.stopPlayingMoves=function(e){
    this.keepPlayingMoves=false;
};
Board.prototype.pasteFen=function(e){
    for(var i=0;i<this.registeredPasteFenClickedListeners.length;i++){
        var _421=this.registeredPasteFenClickedListeners[i].pasteFenClickedCallback();
    }
};
Board.prototype.playComp=function(e){
    window.open("/play-computer/"+this.boardToFen());
};
Board.prototype.showBoardFen=function(e){
    var fen=this.boardToFen();
    var _425=new YAHOO.widget.SimpleDialog("fenDialog",{fixedcenter:false,visible:true,draggable:true,constraintoviewport:false,buttons:[{id:"linkbutton4",text:"Test"},{text:_js("Ok"),handler:function(){
        _425.hide();
    },isDefault:true}]});
    _425.setHeader(_js("Position FEN"));
    _425.setBody("<textarea class=\"showPgn\" id=\"fenText\" rows=\"1\" readonly=\"true\" cols=\""+(fen.length+9)+"\">"+fen+"</textarea>");
    _425.render(document.body);
    _425.setFooter("<span id=\"copyToComment\"></span><span id=\"fenok\"></span>");
    _425.center();
    var _426=this;
    if(this.problem&&this.problem.comments){
        var _427=new YAHOO.widget.Button({type:"button",label:_js("Copy To Comment"),container:"fenok",onclick:{fn:function(){
            _426.copyFenToComment(fen,Board.COPY_COMMENT_PROBLEM);
            _425.hide();
        }}});
    }
    if(this.gameComments){
        var _428=new YAHOO.widget.Button({type:"button",label:_js("Copy To Game Comment"),container:"fenok",onclick:{fn:function(){
            _426.copyFenToComment(fen,Board.COPY_COMMENT_GAME);
            _425.hide();
        }}});
    }
    if(this.playerComments){
        var _429=new YAHOO.widget.Button({type:"button",label:_js("Copy To Player Comment"),container:"fenok",onclick:{fn:function(){
            _426.copyFenToComment(fen,Board.COPY_COMMENT_PLAYER);
            _425.hide();
        }}});
    }
    if(this.openingComments){
        var _42a=new YAHOO.widget.Button({type:"button",label:_js("Copy To Opening Comment"),container:"fenok",onclick:{fn:function(){
            _426.copyFenToComment(fen,Board.COPY_COMMENT_OPENING);
            _425.hide();
        }}});
    }
    var _42b=new YAHOO.widget.Button({type:"button",label:_js("Ok"),container:"fenok",onclick:{fn:function(){
        _425.hide();
    }}});
};
Board.prototype.copyFenToComment=function(fen,_42d){
    switch(_42d){
        case (Board.COPY_COMMENT_PROBLEM):
            if(this.problem){
                var flip=false;
                var col=fen.split(" ")[1];
                var col2=this.startFen.split(" ")[1];
                if(col==col2){
                    flip=true;
                }
                this.problem.comments.copyFenToComment(fen,flip);
            }
            break;
        case (Board.COPY_COMMENT_GAME):
            this.gameComments.copyFenToComment(fen);
            break;
        case (Board.COPY_COMMENT_PLAYER):
            this.playerComments.copyFenToComment(fen);
            break;
        case (Board.COPY_COMMENT_OPENING):
            this.openingComments.copyFenToComment(fen);
            break;
    }
};
Board.COPY_COMMENT_PROBLEM=0;
Board.COPY_COMMENT_PLAYER=1;
Board.COPY_COMMENT_GAME=2;
Board.COPY_COMMENT_OPENING=3;
Board.prototype.copyAnalysisToComment=function(_431,fen,flip,_434){
    switch(_434){
        case (Board.COPY_COMMENT_PROBLEM):
            if(this.problem){
                this.problem.comments.copyAnalysisToComment(_431,fen,flip);
            }
            break;
        case (Board.COPY_COMMENT_GAME):
            this.gameComments.copyAnalysisToComment(_431,fen,flip);
            break;
        case (Board.COPY_COMMENT_PLAYER):
            this.playerComments.copyAnalysisToComment(_431,fen,flip);
            break;
        case (Board.COPY_COMMENT_OPENING):
            this.openingComments.copyAnalysisToComment(_431,fen,flip);
            break;
    }
};
Board.squareColours=new Array(8);
var pCol=ChessPiece.BLACK;
for(var i=0;i<8;i++){
    Board.squareColours[i]=new Array(8);
    for(var j=0;j<8;j++){
        Board.squareColours[i][j]=pCol;
        pCol=Board.invertToMove(pCol);
    }
    pCol=Board.invertToMove(pCol);
}
Board.getSquareColour=function(i,j){
    return Board.squareColours[i][j];
};
Board.prototype.isInsufficientMaterial=function(_437){
    var _438=0;
    var _439=0;
    var _43a=0;
    var _43b=0;
    var _43c=0;
    var _43d=0;
    var _43e=0;
    var _43f=0;
    var _440=0;
    var _441=0;
    for(var i=0;i<8;i++){
        for(var j=0;j<8;j++){
            var p=this.boardPieces[i][j];
            if(p){
                if(p.piece==ChessPiece.PAWN){
                    if(p.colour==ChessPiece.WHITE){
                        _438++;
                    }else{
                        _439++;
                    }
                }else{
                    if(p.piece!=ChessPiece.KING){
                        if(p.colour==ChessPiece.WHITE){
                            _43a++;
                            if(p.piece==ChessPiece.KNIGHT){
                                _43c++;
                            }else{
                                if(p.piece==ChessPiece.BISHOP){
                                    if(Board.getSquareColour(i,j)==ChessPiece.WHITE){
                                        _43e++;
                                    }else{
                                        _43f++;
                                    }
                                }
                            }
                        }else{
                            _43b++;
                            if(p.piece==ChessPiece.KNIGHT){
                                _43d++;
                            }else{
                                if(p.piece==ChessPiece.BISHOP){
                                    if(Board.getSquareColour(i,j)==ChessPiece.WHITE){
                                        _440++;
                                    }else{
                                        _441++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    function checkBothPieces(){
        if(_438>0||_439>0){
            return false;
        }
        if(_43a==_43b==0){
            return true;
        }else{
            if(_43a==_43c&&_43b==0){
                return true;
            }else{
                if(_43b==_43d&&_43a==0){
                    return true;
                }else{
                    if(_43a==_43e&&_43b==_440){
                        return true;
                    }else{
                        if(_43a==_43f&&_43b==_441){
                            return true;
                        }else{
                            if(_43b==_440&&_43a==_43e){
                                return true;
                            }else{
                                if(_43a==_441&&_43a==_43f){
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    if(_437==-1){
        return checkBothPieces();
    }else{
        if(_437==ChessPiece.WHITE){
            if(checkBothPieces()){
                return true;
            }
            if(_438==0&&_43a==0){
                return true;
            }else{
                if(_43a==_43e&&_43b==_440){
                    return true;
                }else{
                    if(_43a==_43f&&_43b==_441){
                        return true;
                    }else{
                        if(_43a==_43c&&(_43b==0&&_439==0)){
                            return true;
                        }
                    }
                }
            }
            return false;
        }else{
            if(checkBothPieces()){
                return true;
            }
            if(_439==0&&_43b==0){
                return true;
            }else{
                if(_43b==_440&&_43a==_43e){
                    return true;
                }else{
                    if(_43b==_441&&_43a==_43f){
                        return true;
                    }else{
                        if(_43b==_43d&&(_43a==0&&_438==0)){
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
};
Board.prototype.analysePosition=function(e){
    window.parentBoard=this;
    var _446=(this.pieceSize*8)+450+50;
    var _447=(this.pieceSize*8)+250;
    var _448=window.open("/windows/analyse.html",this.analysisWindowName,"width="+_446+",height="+_447+",resizable=1,scrollbars=1,location=0,copyhistory=0,status=0,toolbar=0,menubar=0");
    _448.focus();
};
Board.prototype.backMove=function(e){
    if(this.disableNavigation){
        return;
    }
    if(this.blockFowardBack||this.deferredBlockForwardBack){
        return;
    }
    var _44a=this.currentMove;
    if(this.tactics){
        if(this.tactics.problemActive){
            return;
        }
    }
    this.blockForwardBack=true;
    if(this.currentMove&&this.currentMove.prev!=null){
        YAHOO.util.Dom.removeClass(this.lastFromSquare,"ct-from-square");
        YAHOO.util.Dom.removeClass(this.lastToSquare,"ct-to-square");
        this.lastFromRow=null;
        if(this.oldSelectedSquare){
            YAHOO.util.Dom.removeClass(this.oldSelectedSquare,"ct-source-square");
        }
        this.oldSelectedSquare=null;
        this.oldSelectedPiece=null;
        var col=this.toMove;
        if(col==ChessPiece.WHITE){
            col=ChessPiece.BLACK;
        }else{
            col=ChessPiece.WHITE;
        }
        if(!this.dontUpdatePositionReachedTable){
            var _44c=this.boardToUniqueFen(col);
            if(this.positionsSeen[_44c]){
                this.positionsSeen[_44c]--;
            }
        }
        this.toggleToMove();
        this.updateToPlay();
        move=this.currentMove.prev;
        if(move){
            if(clog){
                console.log("backwards moving to prev move:"+move.output()+" from current move:"+this.currentMove.output());
            }
        }
        this.setCurrentMove(move);
        piece=this.boardPieces[move.toColumn][move.toRow];
        if(!piece){
            if(clog){
                console.log("got empty piece in backMove");
            }
        }
        takenPiece=move.taken;
        this.board_xy=null;
        piece.setPosition(move.fromColumn,move.fromRow,true,null,this.moveAnimationLength);
        this.boardPieces[move.fromColumn][move.fromRow]=piece;
        if(move.promotion){
            piece.changePiece("p");
        }
        piece.setVisible(true);
        this.canCastleQueenSide[0]=move.preCastleQueenSide[0];
        this.canCastleQueenSide[1]=move.preCastleQueenSide[1];
        this.canCastleKingSide[0]=move.preCastleKingSide[0];
        this.canCastleKingSide[1]=move.preCastleKingSide[1];
        this.halfMoveNumber=move.preHalfMoveNumber;
        var _44d=false;
        if(piece.piece==ChessPiece.KING&&Math.abs(move.fromColumn-move.toColumn)>1){
            _44d=true;
        }
        this.moveNumber--;
        if(this.moveNumber<=0){
            this.moveNumber=1;
        }
        if(takenPiece&&!_44d){
            this.board_xy=null;
            var _44e=move.toColumn;
            var _44f=move.toRow;
            if(piece.piece==ChessPiece.PAWN&&move.fromColumn!=move.toColumn&&takenPiece.enPassant){
                _44f=move.fromRow;
                this.boardPieces[move.toColumn][move.toRow]=null;
            }
            takenPiece.setPosition(_44e,_44f,false,null,this.moveAnimationLength);
            this.boardPieces[_44e][_44f]=takenPiece;
            move.taken=null;
            this.processTaken(takenPiece,false);
        }else{
            this.boardPieces[move.toColumn][move.toRow]=null;
        }
        if(_44d){
            var _450=move.toRow;
            var _451;
            var _452;
            if(move.fromColumn>move.toColumn){
                _451=0;
                _452=3;
            }else{
                _451=7;
                _452=5;
            }
            var _453=this.boardPieces[_452][_450];
            _453.setPosition(_451,_450,true,null,this.moveAnimationLength);
            this.boardPieces[_451][_450]=_453;
            this.boardPieces[_452][_450]=null;
        }
        if(move!=null&&move.prev!=null&&move.prev.next!=move){
            move=move.prev.next;
            if(clog){
                if(move){
                    console.log("moving backwards out of variation moving to:"+move.output());
                }else{
                    console.log("jumping out of variation to null move");
                }
            }
        }
        for(var i=0;i<this.registeredBackMovePreCurrentListeners.length;i++){
            var _455=this.registeredBackMovePreCurrentListeners[i].backMovePreCurrentCallback(move,_44a);
        }
        this.setCurrentMove(move);
        this.setForwardBack();
    }
    this.blockForwardBack=false;
};
Board.prototype.getMovesToCurrent=function(){
    var mvs=[];
    var res=[];
    var mv=this.currentMove;
    if(!mv||!mv.prev){
        return res;
    }
    mv=mv.prev;
    while(mv){
        mvs.push(mv);
        mv=mv.prev;
    }
    for(var i=mvs.length-1;i>=0;i--){
        res.push(mvs[i].toMoveString());
    }
    return res;
};
Board.prototype.getAllMoves=function(){
    var mv=null;
    if(this.moveArray&&this.moveArray.length>0){
        mv=this.moveArray[0];
    }else{
        mv=this.firstMove;
    }
    if(!mv){
        return [];
    }
    var mvs=[];
    var res=[];
    while(mv&&!mv.atEnd){
        res.push(mv.toMoveString());
        mv=mv.next;
    }
    return res;
};
Board.prototype.countPly=function(){
    var mv=null;
    if(this.moveArray&&this.moveArray.length>0){
        mv=this.moveArray[0];
    }else{
        mv=this.firstMove;
    }
    var _45e=0;
    while(mv&&!mv.atEnd){
        _45e++;
        mv=mv.next;
    }
    return _45e;
};
Board.prototype.processTaken=function(_45f,_460){
    var _461=this.boardName+"-piecestaken";
    var _462=YAHOO.util.Dom.get(_461);
    if(_462){
        if(_460){
            var _463=get_image_str(ChessPiece.pieceIconNames[_45f.colour][_45f.piece],this.boardImagePath,this.pieceSet,this.pieceTakenSize,this.addVersion);
            _462.innerHTML=_462.innerHTML+"<img src=\""+_463+"\"/>";
        }else{
            var _464=_462.innerHTML.split("<");
            _462.innerHTML="";
            for(var i=1;i<_464.length-1;i++){
                _462.innerHTML=_462.innerHTML+"<"+_464[i];
            }
        }
    }
};
Pool=function(){
    this.pool=new Array();
    this.count=-1;
    this.numGot=0;
    this.numPut=0;
};
Pool.prototype.getObject=function(){
    var o=null;
    if(this.count>=0){
        this.numGot++;
        o=this.pool[this.count--];
    }
    return o;
};
Pool.prototype.putObject=function(o){
    if(o!=null){
        this.numPut++;
        this.pool[++this.count]=o;
    }
};
var boardPool=new Pool();
function touchHandler(_468){
    var _469=_468.changedTouches,_46a=_469[0],type="";
    switch(_468.type){
        case "touchstart":
            type="mousedown";
            break;
        case "touchmove":
            type="mousemove";
            break;
        case "touchend":
            type="mouseup";
            break;
        default:
            return;
    }
    var _46c=document.createEvent("MouseEvent");
    _46c.initMouseEvent(type,true,true,window,1,_46a.screenX,_46a.screenY,_46a.clientX,_46a.clientY,false,false,false,false,0,null);
    _46a.target.dispatchEvent(_46c);
    _468.preventDefault();
}
function initIphone(_46d){
    _46d.addEventListener("touchstart",touchHandler,true);
    _46d.addEventListener("touchmove",touchHandler,true);
    _46d.addEventListener("touchend",touchHandler,true);
    _46d.addEventListener("touchcancel",touchHandler,true);
}
FenBoard=function(fen,_46f){
    if(typeof _46f.pieceSize=="undefined"){
        _46f.pieceSize=24;
    }
    _46f.fenBoard=true;
    _46f.dontOutputNavButtons=true;
    _46f.avoidMouseoverActive=true;
    this.chessapp=new ChessApp(_46f);
    this.chessapp.init();
    this.chessapp.board.disableUpdateToPlay=true;
    this.chessapp.board.setupFromFen(fen,false,false,false);
    this.board=this.chessapp.board;
    this.board.startFen=fen;
};

