function move_handler(){
  jQuery('#comment_viewer_prev').text('');
  jQuery('#comment_viewer_next').text('');
  var next = jQuery('.ct-board-move-current').next()
  var prev = jQuery('.ct-board-move-current').prev()
  if (prev.hasClass('ct-board-move-comment')){
    alert(prev.text())
    jQuery('#comment_viewer_prev').text(prev.text())
  }
  if (next.hasClass('ct-board-move-comment')){
    jQuery('#comment_viewer_next').text(next.text())
  }
}

function hook_move_handlers(){
  jQuery("#diagramI-nav-buttons-only").bind("click boardmove", move_handler)
}

function addFigurines() {
    jQuery('.ct-board-move-movetext').replaceWith(function () {
        var movetext = $(this).text();
        var startletter = movetext[0];
        if (startletter == 'B') {
            var newMovetext = movetext.replace("B", "¥");
        } else if (startletter == 'N') {
            var newMovetext = movetext.replace("N", "¤");
        } else if (startletter == 'R') {
            var newMovetext = movetext.replace("R", "¦");
        } else if (startletter == 'Q') {
            var newMovetext = movetext.replace("Q", "£");
        } else if (startletter == 'K') {
            var newMovetext = movetext.replace("K", "¢");
        } else {
            var newMovetext = movetext;
        };
        var newContent = '<span class="ct-board-move-movetext">' + newMovetext + '</span>';
        return newContent;
    });
};


function MyPgnViewer (board_data) {
    new PgnViewer(board_data, addFigurines);
}

