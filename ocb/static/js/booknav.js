var booknav = {
    init: function() {
        $('.sectionList').hide();

        $('a.chapter').click(function () {
            $('.sectionList').hide();
            $(this).siblings(".sectionList").toggle();
        });

        $('#mainNav').children().find('li').each( function(idx,item) {
            var link = $(item).children('a').first();
            if (window.location.pathname == link.attr('href')) {
                link.addClass('selected');
                link.parents("ul").parents('li').first().addClass('selected');
                link.parents("ul").show();
            }
        });

        // TOGGLE TOC
        $('#toc_toggler').click(function() {
            $('#TOC').toggle();
        });

        // NAVIGATE BACK AND FORTH
        if ($('h1').length > 1) {
            var container = $('<span>');
            $.each(['previous', 'next'], function(idx, item) {
                var ele = $("<a>");
                ele.text(item);
                ele.addClass('previous_next_buttons').addClass(item);
                container.append(ele);
            });

            $('.previous_next').append(container);
            next_chap_link = $('a.selected').parent().next().children('a');
            prev_chap_link = $('a.selected').parent().prev().children('a');
            if (next_chap_link.length == 0) {
                next_chap_link = $('a.selected').parent().parent().parent().next().children().find('a').first();
            }
            if (next_chap_link) {
                $('a.next').attr('href', next_chap_link.attr('href'));
            }
            if (prev_chap_link.length == 0) {
                prev_chap_link = $('a.selected').parent().parent().parent().prev().children().find('a').last();
            }
            if (prev_chap_link) {
                $('a.previous').attr('href', prev_chap_link.attr('href'))
            }

            // INIT JQUERY-PJAX
            $(document).pjax('a.pjax-href', '#pjax-container')
        }
    }
}
