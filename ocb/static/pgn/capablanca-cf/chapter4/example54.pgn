[Event "?"]
[Site "?"]
[Date "1919.??.??"]
[Round "?"]
[White "Winter, William"]
[Black "Capablanca, Jose"]
[Result "0-1"]
[ECO "C49"]
[Opening "Four Knights Game"]

1.e4 e5 2.Nf3 Nc6 3.Nc3 Nf6 4.Bb5 Bb4 5.O-O O-O 6.Bxc6 {Niemzowitch's
variation, which I have played successfully in many a game.  It gives White a
very solid game. Niemzowitch's idea is that White will in due time be able to
play f4, opening a line for his Rooks, which, in combination with the posting
of a Knight at f5, should be sufficient to win. He thinks that should Black
attempt to stop the Knight from going to f5, he will have to weaken his game in
some other way. Whether this is true or not remains to be proved, but in my
opinion the move is perfectly good. On the other hand, there is no question
that Black can easily develop his pieces. But it must be considered that in
this variation White does not attempt to hinder Black's development, he simply
attempts to build up a position which he considers impregnable and from which
he can start an attack in due course.} 6...dxc6
    ( {The alternative,}6...bxc6 {gives White the best of the game, without
doubt. (See game Capablanca-Kupchick, from Havana International Masters
Tournament Book, 1913, by J. R. Capablanca; or a game in the Carlsbad
Tournament of 1911, Vidmar playing Black against Alechin.)})
7.d3 Bd6 8.Bg5 {This move is not at all in accordance with the nature of this
variation.  The general strategical plan for White is to play h3, to be
followed in time by the advance of the "g" Pawn to g4, and the bringing of the
Queen Knight to f5 via e2 and g3 or d1 and e3. Then, if possible, the King
Knight is linked with the other Knight by placing it at either h4, g3, or e3 as
the occasion demands. The White King sometimes remains at g1, and other times
it is placed at g2, but mostly at h1. Finally, in most cases comes f4, and then
the real attack begins. Sometimes it is a direct assault against the King (See
Niemzowitch's game in the All Russian Masters Tournament, 1914, at St.
Petersburg, against Levitzki, I believe), and at other times it comes simply to
finessing for positional advantage in the end-game, after most of the pieces
have been exchanged (See Capablanca-Janowski game, New York Masters Tournament,
1913).
} 8...h6 9.Bh4 c5 {To prevent d4 and to draw White into playing Nd5,
which would prove fatal. Black's plan is to play g5, as soon as the
circumstances permit, in order to free his Queen and Knight from the pin by the
Bishop.} 10.Nd5 {White falls into the trap. Only lack of experience can account
for this move. White should have considered that a player of my experience and
strength could never allow such a move if it were good.} 10...g5 {After this
move White's game is lost. White cannot play Nxg5, because ... Nxd5 will win a
piece. Therefore he must play Bg3, either before or after Nxf6+, with
disastrous results in either case, as will be seen.} 11.Nxf6+ Qxf6 12.Bg3 Bg4
13.h3 Bxf3 14.Qxf3 Qxf3 15.gxf3 f6 {A simple examination will show that White
is minus a Bishop for all practical purposes. He can only free it by
sacrificing one Pawn, and possibly not even then. At least it would lose time
besides the Pawn. Black now devotes all his energy to the Queen's side, and,
having practically a Bishop more, the result cannot be in doubt. The rest of
the game is given, so that the student may see how simple it is to win such a
game.} 16.Kg2 a5 17.a4 Kf7 18.Rh1 Ke6 19.h4 Rfb8 {There is no necessity to pay
any attention to the King's side, because White gains nothing by exchanging
Pawns and opening the King's Rook file.} 20.hxg5 hxg5 21.b3 c6 22.Ra2 b5 23.Rha1
c4 {If White takes the proffered Pawn, Black regains it immediately by Rb4,
after bxc4.} 24.axb5 cxb3 25.cxb3 Rxb5 26.Ra4 Rxb3 27.d4 Rb5 28.Rc4 Rb4 29.Rxc6
Rxd4 {White resigns.} 0-1

