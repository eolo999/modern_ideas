#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from flask import render_template, abort, redirect
from flask import request
from werkzeug.routing import BaseConverter
from jinja2 import TemplateNotFound


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app = Flask(__name__)
app.url_map.converters['book_content'] = RegexConverter


@app.route('/chapter1/combination.html')
def fix_spammed_link():
    return redirect('/reti-mic/chapter1/combination.html', code=302)


@app.route('/capablanca-cf/<book_content("chapter[0-9](\/[^\/]*)?\.html"):path>')
def capablanca_cf__content(path):
    try:
        path = 'capablanca-cf/' + path
        app.logger.debug(path)
        return render_template(path, path=path)
    except TemplateNotFound:
        abort(404)


@app.route('/reti-mic/<book_content("chapter[0-9](\/[^\/]*)?\.html"):path>')
def reti_mic_content(path):
    try:
        path = 'reti-mic/' + path
        app.logger.debug(path)
        return render_template(path, path=path)
    except TemplateNotFound:
        abort(404)


@app.route("/credits")
def credits():
    return render_template('credits.html')


@app.route('/full/reti-mic.html')
def reti_mic_full():
    return render_template('reti-mic/full_pgn.html', path=request.path)


@app.route('/full/capablanca-cf.html')
def capablanca_cf_full():
    return render_template('capablanca-cf/full_pgn.html', path=request.path)


@app.route("/")
def root():
    return render_template('home.html', path=request.path)


if __name__ == '__main__':
    app.run(debug=True)
